package cl.zonagps.zonaventagps;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cl.zonagps.zonaventagps.clases.*;
import cl.zonagps.zonaventagps.clases.Motivo;
import cl.zonagps.zonaventagps.clases.Visita;

/**
 * Created by LEONEL CARRASCO
 */
public class DBHelper extends SQLiteOpenHelper {

    private static String DB = "Construmart.db";
    private static int version = 29;



    public DBHelper(Context context) {
        super(context, DB, null, version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("Mensaje SQLite", "Base de Datos creada");
        generarBD(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        generarBD(db);
        Log.d("Mensaje SQLite", "Base de Datos Actualizada");
    }

    public void generarBD(SQLiteDatabase db){
        try{
            //Crear tabla Vendedor
            db.execSQL("CREATE TABLE IF NOT EXISTS Vendedor( PK_RutVendedor int PRIMARY KEY, NombreVendedor varchar(500), NombreUsuario varchar(50), PassVendedor varchar(25), imei varchar(15), FK_Estado int);");
            Log.d("Creación Tabla", "Tabla Vendedor Creada");

            db.execSQL("DROP TABLE IF EXISTS Sync");

            //Crear Tabla Prospecto
            db.execSQL("DROP TABLE IF EXISTS Prospecto;");
            Log.d("Creación Tabla", "Tabla Prospecto Eliminada");
            db.execSQL("CREATE TABLE IF NOT EXISTS Prospecto( PK_Prospecto INTEGER PRIMARY KEY, PK_RutVendedor int, RutCliente varchar(10), NombreCliente varchar(250), Encargado varchar(250), Direccion varchar(250), Telefono varchar(250), Email varchar(250), Comentario varchar(255), FK_Estado int, FechaIngreso Datetime);");
            Log.d("Creación Tabla","Tabla Prospecto Creada");

            //Crear Tabla Prospecto
            db.execSQL("DROP TABLE IF EXISTS ProspectoNew;");
            Log.d("Creación Tabla", "Tabla ProspectoNew Eliminada");
            db.execSQL("CREATE TABLE IF NOT EXISTS ProspectoNew( PK_Prospecto int, PK_RutVendedor int, RutCliente varchar(10), NombreCliente varchar(250), Encargado varchar(250), Direccion varchar(250), Telefono varchar(250), Email varchar(250), Comentario varchar(255), FK_Estado int, FechaIngreso Datetime);");
            Log.d("Creación Tabla", "Tabla ProspectoNew Creada");

            //Crear tabla Obra
            db.execSQL("DROP TABLE IF EXISTS Obra;");
            Log.d("Creación Tabla", "Tabla Obra Eliminada");
            db.execSQL("CREATE TABLE IF NOT EXISTS Obra( PK_Obra bigint PRIMARY KEY, FK_RutCliente int, NombreObra varchar(255), DireccionObra varchar(255), LatitudObra decimal(9,6), LongitudObra decimal(9,6), RadioObra int, Contacto varchar(255), Telefono int, Email varchar(255), Comentario varchar(255));");
            Log.d("Creación Tabla", "Tabla Obra Creada");

            //Crear tabla ObraNew
            db.execSQL("DROP TABLE IF EXISTS ObraNew;");
            Log.d("Creación Tabla", "Tabla ObraNew Eliminada");
            db.execSQL("CREATE TABLE IF NOT EXISTS ObraNew( PK_Obra bigint, FK_RutCliente int, NombreObra varchar(255), DireccionObra varchar(255), LatitudObra decimal(9,6), LongitudObra decimal(9,6), RadioObra int, Contacto varchar(255), Telefono int, Email varchar(255), Comentario varchar(255));");
            Log.d("Creación Tabla", "Tabla ObraNew Creada");


            //Crear tabla ObraVendedor
            db.execSQL("DROP TABLE IF EXISTS ObraVendedor;");
            Log.d("Creación Tabla", "Tabla ObraVendedor Eliminada");
            db.execSQL("CREATE TABLE IF NOT EXISTS ObraVendedor( FK_RutVendedor int, FK_Obra bigint);");
            Log.d("Creación Tabla", "Tabla ObraVendedor Creada");

            //Crear tabla ObraVendedorNew
            db.execSQL("DROP TABLE IF EXISTS ObraVendedorNew;");
            Log.d("Creación Tabla", "Tabla ObraVendedorNew Eliminada");
            db.execSQL("CREATE TABLE IF NOT EXISTS ObraVendedorNew( FK_RutVendedor int, FK_Obra bigint);");
            Log.d("Creación Tabla", "Tabla ObraVendedorNew Creada");


            //Crear Tabla ClienteVendedor
            db.execSQL("DROP TABLE IF EXISTS ClienteVendedor;");
            Log.d("Creación Tabla", "Tabla ClienteVendedor Eliminada");
            db.execSQL("CREATE TABLE IF NOT EXISTS ClienteVendedor( FK_RutCliente int, FK_RutVendedor int);");
            Log.d("Creación Tabla","Tabla ClienteVendedor Creada");


            //Crear tabla Visita
            db.execSQL("DROP TABLE IF EXISTS Visita;");
            Log.d("Creación Tabla", "Tabla Visita Eliminada");
            db.execSQL("CREATE TABLE IF NOT EXISTS Visita(PK_Visita INTEGER PRIMARY KEY, FK_RutCliente int, FK_Obra bigint, FK_RutVendedor int, FechaVisita varchar(25), Latitud decimal(9,6), Longitud decimal(9,6), Foto1 varchar(255), Foto2 varchar(255), Observacion varchar(1000), Subida int);");
            Log.d("Creación Tabla", "Tabla Visita Creada");

            //Crear tabla VisitaNew
            db.execSQL("DROP TABLE IF EXISTS VisitaNew;");
            Log.d("Creación Tabla", "Tabla VisitaNew Eliminada");
            db.execSQL("CREATE TABLE IF NOT EXISTS VisitaNew(PK_Visita INTEGER PRIMARY KEY, FK_RutCliente int, FK_Obra bigint, FK_RutVendedor int, FechaVisita varchar(25), Latitud decimal(9,6), Longitud decimal(9,6), Foto1 varchar(255), Foto2 varchar(255), Observacion varchar(1000), idVisita int);");
            Log.d("Creación Tabla", "Tabla VisitaNew Creada");


            //Crear tabla VIsitaMotivo
            db.execSQL("DROP TABLE IF EXISTS VisitaMotivo;");
            Log.d("Creación Tabla", "Tabla VisitaMotivo Eliminada");
            db.execSQL("CREATE TABLE IF NOT EXISTS VisitaMotivo(FK_Visita int, FK_Motivo int, Confirmacion int, Observacion varchar(1000), Foto varchar(255));");
            Log.d("Creación Tabla", "Tabla VisitaMotivo Creada");

            //Crear tabla VIsitaMotivoNew
            db.execSQL("DROP TABLE IF EXISTS VisitaMotivoNew;");
            Log.d("Creación Tabla", "Tabla VisitaMotivoNew Eliminada");
            db.execSQL("CREATE TABLE IF NOT EXISTS VisitaMotivoNew( FK_Visita int, FK_Motivo int, Confirmacion int, Observacion varchar(1000), Foto varchar(255));");
            Log.d("Creación Tabla", "Tabla VisitaMotivoNew Creada");


            //Crear tabla Motivo
            db.execSQL("DROP TABLE IF EXISTS Motivo;");
            Log.d("Creación Tabla", "Tabla Motivo Eliminada");
            db.execSQL("CREATE TABLE IF NOT EXISTS Motivo( PK_Motivo int PRIMARY KEY, DescripcionMotivo varchar(50), FK_Estado int);");
            Log.d("Creación Tabla", "Tabla Motivo Creada");


            //Crear tabla CorrelativoObra
            db.execSQL("DROP TABLE IF EXISTS CorrelativoObra;");
            Log.d("Creación Tabla", "Tabla CorrelativoObra Eliminada");
            db.execSQL("CREATE TABLE IF NOT EXISTS CorrelativoObra( NroCorrelativoObra int);");
            Log.d("Creación Tabla", "Tabla Correlativo Creada");

            //Crear tabla Cliente
            db.execSQL("DROP TABLE IF EXISTS Cliente;");
            Log.d("Creación Tabla", "Tabla Cliente Eliminada");
            db.execSQL("CREATE TABLE IF NOT EXISTS Cliente( PK_RutCliente int PRIMARY KEY, NombreCliente varchar(250), Encargado varchar(250), Direccion varchar(250), Telefono varchar(250), Email varchar(250), Comentario varchar(250), FK_Estado int);");
            Log.d("Creación Tabla", "Tabla Cliente Creada");







        }catch (SQLiteException e){
            Log.e("SQLiteException", e.getMessage());
        }
    }

    public int insertarProspecto(SQLiteDatabase db, Prospecto p){
        try {
            /*String sql = "INSERT INTO Prospecto values("+p.getPk_RutVendedor()+",'"+p.getRutCliente()+"','"+p.getNombreCliente()+
                    "','"+p.getEncargado()+"','"+p.getDireccion()+"','"+p.getTelefono()+"','"+p.getEmail()+"','"+p.getComentario()+"',"+p.getFk_Estado()+",'"+p.getFechaIngreso()+"')";

            db.execSQL(sql);*/
            String sql = "INSERT INTO ProspectoNew(PK_RutVendedor, RutCliente, NombreCliente, Encargado, Direccion, Telefono, Email, Comentario, FK_Estado, FechaIngreso) values("+p.getPk_RutVendedor()+",'"+p.getRutCliente()+"','"+p.getNombreCliente()+
                    "','"+p.getEncargado()+"','"+p.getDireccion()+"','"+p.getTelefono()+"','"+p.getEmail()+"','"+p.getComentario()+"',"+p.getFk_Estado()+",'"+p.getFechaIngreso()+"')";
            db.execSQL(sql);
            return 1;
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
            return -1;
        }catch(Exception e){
            Log.e("Exception", e.getMessage());
            return -2;
        }

    }

    public int agregarVendedorSQLite(Vendedor v, SQLiteDatabase db){
        try{
            String sql = "INSERT INTO Vendedor VALUES("+v.getPk_RutVendedor()+",'"+v.getNombreVendedor()+"','"+v.getNombreUsuario()+"','"+
                    v.getPassUsuario()+"','"+v.getImei()+"',"+v.getFk_Estado()+")";
            db.execSQL(sql);
            return 1;
        }catch(SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
            return -1;
        }catch(Exception e){
            Log.e("Exception",e.getMessage());
            return -2;
        }
    }

    public Vendedor getVendedor(SQLiteDatabase db, String user){
        Vendedor v = null;
        try{
            Cursor c = db.rawQuery("SELECT PK_RutVendedor, NombreVendedor, NombreUsuario, PassVendedor, Imei, FK_Estado from Vendedor "+
            "WHERE NombreUsuario = '"+user+"'",null);
            if(c.getCount() > 0){
                if(c.moveToFirst()){
                    do{
                        v = new Vendedor();
                        v.setPk_RutVendedor(c.getInt(0));
                        v.setNombreVendedor(c.getString(1));
                        v.setNombreUsuario(c.getString(2));
                        v.setPassUsuario(c.getString(3));
                        v.setImei(c.getString(4));
                        v.setFk_Estado(c.getInt(5));
                    }while (c.moveToNext());
                }
            }
            return v;
        }catch (SQLiteException e) {
            Log.e("SQLiteException",e.getMessage());
            return v;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            return v;
        }

    }

    public String verFechaSincronizacion(SQLiteDatabase db){
        String fecha = "";
        try{
            Cursor c = db.rawQuery("SELECT Fecha FROM Sync",null);
            if(c.getCount() > 0){
                if(c.moveToFirst()){
                    do{
                        fecha = c.getString(0);
                    }while (c.moveToNext());
                }
            }

        }catch (SQLiteException e) {
            Log.e("SQLiteException",e.getMessage());

        }catch (Exception e){
            Log.e("Exception",e.getMessage());

        }
        return fecha;
    }

    public boolean actualizarFechaSincronizacion(SQLiteDatabase db,String fecha){
        boolean exito = false;
        String sql = "UPDATE Sync SET Fecha = '"+ fecha +"'";
        Log.e("fecha",fecha);
        try{
            db.execSQL(sql);
            exito = true;
        }catch (SQLiteException e){
            Log.e("SQLiteException", e.getMessage());
        }
        return exito;
    }

    public int insertarCorrelativoObra(CorrelativoObra co, SQLiteDatabase db){
        int res;

        try{
            String sql = "INSERT INTO CorrelativoObra VALUES("+co.getNroCorrelativoObra()+")";
            db.execSQL(sql);
            res = 1;
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
            res = -1;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            res = -2;
        }

        return res;
    }

    public int eliminarCorrelativoObra(SQLiteDatabase db){
        int res;

        try{
            String sql = "DELETE FROM CorrelativoObra";
            db.execSQL(sql);
            res = 1;
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
            res = -1;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            res = -2;
        }

        return res;
    }

    public int editCorrelativoObra(SQLiteDatabase db){
        int res;

        try{
            String sql = "UPDATE CorrelativoObra set NroCorrelativoObra = NroCorrelativoObra + 1";
            db.execSQL(sql);
            res = 1;
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
            res = -1;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            res = -2;
        }

        return res;
    }

    public CorrelativoObra getCorrelativoObra(SQLiteDatabase db, SQLiteDatabase dbw){
        CorrelativoObra co = null;
        if(editCorrelativoObra(dbw) == 1){
            try{
                Cursor c = db.rawQuery("SELECT NroCorrelativoObra from CorrelativoObra",null);
                if(c.getCount() > 0){
                    if(c.moveToFirst()){
                        do{
                            co = new CorrelativoObra();
                            co.setNroCorrelativoObra(c.getLong(0));
                        }while (c.moveToNext());
                    }
                }
            }catch (SQLiteException e){
                Log.e("SQLiteException",e.getMessage());
                co = null;
            }catch (Exception e){
                Log.e("Exception",e.getMessage());
                co = null;
            }
        }
        return co;
    }

    public int insertarObra(Obra o, SQLiteDatabase db){
        try{
            String sql = "INSERT INTO Obra values("+o.getPk_Obra()+","+o.getFk_RutCliente()+",'"+o.getNombreObra()+"','"+o.getDireccionObra()+
                    "',"+o.getLatitudObra()+","+o.getLongitudObra()+","+o.getRadioObra()+",'"+o.getContacto()+"',"+o.getTelefono()+",'"+o.getEmail()+
                    "','"+o.getComentario()+"')";
            db.execSQL(sql);
            sql = "INSERT INTO ObraNew values("+o.getPk_Obra()+","+o.getFk_RutCliente()+",'"+o.getNombreObra()+"','"+o.getDireccionObra()+
                    "',"+o.getLatitudObra()+","+o.getLongitudObra()+","+o.getRadioObra()+",'"+o.getContacto()+"',"+o.getTelefono()+",'"+o.getEmail()+
                    "','"+o.getComentario()+"')";
            db.execSQL(sql);
            return 1;
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
            return -1;
        }catch(Exception e){
            Log.e("Exception",e.getMessage());
            return -2;
        }
    }

    public int agregarObrasSQLite(Obra o, SQLiteDatabase db){
        try{
            String sql = "INSERT INTO Obra VALUES("+o.getPk_Obra()+","+o.getFk_RutCliente()+",'"+o.getNombreObra()+"','"+o.getDireccionObra()+
                    "',"+o.getLatitudObra()+","+o.getLongitudObra()+","+o.getRadioObra()+",'"+o.getContacto()+"',"+o.getTelefono()+",'"+o.getEmail()+ "','"+o.getComentario()+"')";
            db.execSQL(sql);
            return 1;
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
            return -1;
        }catch(Exception e){
            Log.e("Exception",e.getMessage());
            return -2;
        }
    }

    public Obra verObra(long id, SQLiteDatabase db){
        Obra o = null;
        try{
            Cursor c = db.rawQuery("SELECT NombreObra FROM Obra WHERE PK_Obra = " + id, null);
            if(c.getCount() > 0){
                if(c.moveToFirst()){
                    o = new Obra();
                    o.setNombreObra(c.getString(0));
                }
            }
        }catch (SQLiteException e){
            Log.e("SQLiException", e.getMessage());
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
        }
        return o;
    }

    public ArrayList<Obra> verObras(SQLiteDatabase db){
        ArrayList<Obra> obras = null;
        try{
            Cursor c = db.rawQuery("SELECT NombreObra, PK_Obra, LatitudObra, LongitudObra, FK_RutCliente, RadioObra FROM Obra",null);
            obras = new ArrayList<Obra>();

            if(c.getCount() > 0){

                if(c.moveToFirst()){
                    do{
                        Obra o = new Obra();
                        o.setNombreObra(c.getString(0));
                        o.setPk_Obra(c.getLong(1));
                        o.setLatitudObra(c.getDouble(2));
                        o.setLongitudObra(c.getDouble(3));
                        o.setFk_RutCliente(c.getInt(4));
                        o.setRadioObra(c.getInt(5));
                        obras.add(o);
                    }while(c.moveToNext());
                }
            }
        }catch(SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
        }
        return obras;
    }

    public ArrayList<Obra> verObrasCliente(int rutCliente, SQLiteDatabase db){
        ArrayList<Obra> obras = null;
        try{
            Cursor c = db.rawQuery("SELECT NombreObra, PK_Obra, LatitudObra, LongitudObra, RadioObra FROM Obra WHERE FK_RutCliente = " + rutCliente,null);
            obras = new ArrayList<Obra>();
            Obra no = new Obra();
            no.setPk_Obra(0);
            no.setNombreObra("Seleccione obra...");
            obras.add(no);
            if(c.getCount() > 0){

                if(c.moveToFirst()){
                    do{
                        Obra o = new Obra();
                        o.setNombreObra(c.getString(0));
                        o.setPk_Obra(c.getLong(1));
                        o.setLatitudObra(c.getDouble(2));
                        o.setLongitudObra(c.getDouble(3));
                        o.setRadioObra(c.getInt(4));
                        obras.add(o);
                    }while(c.moveToNext());
                }
            }
        }catch(SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
        }
        return obras;
    }

    public int insertarObraVendedor(ObraVendedor ov, SQLiteDatabase db){
        int res;
        try{
            String sql = "INSERT INTO ObraVendedor VALUES ("+ov.getFk_RutVendedor()+","+ov.getFk_Obra()+")";
            db.execSQL(sql);
            sql = "INSERT INTO ObraVendedorNew VALUES ("+ov.getFk_RutVendedor()+","+ov.getFk_Obra()+")";
            db.execSQL(sql);
            res = 1;
        }catch (SQLiteException e) {
            Log.e("SQLiteException",e.getMessage());
            res = -1;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            res = -2;
        }
        return res;
    }



    public int agregarMotivosaSQLite(cl.zonagps.zonaventagps.clases.Motivo m, SQLiteDatabase db){
        int res = 0;
        try{
            String sql = "INSERT INTO Motivo(PK_Motivo, DescripcionMotivo) VALUES("+m.getPk_Motivo()+",'"+m.getDescripcionMotivo()+"')";
            db.execSQL(sql);
            res = 1;
        }catch (SQLiteException ex){
            Log.e("SQLiteException",ex.getMessage());
            res = -1;
        }catch (Exception ex){
            Log.e("Exception",ex.getMessage());
            res = -2;
        }
        return res;
    }

    public Motivo verMotivo(int id, SQLiteDatabase db){
        Motivo m = null;
        try{
            Cursor c = db.rawQuery("SELECT DescripcionMotivo FROM Motivo WHERE PK_Motivo = "+id,null);
            if(c.getCount() > 0){
                if(c.moveToFirst()){
                    do{
                        m = new Motivo();
                        m.setDescripcionMotivo(c.getString(0));
                    }while (c.moveToNext());
                }
            }
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
        }
        return m;
    }



    public Cliente verCliente(int rut, SQLiteDatabase db){
        Cliente cl = null;
        try{
            Cursor c = db.rawQuery("SELECT PK_RutCliente, NombreCliente FROM Cliente WHERE PK_RutCliente = "+rut+"",null);
            if(c.getCount() > 0){
                if(c.moveToFirst()){
                    do{
                        cl = new Cliente();
                        cl.setPk_RutCliente(c.getInt(0));
                        cl.setNombreCliente(c.getString(1));
                    }while (c.moveToNext());
                }
            }
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
            cl = null;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            cl = null;
        }
        return cl;
    }

    public ArrayList<Cliente> buscarClientes(boolean opcion, SQLiteDatabase db, String valor){
        ArrayList<Cliente> clientes = null;
        try{
            String sql="";
            if(opcion){
                sql = "WHERE PK_RutCliente = "+ valor;
            }else{
                sql = "WHERE NombreCliente LIKE '%"+ valor +"%'";
            }
            Cursor c = db.rawQuery("SELECT PK_RutCliente, NombreCliente FROM Cliente " + sql,null);
            if (c.getCount() > 0){
                clientes = new ArrayList<Cliente>();
                if(c.moveToFirst()){
                    do{
                        Cliente cl = new Cliente();
                        cl.setPk_RutCliente(c.getInt(0));
                        cl.setNombreCliente(c.getString(1));
                        clientes.add(cl);
                    }while (c.moveToNext());
                }
            }
        }catch (SQLiteException e){
            Log.e("SQLiteException", e.getMessage());
        }
        catch (Exception e){
            Log.e("Exception", e.getMessage());
        }
        return clientes;
    }

    public ArrayList<Cliente> verClientes(SQLiteDatabase db){
        ArrayList<Cliente> clientes = null;
        try{

            Cursor c = db.rawQuery("SELECT PK_RutCliente, NombreCliente FROM Cliente",null);
            if (c.getCount() > 0){
                clientes = new ArrayList<Cliente>();
                if(c.moveToFirst()){
                    do{
                        Cliente cl = new Cliente();
                        cl.setPk_RutCliente(c.getInt(0));
                        cl.setNombreCliente(c.getString(1));
                        clientes.add(cl);
                    }while (c.moveToNext());
                }
            }
        }catch (SQLiteException e){
            Log.e("SQLiteException", e.getMessage());
        }
        catch (Exception e){
            Log.e("Exception", e.getMessage());
        }
        return clientes;
    }

    public int eliminarObraSQLite(long obra, SQLiteDatabase db){
        int res;
        try{
            String sql = "DELETE FROM Obra WHERE PK_Obra = " + obra;
            db.execSQL(sql);
            res = 1;
        }catch (SQLiteException e) {
            Log.e("SQLiteException",e.getMessage());
            res = -1;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            res = -2;
        }
        return res;
    }

    public int eliminarClienteSQLite(int rut, SQLiteDatabase db){
        int res;
        try{
            String sql = "DELETE FROM Cliente WHERE PK_RutCliente = " + rut;
            db.execSQL(sql);
            res = 1;
        }catch (SQLiteException e) {
            Log.e("SQLiteException",e.getMessage());
            res = -1;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            res = -2;
        }
        return res;
    }

    public int agregarClientesaSQLite(Cliente cl, SQLiteDatabase db){
        int res;
        try{
            String sql = "INSERT INTO Cliente VALUES ("+cl.getPk_RutCliente()+",'"+cl.getNombreCliente()+"','"+cl.getEncargado()+"','"+cl.getDireccion()+
                    "','"+cl.getTelefono()+"','"+cl.getEmail()+"','"+cl.getComentario()+"',"+cl.getFk_Estado()+")";
            db.execSQL(sql);
            Log.e("Agregado", "Se agrego a " + cl.getNombreCliente());
            res = 1;
        }catch (SQLiteException e) {
            Log.e("SQLiteException",e.getMessage());
            res = -1;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            res = -2;
        }
        return res;
    }

    public int insertarVisita(cl.zonagps.zonaventagps.clases.Visita v, SQLiteDatabase db, SQLiteDatabase dbw){
        int res = 0;
        try {
            String sql = "INSERT INTO Visita VALUES (NULL,"+v.getFk_RutCliente()+","+v.getFk_Obra()+","+v.getFk_RutVendedor()+",'"+v.getFechaVisita()+
                    "',"+v.getLatitud()+","+v.getLongitud()+",'','','"+v.getObservacion()+"',0)";
            dbw.execSQL(sql);
            int idVisita = 0;
            Cursor c = db.rawQuery("SELECT PK_Visita FROM Visita ORDER BY PK_Visita DESC LIMIT 1",null);
            if(c.moveToFirst()){
                idVisita = c.getInt(0);
            }
            sql = "INSERT INTO VisitaNew VALUES (NULL,"+v.getFk_RutCliente()+","+v.getFk_Obra()+","+v.getFk_RutVendedor()+",'"+v.getFechaVisita()+
                    "',"+v.getLatitud()+","+v.getLongitud()+",'','','"+v.getObservacion()+"',"+idVisita+")";
            dbw.execSQL(sql);
            res = 1;

        }catch (SQLiteException e){
            Log.e("SQLiteExceptionv",e.getMessage());
            res = -1;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            res = -2;
        }
        return res;
    }

    public int insertarVisitaSubidas(cl.zonagps.zonaventagps.clases.Visita v, SQLiteDatabase db, SQLiteDatabase dbw){
        int res = 0;
        try {
            String sql = "INSERT INTO Visita VALUES (NULL,"+v.getFk_RutCliente()+","+v.getFk_Obra()+","+v.getFk_RutVendedor()+",'"+v.getFechaVisita()+
                    "',"+v.getLatitud()+","+v.getLongitud()+",'','','"+v.getObservacion()+"')";
            dbw.execSQL(sql);
            res = 1;

        }catch (SQLiteException e){
            Log.e("SQLiteExceptionv",e.getMessage());
            res = -1;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            res = -2;
        }
        return res;
    }

    public int updateVisitaSubidas(int visita, SQLiteDatabase db, SQLiteDatabase dbw){
        int res = 0;
        try {
            String sql = "UPDATE Visita SET Subida = 1 WHERE PK_Visita = " + visita;
            dbw.execSQL(sql);
            res = 1;
        }catch (SQLiteException e){
            Log.e("SQLiteExceptionv",e.getMessage());
            res = -1;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            res = -2;
        }
        return res;
    }

    public int agregarVisitasSQLite(cl.zonagps.zonaventagps.clases.Visita v, SQLiteDatabase db){
        int res = 0;
        try {
            String sql = "INSERT INTO Visita(FK_RutCliente, FK_Obra, FK_RutVendedor, FechaVisita, Latitud, Longitud, Observacion, Subida) VALUES ("+v.getFk_RutCliente()+","+v.getFk_Obra()+","+v.getFk_RutVendedor()+",'"+v.getFechaVisita()+
                    "',"+v.getLatitud()+","+v.getLongitud()+",'"+v.getObservacion()+"',1)";
            db.execSQL(sql);
            res = 1;
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
            res = -1;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            res = -2;
        }
        return res;
    }

    public int agregarVisitasNoSubidasSQLite(cl.zonagps.zonaventagps.clases.Visita v, SQLiteDatabase db){
        int res = 0;
        try {
            String sql = "INSERT INTO Visita(FK_RutCliente, FK_Obra, FK_RutVendedor, FechaVisita, Latitud, Longitud, Observacion, Subida) VALUES ("+v.getFk_RutCliente()+","+v.getFk_Obra()+","+v.getFk_RutVendedor()+",'"+v.getFechaVisita()+
                    "',"+v.getLatitud()+","+v.getLongitud()+",'"+v.getObservacion()+"',0)";
            db.execSQL(sql);
            res = 1;
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
            res = -1;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            res = -2;
        }
        return res;
    }

    /*
    public int agregarVisitasMotivoSQLite(VisitaMotivo vm, SQLiteDatabase db){
        int res = 0;
        try {
            String sql = "INSERT INTO VisitaMotivo(FK_Visita, FK_Motivo, Confirmacion, FechaVisita, Latitud, Longitud, Observacion) VALUES ("+v.getFk_RutCliente()+","+v.getFk_Obra()+","+v.getFk_RutVendedor()+",'"+v.getFechaVisita()+
                    "',"+v.getLatitud()+","+v.getLongitud()+",'"+v.getObservacion()+"')";
            db.execSQL(sql);
            res = 1;
        }catch (SQLiteException e){
            res = -1;
        }catch (Exception e){
            res = -2;
        }
        return res;
    }*/

    public int getUltimoIDVisita(SQLiteDatabase db){
        int idVisita = 0;
        try{
            Cursor c = db.rawQuery("SELECT PK_Visita FROM VisitaNew ORDER BY PK_Visita DESC LIMIT 1",null);
            if(c.moveToFirst()){
                idVisita = c.getInt(0);
            }
        }catch (SQLiteException e){
            Log.e("SQLiteException1", e.getMessage());
            idVisita = -1;
        }catch (Exception e){
            Log.e("Exception", e.getMessage());
            idVisita = -2;
        }
        return idVisita;
    }

    public cl.zonagps.zonaventagps.clases.Visita verVisita(int visita, SQLiteDatabase db){
        cl.zonagps.zonaventagps.clases.Visita v = null;
        try{
            Cursor c = db.rawQuery("SELECT FK_RutCliente, FechaVisita, Observacion FROM Visita WHERE PK_Visita = "+visita,null);
            if(c.getCount()> 0){
                if(c.moveToFirst()){
                    do {
                        v = new cl.zonagps.zonaventagps.clases.Visita();
                        v.setFk_RutCliente(c.getInt(0));
                        v.setFechaVisita(c.getString(1));
                        v.setObservacion(c.getString(2));
                    }while (c.moveToNext());
                }
            }
        }catch(SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
        }
        return v;
    }

    public ArrayList<Visita> verMisVisitasDelDia(SQLiteDatabase db){
        ArrayList<Visita> visitas= null;
        try{
            String fecha = getDateTime();
            Log.e("Fecha",fecha);
            Cursor c = db.rawQuery("SELECT FK_RutCliente, PK_Visita, FechaVisita, Observacion, FK_Obra, Subida FROM Visita WHERE FechaVisita LIKE '"+fecha+"%'",null);
            if(c.getCount() > 0){
                visitas = new ArrayList<Visita>();
                if(c.moveToFirst()){
                    do {
                        Visita v = new Visita();
                        v.setFk_RutCliente(c.getInt(0));
                        v.setPk_Visita(c.getInt(1));
                        v.setFechaVisita(c.getString(2));
                        v.setObservacion(c.getString(3));
                        v.setFk_Obra(c.getLong(4));
                        v.setSubida(c.getInt(5));
                        visitas.add(v);
                    }while (c.moveToNext());
                }
            }
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
        }
        return visitas;
    }

    public ArrayList<Visita> verMisVisitasCliente(SQLiteDatabase db, int rut){
        ArrayList<Visita> visitas= null;
        try{


            Cursor c = db.rawQuery("SELECT FK_RutCliente, PK_Visita, FechaVisita, Observacion, FK_Obra, Subida FROM Visita WHERE FK_RutCliente = "+rut+"",null);
            if(c.getCount() > 0){
                visitas = new ArrayList<Visita>();
                if(c.moveToFirst()){
                    do {
                        Visita v = new Visita();
                        v.setFk_RutCliente(c.getInt(0));
                        v.setPk_Visita(c.getInt(1));
                        v.setFechaVisita(c.getString(2));
                        v.setObservacion(c.getString(3));
                        v.setFk_Obra(c.getLong(4));
                        v.setSubida(c.getInt(5));
                        visitas.add(v);
                    }while (c.moveToNext());
                }
            }
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
        }
        return visitas;
    }

    public ArrayList<Visita> verVisitas(SQLiteDatabase db){
        ArrayList<Visita> visitas= null;
        try{


            Cursor c = db.rawQuery("SELECT FK_RutCliente, PK_Visita, FechaVisita, Observacion, FK_Obra, Subida FROM Visita",null);
            Log.e("COUNT",""+c.getCount());
            if(c.getCount() > 0){
                visitas = new ArrayList<Visita>();
                if(c.moveToFirst()){
                    do {
                        Visita v = new Visita();
                        v.setFk_RutCliente(c.getInt(0));
                        v.setPk_Visita(c.getInt(1));
                        v.setFechaVisita(c.getString(2));
                        v.setObservacion(c.getString(3));
                        v.setFk_Obra(c.getLong(4));
                        v.setSubida(c.getInt(5));
                        visitas.add(v);
                    }while (c.moveToNext());
                }
            }
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
        }
        return visitas;
    }

    public int insertarVisitaMotivo(VisitaMotivo vm, SQLiteDatabase db){
        int res = 0;
        try{
            String sql = "INSERT INTO VisitaMotivo(FK_Visita, FK_Motivo, Observacion, Confirmacion) VALUES("+vm.getFk_Visita()+","+vm.getFk_Motivo()+",'"+vm.getObservacion()+"',"+vm.getConfirmacion()+")";
            db.execSQL(sql);

            sql = "INSERT INTO VisitaMotivoNew(FK_Visita, FK_Motivo, Observacion, Confirmacion) VALUES("+vm.getFk_Visita()+","+vm.getFk_Motivo()+",'"+vm.getObservacion()+"',"+vm.getConfirmacion()+")";
            db.execSQL(sql);
            res = 1;
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
            res = -1;
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
            res = -2;
        }
        return res;
    }




    //TRAER PROSPECTOS, OBRA Y VISITAS

    public ArrayList<Prospecto> getProspectosNuevostoSQLServer(SQLiteDatabase db){
        ArrayList<Prospecto> prospectos = null;
        String sql = "SELECT PK_RutVendedor, RutCliente, NombreCliente, Encargado, Direccion, Telefono, Email, Comentario, FK_Estado, FechaIngreso FROM ProspectoNew";
        try{
            Cursor c = db.rawQuery(sql,null);
            if(c.getCount() > 0){
                prospectos = new ArrayList<Prospecto>();
                if(c.moveToFirst()){
                    do{
                        Prospecto p = new Prospecto();
                        p.setPk_RutVendedor(c.getInt(0));
                        p.setRutCliente(c.getString(1));
                        p.setNombreCliente(c.getString(2));
                        p.setEncargado(c.getString(3));
                        p.setDireccion(c.getString(4));
                        p.setTelefono(c.getString(5));
                        p.setEmail(c.getString(6));
                        p.setComentario(c.getString(7));
                        p.setFk_Estado(c.getInt(8));
                        p.setFechaIngreso(c.getString(9));
                        prospectos.add(p);
                    }while (c.moveToNext());
                }
            }
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
        }catch(Exception e){
            Log.e("Exception",e.getMessage());
        }
        return prospectos;
    }

    public ArrayList<ObraVendedor> getObrasVendedorNuevostoSQLServer(long obra,SQLiteDatabase db){
        ArrayList<ObraVendedor> ovs = null;
        String sql = "SELECT FK_RutVendedor, FK_Obra FROM ObraVendedorNew WHERE FK_Obra = " +obra;
        try {
            Cursor c = db.rawQuery(sql,null);
            if(c.getCount() > 0){
                ovs = new ArrayList<ObraVendedor>();
                if(c.moveToFirst()){
                    do{
                        ObraVendedor ov = new ObraVendedor();
                        ov.setFk_RutVendedor(c.getInt(0));
                        ov.setFk_Obra(c.getLong(1));
                        ovs.add(ov);
                    }while (c.moveToNext());
                }
            }
        }catch (SQLiteException e){
            Log.e("SQLiteException", e.getMessage());
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
        }
        return  ovs;
    }

    public ArrayList<Obra> getObrasNuevastoSQLServer(SQLiteDatabase db){
        ArrayList<Obra> obras = null;
        String sql = "SELECT FK_RutCliente, NombreObra, DireccionObra, LatitudObra, LongitudObra, RadioObra, Contacto, Telefono, Email, Comentario, PK_Obra FROM ObraNew";
        try {
            Cursor c = db.rawQuery(sql,null);
            if(c.getCount() > 0){
                obras = new ArrayList<Obra>();
                if(c.moveToFirst()){
                    do{
                        Obra o = new Obra();
                        o.setFk_RutCliente(c.getInt(0));
                        o.setNombreObra(c.getString(1));
                        o.setDireccionObra(c.getString(2));
                        o.setLatitudObra(c.getDouble(3));
                        o.setLongitudObra(c.getDouble(4));
                        o.setRadioObra(c.getInt(5));
                        o.setContacto(c.getString(6));
                        o.setTelefono(c.getInt(7));
                        o.setEmail(c.getString(8));
                        o.setComentario(c.getString(9));
                        o.setPk_Obra(c.getLong(10));
                        obras.add(o);
                    }while (c.moveToNext());
                }
            }
        }catch (SQLiteException e){
            Log.e("SQLiteException", e.getMessage());
        }catch (Exception e){
            Log.e("Exception", e.getMessage());
        }
        return obras;
    }

    public int actualizarObraVisita(SQLiteDatabase db, long obra, long obraNueva){
        int res = 0;
        String sql = "UPDATE VisitaNew SET FK_Obra = "+ obraNueva + " WHERE FK_Obra = " +obra;
        try{
            db.execSQL(sql);

            res = 1;
        }catch (SQLiteException e){
            Log.e("SQLiteException", e.getMessage());
            res = -1;
        }
        return res;
    }


    public ArrayList<Visita> getVisitasNuevasdeObratoSQLServer(long obra, SQLiteDatabase db){
        ArrayList<Visita> visitas = null;
        String sql;
        if(obra != 0){
             sql = "SELECT PK_Visita, FK_RutCliente, FK_Obra, FK_RutVendedor, FechaVisita, Latitud, Longitud, Observacion, idVisita FROM VisitaNew WHERE  FK_Obra = " + obra;
        }else{
            sql = "SELECT PK_Visita, FK_RutCliente, FK_Obra, FK_RutVendedor, FechaVisita, Latitud, Longitud, Observacion, idVisita FROM VisitaNew";
        }
        try{
            Cursor c = db.rawQuery(sql,null);
            if(c.getCount() > 0){
                visitas = new ArrayList<Visita>();
                if(c.moveToFirst()){
                    do{
                        Visita v = new Visita();
                        v.setPk_Visita(c.getInt(0));
                        v.setFk_RutCliente(c.getInt(1));
                        v.setFk_Obra(c.getLong(2));
                        v.setFk_RutVendedor(c.getInt(3));
                        v.setFechaVisita(c.getString(4));
                        v.setLatitud(c.getDouble(5));
                        v.setLongitud(c.getDouble(6));
                        v.setObservacion(c.getString(7));
                        v.setIdVisita(c.getInt(8));
                        visitas.add(v);
                    }while (c.moveToNext());
                }
            }
        }catch (SQLiteException e){
            Log.e("SQLiteException", e.getMessage());
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
        }
        return  visitas;
    }

    public int deleteVisita(int id, SQLiteDatabase db){
        int res = 0;
        String sql = "DELETE FROM VisitaNew WHERE PK_Visita = " +id;
        try{
            db.execSQL(sql);

            res = 1;
        }catch (SQLiteException e){
            Log.e("SQLiteException", e.getMessage());
            res = -1;
        }
        return res;
    }

    public int deleteVisitaMotivo(int visita, SQLiteDatabase db){
        int res = 0;
        String sql = "DELETE FROM VisitaMotivoNew WHERE FK_Visita = " + visita;
        try{
            db.execSQL(sql);

            res = 1;
        }catch (SQLiteException e){
            Log.e("SQLiteException", e.getMessage());
            res = -1;
        }
        return res;
    }

    public int deleteObra(long obra, SQLiteDatabase db){
        int res = 0;
        String sql = "DELETE FROM ObraNew WHERE PK_Obra = " + obra;
        try{
            db.execSQL(sql);

            res = 1;
        }catch (SQLiteException e){
            Log.e("SQLiteException", e.getMessage());
            res = -1;
        }
        return res;
    }

    public int updateObra(long correlativo, long obra, SQLiteDatabase db){
        int res = 0;
        String sql = "UPDATE Obra SET PK_Obra = " + correlativo +" WHERE PK_Obra = " +obra;
        try{
            db.execSQL(sql);

            res = 1;
        }catch (SQLiteException e){
            Log.e("SQLiteException", e.getMessage());
            res = -1;
        }
        return res;
    }


    public ArrayList<Visita> getVisitasSinSubirObratoSQLServer(SQLiteDatabase db){
        ArrayList<Visita> visitas = null;
        String sql;

            sql = "SELECT PK_Visita, FK_RutCliente, FK_Obra, FK_RutVendedor, FechaVisita, Latitud, Longitud, Observacion FROM Visita WHERE Subida = 0";

        try{
            Cursor c = db.rawQuery(sql,null);
            if(c.getCount() > 0){
                visitas = new ArrayList<Visita>();
                if(c.moveToFirst()){
                    do{
                        Visita v = new Visita();
                        v.setPk_Visita(c.getInt(0));
                        v.setFk_RutCliente(c.getInt(1));
                        v.setFk_Obra(c.getLong(2));
                        v.setFk_RutVendedor(c.getInt(3));
                        v.setFechaVisita(c.getString(4));
                        v.setLatitud(c.getDouble(5));
                        v.setLongitud(c.getDouble(6));
                        v.setObservacion(c.getString(7));
                        visitas.add(v);
                    }while (c.moveToNext());
                }
            }
        }catch (SQLiteException e){
            Log.e("SQLiteException", e.getMessage());
        }catch (Exception e){
            Log.e("Exception",e.getMessage());
        }
        return  visitas;
    }


    public ArrayList<VisitaMotivo> getMotivosVisitasNuevasdeObra(int visita, SQLiteDatabase db){
        ArrayList<VisitaMotivo> vms = null;
        String sql = "SELECT FK_Visita, FK_Motivo, Observacion, Confirmacion FROM VisitaMotivoNew WHERE FK_Visita = " + visita;
        try{
            Cursor c = db.rawQuery(sql,null);
            if(c.getCount() > 0){
                vms = new ArrayList<VisitaMotivo>();
                if(c.moveToFirst()){
                    do{
                        VisitaMotivo vm = new VisitaMotivo();
                        vm.setFk_Visita(c.getInt(0));
                        vm.setFk_Motivo(c.getInt(1));
                        vm.setObservacion(c.getString(2));
                        vm.setConfirmacion(c.getInt(3));
                        //TODO: aqui
                        vms.add(vm);
                    }while (c.moveToNext());
                }
            }
        }catch (SQLiteException e){
            Log.e("SQLiteException",e.getMessage());
        }catch (Exception e){
            Log.e("Exception", e.getMessage());
        }
        return vms;
    }







    private String getDateTime() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }
}
