package cl.zonagps.zonaventagps;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.Settings;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import cl.zonagps.zonaventagps.DAO.Sincronizar;
import cl.zonagps.zonaventagps.DAO.VendedorDAO;
import cl.zonagps.zonaventagps.clases.*;
import cl.zonagps.zonaventagps.clases.Visita;


public class Panel extends ActionBarActivity implements ActionBar.TabListener, ViewPager.OnPageChangeListener, LocationListener {

    private ViewPager mViewPager;
    private Vendedor vendedor;
    private MenuItem btnSubir;
    private MenuItem btnCargar;
    String FechaHoy;

    @Override
    protected void onRestart() {
        super.onRestart();
        finish();
        startActivity(getIntent());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panel);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#006e98")));

        getSupportActionBar().setStackedBackgroundDrawable(new ColorDrawable(Color.parseColor("#207ea2")));


        Intent i = new Intent(Panel.this,SyncService.class);
        startService(i);

        //vendedor = (Vendedor)getIntent().getExtras().getSerializable("Vendedor");
        vendedor = AuxClass.getV();
        //Toast.makeText(getApplicationContext(),"BIENVENIDO(A) "+vendedor.getNombreVendedor(),Toast.LENGTH_LONG).show();

        PagerAdapter adapter = new PagerAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(adapter);

        mViewPager.setOnPageChangeListener(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);



        ActionBar.Tab t = actionBar.newTab()
                .setText("Ver Clientes")

                .setIcon(R.drawable.ic_action_social_group)
                .setTabListener(this);
        actionBar.addTab(t);
        t = actionBar.newTab()
                .setText("Nuevo Prospecto")
                .setIcon(R.drawable.ic_action_content_add_circle_outline)
                .setTabListener(this);
        actionBar.addTab(t);



    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.menu_panel, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        btnSubir = item;
        btnCargar = item;
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            Intent i = new Intent();
            i.setData(Uri.parse("http://www.zonagps.cl/construmart/manual.pdf"));
            startActivity(i);
            return true;
        }
        if(id == R.id.action_about){
            Intent i = new Intent(getApplicationContext(),Acerca.class);
            startActivity(i);
        }
        if(id == R.id.action_salir){
            if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN){
                finishAffinity();
            }
        }
        if(id == R.id.listarVisitas){
            Intent i = new Intent(getApplicationContext(),VisitasDia.class);
            startActivity(i);
        }
        if(id == R.id.cargarDatos){
            if(!isAirplaneModeOn(getApplicationContext())){
                if(hayInternet(getApplicationContext())) {

                    //TODO: cargar datos
                    new cargarDatosSync().execute();

                }else{
                    //Toast.makeText(getApplicationContext(),"No hay internet",Toast.LENGTH_LONG).show();
                    // Inflating the layout for the toast
                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.error_toast,
                            (ViewGroup) findViewById(R.id.toast_custom_error));

                    // Typecasting and finding the view in the inflated layout
                    TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                    // Setting the text to be displayed in the Toast
                    text.setText("No hay internet. No se\npueden subir los datos.");

                    // Creating the Toast
                    Toast toast = new Toast(getApplicationContext());

                    // Setting the position of the Toast to centre
                    toast.setGravity(Gravity.CENTER, 0, 0);

                    // Setting the duration of the Toast
                    toast.setDuration(Toast.LENGTH_SHORT);

                    // Setting the Inflated Layout to the Toast
                    toast.setView(layout);

                    // Showing the Toast
                    toast.show();
                }
            }else{
                //Toast.makeText(getApplicationContext(),"El dispositivo está en modo avión, desactívelo.",Toast.LENGTH_LONG).show();
                // Inflating the layout for the toast
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.error_toast,
                        (ViewGroup) findViewById(R.id.toast_custom_error));

                // Typecasting and finding the view in the inflated layout
                TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                // Setting the text to be displayed in the Toast
                text.setText("El dispositivo está en\nmodo avión, desactívelo.");

                // Creating the Toast
                Toast toast = new Toast(getApplicationContext());

                // Setting the position of the Toast to centre
                toast.setGravity(Gravity.CENTER, 0, 0);

                // Setting the duration of the Toast
                toast.setDuration(Toast.LENGTH_SHORT);

                // Setting the Inflated Layout to the Toast
                toast.setView(layout);

                // Showing the Toast
                toast.show();
            }
        }
        if(id == R.id.subirDatos){
            if(!isAirplaneModeOn(getApplicationContext())){
                if(hayInternet(getApplicationContext())) {
                    new SubirDatos().execute();
                }else{
                    //Toast.makeText(getApplicationContext(),"No hay internet",Toast.LENGTH_LONG).show();
                    // Inflating the layout for the toast
                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.error_toast,
                            (ViewGroup) findViewById(R.id.toast_custom_error));

                    // Typecasting and finding the view in the inflated layout
                    TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                    // Setting the text to be displayed in the Toast
                    text.setText("No hay internet. No se\npueden subir los datos.");

                    // Creating the Toast
                    Toast toast = new Toast(getApplicationContext());

                    // Setting the position of the Toast to centre
                    toast.setGravity(Gravity.CENTER, 0, 0);

                    // Setting the duration of the Toast
                    toast.setDuration(Toast.LENGTH_SHORT);

                    // Setting the Inflated Layout to the Toast
                    toast.setView(layout);

                    // Showing the Toast
                    toast.show();
                }
            }else{
                //Toast.makeText(getApplicationContext(),"El dispositivo está en modo avión, desactívelo.",Toast.LENGTH_LONG).show();
                // Inflating the layout for the toast
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.error_toast,
                        (ViewGroup) findViewById(R.id.toast_custom_error));

                // Typecasting and finding the view in the inflated layout
                TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                // Setting the text to be displayed in the Toast
                text.setText("El dispositivo está en\nmodo avión, desactívelo.");

                // Creating the Toast
                Toast toast = new Toast(getApplicationContext());

                // Setting the position of the Toast to centre
                toast.setGravity(Gravity.CENTER, 0, 0);

                // Setting the duration of the Toast
                toast.setDuration(Toast.LENGTH_SHORT);

                // Setting the Inflated Layout to the Toast
                toast.setView(layout);

                // Showing the Toast
                toast.show();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onPageScrolled(int i, float v, int i1) {

    }

    @Override
    public void onPageSelected(int i) {
        getSupportActionBar().setSelectedNavigationItem(i);
    }

    @Override
    public void onPageScrollStateChanged(int i) {

    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        mViewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {

    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Panel.this);
        alertDialogBuilder.setTitle("GPS Desactivado");
        alertDialogBuilder.setMessage("Tiene que activar el GPS para seguir").setPositiveButton("Habilitar", new DialogInterface.OnClickListener()  {
            public void onClick(DialogInterface dialog, int id)
            {
                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));

            }}).setNegativeButton("Salir", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
                finish();
            }
        });
        alertDialogBuilder.show();
    }


    //SUBIR DATOS A LA NUBE
    public class SubirDatos extends AsyncTask<Void,Void,Void>{

        ProgressDialog pd;

        @Override
        protected void onPostExecute(Void aVoid) {
            btnSubir.setEnabled(true);
            pd.dismiss();
            //Toast.makeText(Panel.this,"Sincronización exitosa",Toast.LENGTH_LONG).show();
            // Inflating the layout for the toast
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.ok_toast,
                    (ViewGroup) findViewById(R.id.toast_custom_ok));

            // Typecasting and finding the view in the inflated layout
            TextView text = (TextView) layout.findViewById(R.id.tvtoast);

            // Setting the text to be displayed in the Toast
            text.setText("Sincronización exitosa.");

            // Creating the Toast
            Toast toast = new Toast(getApplicationContext());

            // Setting the position of the Toast to centre
            toast.setGravity(Gravity.CENTER, 0, 0);

            // Setting the duration of the Toast
            toast.setDuration(Toast.LENGTH_SHORT);

            // Setting the Inflated Layout to the Toast
            toast.setView(layout);

            // Showing the Toast
            toast.show();
        }

        @Override
        protected void onPreExecute() {
            btnSubir.setEnabled(false);
            pd = ProgressDialog.show(Panel.this,"Subiendo...","Subiendo datos al servidor, espere por favor.");
        }

        @Override
        protected Void doInBackground(Void... params) {
            subirDatosToSQLServer();
            return null;
        }
    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }

    public void subirDatosToSQLServer(){

        if(!isAirplaneModeOn(getApplicationContext())){
            if(hayInternet(getApplicationContext())){
                DBHelper h = new DBHelper(Panel.this);
                SQLiteDatabase db = h.getReadableDatabase();
                SQLiteDatabase dbw = h.getWritableDatabase();

                Sincronizar sync = new Sincronizar();

                //SUBIR LOS PROSPECTOS NUEVOS
                ArrayList<Prospecto> prospectos = h.getProspectosNuevostoSQLServer(db);
                if(prospectos != null && prospectos.size() > 0){
                    for (Prospecto p : prospectos){
                        if(sync.agregarProspectoSQLServer(p) == 1){
                            Log.e("Prospecto","Agregado");
                        }else{
                            Log.e("Prospecto","Error al agregar");
                        }
                    }
                }else{
                    Log.e("Prospecto", "No hay prospectos nuevos");
                }


                ArrayList<Obra> obrasNuevas = h.getObrasNuevastoSQLServer(db);
                if(obrasNuevas != null && obrasNuevas.size() > 0){
                    ArrayList<Visita> visitasObraNueva;
                    for (Obra o : obrasNuevas){
                        Log.e("Obras", o.getPk_Obra() + " " + o.getNombreObra());
                        CorrelativoObra co = null;
                        if(sync.updateCorrelativoObra() == 1){
                            co = sync.getCorrelativoObra();
                            Log.e("GET Correlativo","Success");
                        }else{
                            Log.e("GET Correlativo","Error");
                        }

                        if(sync.agregarObraSQLServer(o, co.getNroCorrelativoObra())==1){
                            //h.updateObra(co.getNroCorrelativoObra(),o.getPk_Obra(), dbw);
                            for(ObraVendedor ov : h.getObrasVendedorNuevostoSQLServer(o.getPk_Obra(),db)){
                                //AGREGAR OBRA VENDEDOR
                                if(sync.agregarObraVendedorSQLServer(ov,o.getPk_Obra()) == 1){
                                    Log.e("ObraVendedor","Agregada");
                                }
                                Log.e("Obra Vendedor",ov.getFk_RutVendedor() + " " + ov.getFk_Obra());
                            }
                            //NUEVO
                            /*visitasObraNueva = h.getVisitasNuevasdeObratoSQLServer(o.getPk_Obra(), db);
                            if( visitasObraNueva != null && visitasObraNueva.size() > 0){
                                //Colocar Correlativo
                                int res = h.actualizarObraVisita(dbw,o.getPk_Obra(),co.getNroCorrelativoObra());
                                if(res == 1){
                                    Log.e("Visita","Se actualizo la FK OBRA de la visita");
                                }
                            }else{
                                Log.e("Visitas Obra nueva","No tiene visitas");
                            }*/

                            ArrayList<Visita> visitas2 = h.getVisitasNuevasdeObratoSQLServer(o.getPk_Obra(), db);

                            if(visitas2 != null && visitas2.size() > 0){
                                ArrayList<VisitaMotivo> vms;
                                for(Visita v : visitas2){
                                    //AGREGAR VISITA
                                    if(sync.agregarVisitaSQLServer(v) == 1){

                                        Log.e("Sinc Visita ObraNueva","Visita Obra Nueva Agregada");



                                        if(h.updateVisitaSubidas(v.getIdVisita(),db,dbw)== 1){
                                            Log.e("Visita a SQLite","UPDATE");
                                        }else{
                                            Log.e("Visita a SQLite","Error");
                                        }

                                        vms = h.getMotivosVisitasNuevasdeObra(v.getPk_Visita(),db);
                                        if(vms!=null && vms.size() > 0){
                                            int idVisita = sync.getIDVisitaAgregadaSQLServer();
                                            for(VisitaMotivo vm : vms){
                                                vm.setFk_Visita(idVisita);
                                                if(sync.agregarVisitaMotivoSQLServer(vm) == 1){
                                                    Log.e("Sincron VisitaMotivo","Se agregó VisitaMotivo");
                                                }
                                            }
                                        }else{
                                            Log.e("VISITAMOTIVO"," no hay visita motivos");
                                        }
                                        h.deleteVisita(v.getPk_Visita(), dbw);
                                        h.deleteVisitaMotivo(v.getPk_Visita(),dbw);
                                    }else{
                                        Log.e("SincVisita ObraNueva","No se agregó visita de obra nueva");
                                    }

                                }
                            }else{
                                Log.e("Sincronzacion Visitas", "No hay visitas");
                            }

                            //FIN NUEVO

                            h.deleteObra(o.getPk_Obra(),dbw);
                        }else{
                            Log.e("Sincronizacion Obra","Error");
                        }



                        /*visitasObraNueva = h.getVisitasNuevasdeObratoSQLServer(o.getPk_Obra(),db);
                        if( visitasObraNueva != null && visitasObraNueva.size() > 0){
                            //Colocar Correlativo
                            int res = h.actualizarObraVisita(dbw,o.getPk_Obra(),co.getNroCorrelativoObra());
                            if(res == 1){
                                Log.e("Visita","Se actualizo la FK OBRA de la visita");
                            }
                        }else{
                            Log.e("Visitas Obra nueva","No tiene visitas");
                        }*/

                    }

                }else{
                    Log.e("Obras","No hay obras");
                }


                ArrayList<Visita> visitas = h.getVisitasNuevasdeObratoSQLServer(0, db);

                if(visitas != null && visitas.size() > 0){
                    ArrayList<VisitaMotivo> vms;
                    for(Visita v : visitas){
                        //AGREGAR VISITA
                        if(sync.agregarVisitaSQLServer(v) == 1){

                            Log.e("Sincronizacion Visita","Visita Agregada");



                            if(h.updateVisitaSubidas(v.getIdVisita(),db,dbw)== 1){
                                Log.e("Visita a SQLite","UPDATE");
                            }else{
                                Log.e("Visita a SQLite","Error");
                            }

                            vms = h.getMotivosVisitasNuevasdeObra(v.getPk_Visita(),db);
                            if(vms!=null && vms.size() > 0){
                                int idVisita = sync.getIDVisitaAgregadaSQLServer();
                                for(VisitaMotivo vm : vms){
                                    vm.setFk_Visita(idVisita);
                                    if(sync.agregarVisitaMotivoSQLServer(vm) == 1){
                                        Log.e("Sincron VisitaMotivo","Se agregó VisitaMotivo");
                                    }
                                }
                            }else{
                                Log.e("VISITAMOTIVO"," no hay visita motivos");
                            }
                            h.deleteVisita(v.getPk_Visita(),dbw);
                            h.deleteVisitaMotivo(v.getPk_Visita(),dbw);
                        }else{
                            Log.e("Sincronizar Visita","No se agregó visita");
                        }

                    }
                }else{
                    Log.e("Sincronzacion Visitas", "No hay visitas");
                }



                dbw.execSQL("DELETE FROM ObraNew");
                //dbw.execSQL("DELETE FROM VisitaNew");
                //dbw.execSQL("DELETE FROM VisitaMotivoNew");
                dbw.execSQL("DELETE FROM ProspectoNew");
                dbw.execSQL("DELETE FROM ObraVendedorNew");

            }else{
                //Toast.makeText(getApplicationContext(),"No hay internet, no se pueden subir los datos.",Toast.LENGTH_LONG).show();
                // Inflating the layout for the toast
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.error_toast,
                        (ViewGroup) findViewById(R.id.toast_custom_error));

                // Typecasting and finding the view in the inflated layout
                TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                // Setting the text to be displayed in the Toast
                text.setText("No hay internet. No se\npueden subir los datos.");

                // Creating the Toast
                Toast toast = new Toast(getApplicationContext());

                // Setting the position of the Toast to centre
                toast.setGravity(Gravity.CENTER, 0, 0);

                // Setting the duration of the Toast
                toast.setDuration(Toast.LENGTH_SHORT);

                // Setting the Inflated Layout to the Toast
                toast.setView(layout);

                // Showing the Toast
                toast.show();
            }
        }else{
            //Toast.makeText(getApplicationContext(),"Está en modo avión, no se pueden guardar los datos",Toast.LENGTH_LONG).show();
            // Inflating the layout for the toast
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.error_toast,
                    (ViewGroup) findViewById(R.id.toast_custom_error));

            // Typecasting and finding the view in the inflated layout
            TextView text = (TextView) layout.findViewById(R.id.tvtoast);

            // Setting the text to be displayed in the Toast
            text.setText("Está en modo avión, no se \npueden guardar los datos.");

            // Creating the Toast
            Toast toast = new Toast(getApplicationContext());

            // Setting the position of the Toast to centre
            toast.setGravity(Gravity.CENTER, 0, 0);

            // Setting the duration of the Toast
            toast.setDuration(Toast.LENGTH_SHORT);

            // Setting the Inflated Layout to the Toast
            toast.setView(layout);

            // Showing the Toast
            toast.show();
        }

    }

    public class cargarDatosSync extends AsyncTask<Void,Void,Void>{
        ProgressDialog pd = null;
        boolean error = false;

        @Override
        protected void onPreExecute() {
            btnCargar.setEnabled(false);
            pd = ProgressDialog.show(Panel.this,"Cargando datos","Sincronizando con el servidor, espere por favor.");
        }

        @Override
        protected Void doInBackground(Void... params) {
            DBHelper h = new DBHelper(Panel.this);
            SQLiteDatabase db = h.getReadableDatabase();
            ArrayList<Cliente> bClientes = h.verClientes(db);
            ArrayList<Obra> bObras = h.verObras(db);
            ArrayList<Visita> bVisitas = h.verVisitas(db);
            try {
                VendedorDAO vDAO;


                vDAO = new VendedorDAO(vendedor.getPk_RutVendedor());


                //Obtener fecha Servidor formato 01-01-2015
                FechaHoy = vDAO.getFechaServidor();
                //Agregar Clientes a SQLite
                DBHelper helper = new DBHelper(Panel.this);
                SQLiteDatabase dbw = helper.getWritableDatabase();
                try {
                    dbw.execSQL("DELETE FROM Cliente");
                    dbw.execSQL("DELETE FROM Obra");
                    //dbw.execSQL("DELETE FROM ObraNew");
                    dbw.execSQL("DELETE FROM Motivo");
                    //COMENTAR SI NO
                    dbw.execSQL("DELETE FROM Visita WHERE Subida = 1");
                    //dbw.execSQL("DELETE FROM VisitaNew");
                    //COMENTAR SINO
                    //dbw.execSQL("DELETE FROM VisitaMotivo");
                    //dbw.execSQL("DELETE FROM VisitaMotivoNew");
                    //dbw.execSQL("DELETE FROM ProspectoNew");
                    //dbw.execSQL("DELETE FROM ObraVendedorNew");
                } catch (SQLiteException ex) {

                }
                for (Cliente cl : vDAO.getClientesVendedor()) {
                    helper.agregarClientesaSQLite(cl, dbw);
                    Log.e("AGREGADOCliente", "Cliente agregado: " + cl.getNombreCliente());
                    //Añadimos obras de cada cliente
                    int sizeObrasCliente = vDAO.getObrasporCliente(cl.getPk_RutCliente()).size();
                    Log.e("CANTIDAD OBRAS", "Cliente " + cl.getNombreCliente() + " tiene " + sizeObrasCliente);
                    if (sizeObrasCliente > 0) {
                        for (Obra o : vDAO.getObrasporCliente(cl.getPk_RutCliente())) {
                            int r = helper.agregarObrasSQLite(o, dbw);
                            Log.e("ObraCliente", "Obra agregada: " + o.getNombreObra() + " de " + o.getFk_RutCliente());
                            if (r == 1)
                                Log.e("Obra agregada", "Obra agregada: " + o.getNombreObra());
                            else
                                Log.e("Obra no agregada", "Obra no agregada: " + o.getNombreObra());
                        }
                    } else {
                        if (helper.eliminarClienteSQLite(cl.getPk_RutCliente(), dbw) == 1) {
                            Log.e("Cliente eliminado", "Cliente eliminado por no tener obras");
                        }
                    }
                }
                ArrayList<Obra> obrasN = h.getObrasNuevastoSQLServer(db);
                if(obrasN != null && obrasN.size() > 0){
                    for(Obra ob : obrasN){
                        h.agregarObrasSQLite(ob,dbw);
                    }
                }
                //Agregamos los motivos a SQLite
                for (cl.zonagps.zonaventagps.clases.Motivo m : vDAO.getMotivos()) {
                    helper.agregarMotivosaSQLite(m, dbw);
                }

                //Obtener el ultimo numero Correlativo de Obra
                helper.eliminarCorrelativoObra(dbw);
                helper.insertarCorrelativoObra(vDAO.getCorrelativoObra(), dbw);

                //Agregamos las Visitas a SQLite
                //COMENTAR SINO

                for (Visita vis : vDAO.getVisitasdelVendedor()) {
                    int res = helper.agregarVisitasSQLite(vis, dbw);
                    if (res == 1)
                        Log.e("Visita agregada", "Visita " + vis.getPk_Visita());
                }


            /*//NUEVO
            DBHelper h = new DBHelper(Panel.this);
            SQLiteDatabase dbr = h.getReadableDatabase();
            ArrayList<Visita> visitas_sqlite = h.verVisitas(dbr);
            int sizeVisitasSQLite = 0;
            if(visitas_sqlite != null){
                sizeVisitasSQLite = visitas_sqlite.size();
            }

            ArrayList<Visita> visitas_nube = vDAO.getVisitasdelVendedor();
            int sizeVisitasNube = 0;
            if(visitas_nube != null){
                sizeVisitasNube = visitas_nube.size();
            }
            //if (sizeVisitasNube != sizeVisitasSQLite) {
                dbw.execSQL("DELETE FROM Visita");
                for (Visita vis : visitas_nube) {
                    int res = helper.agregarVisitasSQLite(vis, dbw);
                    if (res == 1)
                        Log.e("Visita agregada", "Visita " + vis.getPk_Visita() + " " + vis.getFechaVisita());
                }
           // }*/
            }catch (Exception e){
                error = true;
                SQLiteDatabase dbw = h.getWritableDatabase();
                try{

                    dbw.execSQL("DELETE FROM Cliente");
                }catch(SQLiteException ex){
                    Log.e("SQLiteException","Error eliminar clientes");
                }

                for(Cliente c : bClientes){
                    h.agregarClientesaSQLite(c,dbw);
                }

                try{
                    dbw.execSQL("DELETE FROM Obra");
                }catch(SQLiteException ex){
                    Log.e("SQLiteException","Error eliminar obras");
                }

                for(Obra o : bObras){
                    Log.e("Obra",o.getPk_Obra() + "");
                    h.agregarObrasSQLite(o,dbw);
                }

                try{
                    dbw.execSQL("DELETE FROM Visita");
                }catch (SQLiteException ex){
                    Log.e("SQLiteException","ERROR ELIMINAR VISITAS");
                }

                for(Visita v: bVisitas){
                    int sub = v.getSubida();
                    if(sub == 1){
                        h.agregarVisitasSQLite(v,dbw);
                    }else{
                        h.agregarVisitasNoSubidasSQLite(v,dbw);
                    }

                }
            }




            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            btnCargar.setEnabled(true);

            pd.dismiss();

            if(error){
                Toast.makeText(Panel.this,"Hubo un error, se trabajará con la última sincronización",Toast.LENGTH_LONG).show();
            }

            recreate();
            Intent intent = getIntent();
            //intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
            finish();
            //overridePendingTransition(0, 0);
            startActivity(intent);
            //overridePendingTransition(0, 0);


        }
    }

    public Boolean isOnline() {
        try {
            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal==0);
            return reachable;
        } catch (Exception e) {
            Log.e("Error",e.getMessage());
            e.printStackTrace();
        }
        return false;
    }


    public static boolean verificaInternet(Context ctx) {

        boolean bConectado = false;
        ConnectivityManager connec = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        // No sólo wifi, también GPRS
        NetworkInfo[] redes = connec.getAllNetworkInfo();
        // este bucle debería no ser tan ñapa
        for (int i = 0; i < 2; i++) {
            // ¿Tenemos conexión? ponemos a true
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                bConectado = true;
            }
        }
        return bConectado;
    }

    public static boolean hayInternet(Context ctx) {
        /*if (verificaInternet(ctx)) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                Log.e("INTERNET", "Error checking internet connection", e);
                e.printStackTrace();
            }
        } else {
            Log.e("INTERNET", "RED NO DISPONIBLE");
        }
        return false;*/
        boolean bConectado = false;
        ConnectivityManager connec = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        // No sólo wifi, también GPRS
        NetworkInfo[] redes = connec.getAllNetworkInfo();
        // este bucle debería no ser tan ñapa
        for (int i = 0; i < 2; i++) {
            // ¿Tenemos conexión? ponemos a true
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                bConectado = true;
            }
        }
        return bConectado;
    }

}

