package cl.zonagps.zonaventagps;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import cl.zonagps.zonaventagps.DAO.Sincronizar;
import cl.zonagps.zonaventagps.clases.*;
import cl.zonagps.zonaventagps.clases.Visita;

public class SyncService extends Service {

    private static Timer timer = new Timer();
    private Context ctx;
    private Connection conn;

    public SyncService() {

    }

    @Override
    public void onCreate() {
        super.onCreate();
        ctx = this;
        startService();
        Log.e("Servicio Sync","Creado");
    }

    private void startService()
    {
        timer.scheduleAtFixedRate(new mainTask(), 0, 900000); //cada media hora
    }

    private class mainTask extends TimerTask
    {
        public void run()
        {
            toastHandler.sendEmptyMessage(0);
        }
    }

    private final Handler toastHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            //Codigo a ejectuar cada 30 min
            new syncSubirDatosSQLServer().execute();
        }
    };

    public class syncSubirDatosSQLServer extends AsyncTask<Void,Void,Void>{

        @Override
        protected void onPreExecute() {
            Log.e("Sincronizacion","Empezando");
        }

        @Override
        protected Void doInBackground(Void... params) {
            subirDatosToSQLServer();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Log.e("Sincronizacion","Sincronizacion terminada");
        }
    }

    public void subirDatosToSQLServer(){

        if(!isAirplaneModeOn(getApplicationContext())){
            if(hayInternet(getApplicationContext()) && isOnline()){
                DBHelper h = new DBHelper(getApplicationContext());
                SQLiteDatabase db = h.getReadableDatabase();
                SQLiteDatabase dbw = h.getWritableDatabase();

                Sincronizar sync = new Sincronizar();

                //SUBIR LOS PROSPECTOS NUEVOS
                ArrayList<Prospecto> prospectos = h.getProspectosNuevostoSQLServer(db);
                if(prospectos != null && prospectos.size() > 0){
                    for (Prospecto p : prospectos){
                        if(sync.agregarProspectoSQLServer(p) == 1){
                            Log.e("Prospecto","Agregado");
                        }else{
                            Log.e("Prospecto","Error al agregar");
                        }
                    }
                }else{
                    Log.e("Prospecto", "No hay prospectos nuevos");
                }


                ArrayList<Obra> obrasNuevas = h.getObrasNuevastoSQLServer(db);
                if(obrasNuevas != null && obrasNuevas.size() > 0){
                    ArrayList<Visita> visitasObraNueva;
                    for (Obra o : obrasNuevas){
                        Log.e("Obras", o.getPk_Obra() + " " + o.getNombreObra());
                        CorrelativoObra co = null;
                        if(sync.updateCorrelativoObra() == 1){
                            co = sync.getCorrelativoObra();
                            Log.e("GET Correlativo","Success");
                        }else{
                            Log.e("GET Correlativo","Error");
                        }

                        if(sync.agregarObraSQLServer(o, co.getNroCorrelativoObra())==1){
                            //h.updateObra(co.getNroCorrelativoObra(),o.getPk_Obra(), dbw);
                            for(ObraVendedor ov : h.getObrasVendedorNuevostoSQLServer(o.getPk_Obra(),db)){
                                //AGREGAR OBRA VENDEDOR
                                if(sync.agregarObraVendedorSQLServer(ov,o.getPk_Obra()) == 1){
                                    Log.e("ObraVendedor","Agregada");
                                }
                                Log.e("Obra Vendedor",ov.getFk_RutVendedor() + " " + ov.getFk_Obra());
                            }
                            //NUEVO
                            /*visitasObraNueva = h.getVisitasNuevasdeObratoSQLServer(o.getPk_Obra(), db);
                            if( visitasObraNueva != null && visitasObraNueva.size() > 0){
                                //Colocar Correlativo
                                int res = h.actualizarObraVisita(dbw,o.getPk_Obra(),co.getNroCorrelativoObra());
                                if(res == 1){
                                    Log.e("Visita","Se actualizo la FK OBRA de la visita");
                                }
                            }else{
                                Log.e("Visitas Obra nueva","No tiene visitas");
                            }*/

                            ArrayList<Visita> visitas2 = h.getVisitasNuevasdeObratoSQLServer(o.getPk_Obra(), db);

                            if(visitas2 != null && visitas2.size() > 0){
                                ArrayList<VisitaMotivo> vms;
                                for(Visita v : visitas2){
                                    //AGREGAR VISITA
                                    if(sync.agregarVisitaSQLServer(v) == 1){

                                        Log.e("Sinc Visita ObraNueva","Visita Obra Nueva Agregada");



                                        if(h.updateVisitaSubidas(v.getIdVisita(),db,dbw)== 1){
                                            Log.e("Visita a SQLite","UPDATE");
                                        }else{
                                            Log.e("Visita a SQLite","Error");
                                        }

                                        vms = h.getMotivosVisitasNuevasdeObra(v.getPk_Visita(),db);
                                        if(vms!=null && vms.size() > 0){
                                            int idVisita = sync.getIDVisitaAgregadaSQLServer();
                                            for(VisitaMotivo vm : vms){
                                                vm.setFk_Visita(idVisita);
                                                if(sync.agregarVisitaMotivoSQLServer(vm) == 1){
                                                    Log.e("Sincron VisitaMotivo","Se agregó VisitaMotivo");
                                                }
                                            }
                                        }else{
                                            Log.e("VISITAMOTIVO"," no hay visita motivos");
                                        }
                                        h.deleteVisita(v.getPk_Visita(), dbw);
                                        h.deleteVisitaMotivo(v.getPk_Visita(),dbw);
                                    }else{
                                        Log.e("SincVisita ObraNueva","No se agregó visita de obra nueva");
                                    }

                                }
                            }else{
                                Log.e("Sincronzacion Visitas", "No hay visitas");
                            }

                            //FIN NUEVO

                            h.deleteObra(o.getPk_Obra(),dbw);
                        }else{
                            Log.e("Sincronizacion Obra","Error");
                        }



                        /*visitasObraNueva = h.getVisitasNuevasdeObratoSQLServer(o.getPk_Obra(),db);
                        if( visitasObraNueva != null && visitasObraNueva.size() > 0){
                            //Colocar Correlativo
                            int res = h.actualizarObraVisita(dbw,o.getPk_Obra(),co.getNroCorrelativoObra());
                            if(res == 1){
                                Log.e("Visita","Se actualizo la FK OBRA de la visita");
                            }
                        }else{
                            Log.e("Visitas Obra nueva","No tiene visitas");
                        }*/

                    }

                }else{
                    Log.e("Obras","No hay obras");
                }


                ArrayList<Visita> visitas = h.getVisitasNuevasdeObratoSQLServer(0, db);

                if(visitas != null && visitas.size() > 0){
                    ArrayList<VisitaMotivo> vms;
                    for(Visita v : visitas){
                        //AGREGAR VISITA
                        if(sync.agregarVisitaSQLServer(v) == 1){

                            Log.e("Sincronizacion Visita","Visita Agregada");



                            if(h.updateVisitaSubidas(v.getIdVisita(),db,dbw)== 1){
                                Log.e("Visita a SQLite","UPDATE");
                            }else{
                                Log.e("Visita a SQLite","Error");
                            }

                            vms = h.getMotivosVisitasNuevasdeObra(v.getPk_Visita(),db);
                            if(vms!=null && vms.size() > 0){
                                int idVisita = sync.getIDVisitaAgregadaSQLServer();
                                for(VisitaMotivo vm : vms){
                                    vm.setFk_Visita(idVisita);
                                    if(sync.agregarVisitaMotivoSQLServer(vm) == 1){
                                        Log.e("Sincron VisitaMotivo","Se agregó VisitaMotivo");
                                    }
                                }
                            }else{
                                Log.e("VISITAMOTIVO"," no hay visita motivos");
                            }
                            h.deleteVisita(v.getPk_Visita(),dbw);
                            h.deleteVisitaMotivo(v.getPk_Visita(),dbw);
                        }else{
                            Log.e("Sincronizar Visita","No se agregó visita");
                        }

                    }
                }else{
                    Log.e("Sincronzacion Visitas", "No hay visitas");
                }



                dbw.execSQL("DELETE FROM ObraNew");
                //dbw.execSQL("DELETE FROM VisitaNew");
                //dbw.execSQL("DELETE FROM VisitaMotivoNew");
                dbw.execSQL("DELETE FROM ProspectoNew");
                dbw.execSQL("DELETE FROM ObraVendedorNew");

            }else{
                Log.e("Sincronizacion","No hay internet");
            }
        }else{
            Log.e("Modo avión","El modo avión está activado");
        }

    }

    @SuppressWarnings("deprecation")
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public static boolean isAirplaneModeOn(Context context) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }

    public Boolean isOnline() {
        try {
            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal==0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }


    public static boolean verificaInternet(Context ctx) {

        boolean bConectado = false;
        ConnectivityManager connec = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        // No sólo wifi, también GPRS
        NetworkInfo[] redes = connec.getAllNetworkInfo();
        // este bucle debería no ser tan ñapa
        for (int i = 0; i < 2; i++) {
            // ¿Tenemos conexión? ponemos a true
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                bConectado = true;
            }
        }
        return bConectado;
    }

    public static boolean hayInternet(Context ctx) {
        /*if (verificaInternet(ctx)) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                Log.e("INTERNET", "Error checking internet connection", e);
                e.printStackTrace();
            }
        } else {
            Log.e("INTERNET", "RED NO DISPONIBLE");
        }
        return false;*/
        boolean bConectado = false;
        ConnectivityManager connec = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        // No sólo wifi, también GPRS
        NetworkInfo[] redes = connec.getAllNetworkInfo();
        // este bucle debería no ser tan ñapa
        for (int i = 0; i < 2; i++) {
            // ¿Tenemos conexión? ponemos a true
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                bConectado = true;
            }
        }
        return bConectado;
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }



}
