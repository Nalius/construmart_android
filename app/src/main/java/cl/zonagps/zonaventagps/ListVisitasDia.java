package cl.zonagps.zonaventagps;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import cl.zonagps.zonaventagps.clases.Cliente;
import cl.zonagps.zonaventagps.clases.Obra;
import cl.zonagps.zonaventagps.clases.Visita;

/**
 * Created by LEONEL CARRASCO
 */
public class ListVisitasDia extends BaseAdapter {

    private final Activity actividad;
    private final ArrayList<Visita> lista;

    public ListVisitasDia(Activity actividad, ArrayList<Visita> lista) {
        super();
        this.actividad = actividad;
        this.lista = lista;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Object getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = actividad.getLayoutInflater();
        View view = inflater.inflate(R.layout.item_visita, null,
                true);
        TextView t = (TextView)view.findViewById(R.id.lblItemVisitaNombreCliente);
        DBHelper h = new DBHelper(actividad);
        SQLiteDatabase db = h.getReadableDatabase();

        if(lista.get(position).getSubida() == 0){

            ((ImageView)view.findViewById(R.id.imgVisitaCheck)).setImageResource(R.drawable.ic_action_action_done);
        }else{

            ((ImageView)view.findViewById(R.id.imgVisitaCheck)).setImageResource(R.drawable.ic_action_action_done_all);
        }

        Cliente cl = h.verCliente(lista.get(position).getFk_RutCliente(),db);
        if(cl != null){
            t.setText("Nombre: " + cl.getNombreCliente());
        }else{
            t.setText("Nombre: ");
        }

        t = (TextView) view.findViewById(R.id.lblItemVisitaRutCliente);
        t.setText("RUT: " +lista.get(position).getFk_RutCliente());
        t = (TextView)view.findViewById(R.id.lblItemVisitaNombreObra);
        Obra o = h.verObra(lista.get(position).getFk_Obra(),db);
        if(o!=null){
            t.setText("Obra: " + o.getNombreObra() );
        }else{
            t.setText("Obra: ");
        }

        return view;
    }
}
