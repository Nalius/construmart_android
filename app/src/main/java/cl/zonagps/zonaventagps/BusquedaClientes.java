package cl.zonagps.zonaventagps;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

import cl.zonagps.zonaventagps.clases.Cliente;
import cl.zonagps.zonaventagps.clases.Vendedor;


public class BusquedaClientes extends ActionBarActivity {

    ArrayList<Cliente> clientes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_busqueda_clientes);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        String filtro = getIntent().getExtras().getString("OPCION");
        String valor = getIntent().getExtras().getString("VALORBUSQUEDA");
        final Vendedor vendedor = (Vendedor)getIntent().getExtras().getSerializable("Vendedor");
        if(vendedor!=null){
            Log.e("Vendedor","Existe");
        }else{
            Log.e("Vendedor","nulo");
        }

        DBHelper helper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = helper.getReadableDatabase();
        boolean opcion;
        if(filtro.equals("RUT")){
            opcion = true;
        }else{
            opcion = false;
        }

        if(valor.isEmpty() || valor.equals("")){
            clientes = helper.verClientes(db);
        }else{
            clientes = helper.buscarClientes(opcion,db,valor);
        }
        ListView listaClientes = (ListView)findViewById(R.id.lvListaClientes);
        if(clientes == null || clientes.size() == 0){
            ((TextView)findViewById(R.id.lblNoResults)).setVisibility(View.VISIBLE);
        }else{
            ((TextView)findViewById(R.id.lblNoResults)).setVisibility(View.INVISIBLE);
            listaClientes.setAdapter(new ListClientesAdapter(this, clientes));
        }




        listaClientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Cliente cl = clientes.get(position);
                Intent i = new Intent(getApplicationContext(),Entregas.class);
                i.putExtra("Cliente", (Serializable)cl);
                i.putExtra("Vendedor",  (Serializable)vendedor);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_busqueda_clientes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            Intent i = new Intent();
            i.setData(Uri.parse("http://www.zonagps.cl/construmart/manual.pdf"));
            startActivity(i);
            return true;
        }
        if(id == R.id.action_salir){
            if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN){
                finishAffinity();
            }
        }
        if(id == R.id.action_about){
            Intent i = new Intent(getApplicationContext(),Acerca.class);
            startActivity(i);
        }

        if(id == R.id.btnVolver)
            finish();

        return super.onOptionsItemSelected(item);
    }
}
