package cl.zonagps.zonaventagps;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by LEONEL CARRASCO
 */
public class PagerAdapter extends FragmentPagerAdapter {

    public PagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public Fragment getItem(int arg0) {
        switch (arg0) {
            case 0:
                return new VerClientes();
            case 1:
                return new NuevoProspecto();
            default:
                return null;
        }
    }

    public int getCount() {
        return 2;
    }

}