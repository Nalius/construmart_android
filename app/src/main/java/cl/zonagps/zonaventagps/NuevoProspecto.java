package cl.zonagps.zonaventagps;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import cl.zonagps.zonaventagps.clases.AuxClass;
import cl.zonagps.zonaventagps.clases.Prospecto;
import cl.zonagps.zonaventagps.clases.Vendedor;


public class NuevoProspecto extends Fragment {
    View view;
    Vendedor vendedor;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =  inflater.inflate(R.layout.fragment_nuevo_prospecto, container, false);
        //vendedor = (Vendedor)getActivity().getIntent().getExtras().getSerializable("Vendedor");
        vendedor = AuxClass.getV();
        view = v;
        nuevoProspecto(v,vendedor);

        return v;
    }

    private void nuevoProspecto(View v, final Vendedor vendedor) {
        Button agregarProspecto = (Button)v.findViewById(R.id.btnAgregarProspecto);

        agregarProspecto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String rutEmpresa = ((EditText) view.findViewById(R.id.txtProspectoRutEmpresa)).getText().toString();
                String nombreEmpresa = ((EditText) view.findViewById(R.id.txtProspectoNombreEmpresa)).getText().toString();
                String encargado = ((EditText) view.findViewById(R.id.txtProspectoEncargadoContacto)).getText().toString();
                String direccion = ((EditText) view.findViewById(R.id.txtProspectoDireccion)).getText().toString();
                String correo = ((EditText) view.findViewById(R.id.txtProspectoCorreo)).getText().toString();
                String telefono = ((EditText) view.findViewById(R.id.txtProspectoTelefono)).getText().toString();
                String comentario = ((EditText) view.findViewById(R.id.txtProspectoComentario)).getText().toString();

                if (rutEmpresa.equals("") || nombreEmpresa.equals("") || encargado.equals("") || direccion.equals("") || correo.equals("") || telefono.equals("") || comentario.equals("")) {
                    //Toast.makeText(getActivity(), "Todos los campos son requeridos", Toast.LENGTH_LONG).show();
                    // Inflating the layout for the toast
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View layout = inflater.inflate(R.layout.error_toast,
                            (ViewGroup)view.findViewById(R.id.toast_custom_error));

                    // Typecasting and finding the view in the inflated layout
                    TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                    // Setting the text to be displayed in the Toast
                    text.setText("Todos los campos son\nrequeridos.");

                    // Creating the Toast
                    Toast toast = new Toast(getActivity());

                    // Setting the position of the Toast to centre
                    toast.setGravity(Gravity.CENTER, 0, 0);

                    // Setting the duration of the Toast
                    toast.setDuration(Toast.LENGTH_SHORT);

                    // Setting the Inflated Layout to the Toast
                    toast.setView(layout);

                    // Showing the Toast
                    toast.show();
                } else {
                    Prospecto p = new Prospecto();

                    p.setPk_RutVendedor(vendedor.getPk_RutVendedor());
                    p.setRutCliente(rutEmpresa);
                    p.setNombreCliente(nombreEmpresa);
                    p.setEncargado(encargado);
                    p.setDireccion(direccion);
                    p.setTelefono(telefono);
                    p.setEmail(correo);
                    p.setComentario(comentario);
                    p.setFechaIngreso(getDate());
                    p.setFk_Estado(0);

                    DBHelper h = new DBHelper(getActivity());
                    SQLiteDatabase db = h.getWritableDatabase();

                    int res = h.insertarProspecto(db,p);

                    if(res == 1){
                        //Toast.makeText(getActivity(),"Nuevo prospecto agregado",Toast.LENGTH_LONG).show();
                        // Inflating the layout for the toast
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View layout = inflater.inflate(R.layout.ok_toast,
                                (ViewGroup) view.findViewById(R.id.toast_custom_ok));

                        // Typecasting and finding the view in the inflated layout
                        TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                        // Setting the text to be displayed in the Toast
                        text.setText("Nuevo prospecto agregado.");

                        // Creating the Toast
                        Toast toast = new Toast(getActivity());

                        // Setting the position of the Toast to centre
                        toast.setGravity(Gravity.CENTER, 0, 0);

                        // Setting the duration of the Toast
                        toast.setDuration(Toast.LENGTH_SHORT);

                        // Setting the Inflated Layout to the Toast
                        toast.setView(layout);

                        // Showing the Toast
                        toast.show();

                        ((EditText)view.findViewById(R.id.txtProspectoRutEmpresa)).setText("");
                        ((EditText)view.findViewById(R.id.txtProspectoNombreEmpresa)).setText("");
                        ((EditText)view.findViewById(R.id.txtProspectoEncargadoContacto)).setText("");
                        ((EditText)view.findViewById(R.id.txtProspectoDireccion)).setText("");
                        ((EditText)view.findViewById(R.id.txtProspectoCorreo)).setText("");
                        ((EditText)view.findViewById(R.id.txtProspectoTelefono)).setText("");
                        ((EditText)view.findViewById(R.id.txtProspectoComentario)).setText("");

                    }else{
                        //Toast.makeText(getActivity(),"Error al agregar prospecto",Toast.LENGTH_LONG).show();
                        // Inflating the layout for the toast
                        LayoutInflater inflater = getActivity().getLayoutInflater();
                        View layout = inflater.inflate(R.layout.error_toast,
                                (ViewGroup) view.findViewById(R.id.toast_custom_error));

                        // Typecasting and finding the view in the inflated layout
                        TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                        // Setting the text to be displayed in the Toast
                        text.setText("Error al agregar prospecto");

                        // Creating the Toast
                        Toast toast = new Toast(getActivity());

                        // Setting the position of the Toast to centre
                        toast.setGravity(Gravity.CENTER, 0, 0);

                        // Setting the duration of the Toast
                        toast.setDuration(Toast.LENGTH_SHORT);

                        // Setting the Inflated Layout to the Toast
                        toast.setView(layout);

                        // Showing the Toast
                        toast.show();
                    }
                }
            }
        });
    }


    public String getDate(){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String date = df.format(Calendar.getInstance().getTime());
        return  date;
    }

}
