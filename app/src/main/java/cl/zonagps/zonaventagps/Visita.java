package cl.zonagps.zonaventagps;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import cl.zonagps.zonaventagps.clases.Cliente;


public class Visita extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visita);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#006e98")));

        cl.zonagps.zonaventagps.clases.Visita visita = (cl.zonagps.zonaventagps.clases.Visita) getIntent().getExtras().getSerializable("Visita");

        EditText rutCliente = (EditText)findViewById(R.id.txtVisitaRut);

        rutCliente.setText(visita.getFk_RutCliente() + "");

        DBHelper h = new DBHelper(getApplicationContext());
        SQLiteDatabase db = h.getReadableDatabase();

        Cliente cl = h.verCliente(visita.getFk_RutCliente(), db);

        EditText nombreCliente = (EditText)findViewById(R.id.txtVisitaNombre);
        nombreCliente.setText(cl.getNombreCliente());

        EditText fechaVisita = (EditText)findViewById(R.id.txtVisitaFechaVisita);
        fechaVisita.setText(visita.getFechaVisita());

        EditText observacionVisita = (EditText)findViewById(R.id.txtVisitaObservacion);
        observacionVisita.setText(visita.getObservacion());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_visita, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            Intent i = new Intent();
            i.setData(Uri.parse("http://www.zonagps.cl/construmart/manual.pdf"));
            startActivity(i);
            return true;
        }
        if(id == R.id.action_about){
            Intent i = new Intent(getApplicationContext(),Acerca.class);
            startActivity(i);
        }
        if(id == R.id.action_salir){
            if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN){
                finishAffinity();
            }
        }
        if(id == R.id.btnVolver)
            finish();

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
