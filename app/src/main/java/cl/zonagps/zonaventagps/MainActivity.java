package cl.zonagps.zonaventagps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import cl.zonagps.zonaventagps.DAO.Conectar;


public class MainActivity extends Activity{

    String version;
    String licencia;
    boolean conexionServidor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Thread timer = new Thread() {
            public void run(){
                try{
                    sleep(10);
                }catch(InterruptedException e){
                    e.printStackTrace();
                }catch(Exception e) {
                    e.printStackTrace();
                }
                finally
                {
                    if(verificaInternet(getApplicationContext())) {
                        new syncValidarVersionyLicencia().execute();
                    }else{
                        Intent i = new Intent(getApplicationContext(), Login.class);
                        startActivity(i);
                    }
                }
            }

        };
        timer.start();
    }



    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
        finish();
    }

    public class syncValidarVersionyLicencia extends AsyncTask<Void,Void,Void>{

        @Override
        protected Void doInBackground(Void... params) {
            conexionServidor = conexionServidor();
            if(conexionServidor)
                version = validarNuevaVersion();

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
                if(conexionServidor){
                    if(LicenciaAlDia()){
                        if(version.equals("")){
                            Intent i = new Intent(getApplicationContext(), Login.class);
                            startActivity(i);
                        }else{
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MainActivity.this);
                            alertDialogBuilder.setTitle("Nueva versión...");
                            alertDialogBuilder.setCancelable(false);
                            alertDialogBuilder.setMessage("Existe una nueva versión de la aplicación.").setNeutralButton("Descargar", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent i = new Intent();
                                    i.setData(Uri.parse(version));
                                    startActivity(i);
                                }
                            });
                            alertDialogBuilder.show();
                        }
                    }else{
                        Log.e("Licencia","Licencia agotada");
                        finish();
                    }
                }else{
                    Log.e("Servidor","No hay conexion con el servidor");
                    Intent i = new Intent(getApplicationContext(), Login.class);
                    startActivity(i);
                }

        }


    }

    public boolean LicenciaAlDia(){
        boolean isTrue = false;
        Date fechaActual = new Date();
        SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
        String fechaSistema = formateador.format(fechaActual);

        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date fechalicencia = format.parse(licencia);
            Date fechaAndroid = format.parse(fechaSistema);
            if(fechaAndroid.before(fechalicencia)){
                isTrue = true;
            }
        } catch (ParseException e) {
            Log.e("ERROR","ERROR LICENCIA ADIA");
            e.printStackTrace();
        }catch (NullPointerException e){
            isTrue = true;
        }

        return isTrue;
    }

    public static boolean verificaInternet(Context ctx) {
        boolean bConectado = false;
        ConnectivityManager connec = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        // No sólo wifi, también GPRS
        NetworkInfo[] redes = connec.getAllNetworkInfo();
        // este bucle debería no ser tan ñapa
        for (int i = 0; i < 2; i++) {
            // ¿Tenemos conexión? ponemos a true
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                bConectado = true;
            }
        }
        return bConectado;
    }

    public String validarNuevaVersion()
    {
        System.out.println("Validando nueva");
        try
        {
            String INFO_FILE = "http://boxapp.cl/zonagps/versionZV.txt";
            //int versionCode = getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
            int versionCode = BuildConfig.VERSION_CODE;
            String versionName = getPackageManager().getPackageInfo(getPackageName(), 0).versionName;
            String data = downloadHttp(new URL(INFO_FILE));
            JSONObject json = new JSONObject(data);
            int latestVersionCode = json.getInt("versionCode");
            String latestVersionName = json.getString("versionName");
            String downloadURL = json.getString("downloadURL");
            licencia = json.getString("license");
            if(versionCode != latestVersionCode)
                return downloadURL;
            else
                return "";
        } catch (PackageManager.NameNotFoundException e) {
            System.out.println("NameNotFoundException: " + e);
            Log.e("Error", "Valida version");
            return "";
        } catch (MalformedURLException e) {
            System.out.println("MalformedURLException: " + e);
            Log.e("Error", "Valida version");
            return "";
        } catch (IOException e) {
            System.out.println("IOException: " + e);
            Log.e("Error", "Valida version");
            return "";
        } catch (JSONException e) {
            System.out.println("JSONException: " + e);
            Log.e("Error", "Valida version");
            return "";
        } catch (Exception e) {
            System.out.println("Exception: " + e);
            Log.e("Error", "Valida version");
            return "";
        }
    }

    private String downloadHttp(URL url) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        try
        {
            HttpURLConnection c = (HttpURLConnection)url.openConnection();
            c.setRequestMethod("GET");
            c.setReadTimeout(15 * 1000);
            c.setUseCaches(false);
            c.connect();
            BufferedReader reader = new BufferedReader(new InputStreamReader(c.getInputStream()));
            String line = null;
            while((line = reader.readLine()) != null){
                stringBuilder.append(line + "\n");
            }
            return stringBuilder.toString();
        }
        catch(Exception ex)
        {
            System.out.println("Exception: " + ex);
            return stringBuilder.toString();
        }
    }

    public boolean conexionServidor(){
        Connection c = Conectar.GetConnection();
        if(c==null)
            return false;
        else
            return true;
    }


}
