package cl.zonagps.zonaventagps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;

import cl.zonagps.zonaventagps.DAO.VendedorDAO;
import cl.zonagps.zonaventagps.clases.AuxClass;
import cl.zonagps.zonaventagps.clases.Cliente;
import cl.zonagps.zonaventagps.clases.CorrelativoObra;
import cl.zonagps.zonaventagps.clases.Obra;
import cl.zonagps.zonaventagps.clases.ObraVendedor;
import cl.zonagps.zonaventagps.clases.Vendedor;


public class NuevaObra extends ActionBarActivity {

    Vendedor vendedor;
    Cliente cl;
    CorrelativoObra co;
    private Location locObra;
    private LocationManager locManager;
    private LocationListener locListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nueva_obra);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#006e98")));

        //Obtenemos una referencia al LocationManager
        locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        //Obtenemos la última posición conocida
        Location loc = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(loc == null){
            loc = locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        locObra = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(locObra == null){
            locObra = locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        locListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                locObra = location;
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.e("GPS","GPS OFF");
                Toast.makeText(getApplicationContext(),"Desactivó el GPS no podrá guardar la visita",Toast.LENGTH_LONG).show();
            }
        };

        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,3000,0,locListener);
        locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,3000,0,locListener);




    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_nueva_obra, menu);
        return true;
    }
    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Finalizar");
        alertDialogBuilder.setMessage("Desea volver sin guardar?").setPositiveButton("Si", new DialogInterface.OnClickListener()  {
            public void onClick(DialogInterface dialog, int id)
            {
                finish();
            }}).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alertDialogBuilder.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            Intent i = new Intent();
            i.setData(Uri.parse("http://www.zonagps.cl/construmart/manual.pdf"));
            startActivity(i);
            return true;
        }
        if(id == R.id.action_about){
            Intent i = new Intent(getApplicationContext(),Acerca.class);
            startActivity(i);
        }



        if(id == R.id.action_salir){

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Finalizar");
            alertDialogBuilder.setMessage("Desea finalizar el programa sin guardar?").setPositiveButton("Si", new DialogInterface.OnClickListener()  {
                public void onClick(DialogInterface dialog, int id)
                {
                    if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
                        finishAffinity();
                    }
                }}).setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            alertDialogBuilder.show();

        }

        if(id == R.id.btnGuardarDatos){
            LocationManager location = (LocationManager)getSystemService(LOCATION_SERVICE);
            if(location.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                String nombre = ((TextView) findViewById(R.id.txtObraNombre)).getText().toString();
                String contacto = ((TextView) findViewById(R.id.txtObraContacto)).getText().toString();
                String direccion = ((TextView) findViewById(R.id.txtObraDireccion)).getText().toString();
                String telefono = ((TextView) findViewById(R.id.txtObraTelefono)).getText().toString();
                String email = ((TextView) findViewById(R.id.txtObraEmail)).getText().toString();
                String comentario = ((TextView) findViewById(R.id.txtObraComentario)).getText().toString();
                if (nombre.equals("") || contacto.equals("") || direccion.equals("")) {
                    //Toast.makeText(getApplicationContext(), "Los campos Nombre, Contacto y Dirección son obligatorios.", Toast.LENGTH_LONG).show();
                    // Inflating the layout for the toast
                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.error_toast,
                            (ViewGroup) findViewById(R.id.toast_custom_error));

                    // Typecasting and finding the view in the inflated layout
                    TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                    // Setting the text to be displayed in the Toast
                    text.setText("Los campos Nombre, Contacto\ny Dirección son obligatorios.");

                    // Creating the Toast
                    Toast toast = new Toast(getApplicationContext());

                    // Setting the position of the Toast to centre
                    toast.setGravity(Gravity.CENTER, 0, 0);

                    // Setting the duration of the Toast
                    toast.setDuration(Toast.LENGTH_SHORT);

                    // Setting the Inflated Layout to the Toast
                    toast.setView(layout);

                    // Showing the Toast
                    toast.show();
                } else {
                    Location loc = locObra;
                    if(loc!=null){
                        //GUARDAR
                        cl = (Cliente)getIntent().getExtras().getSerializable("Cliente");
                        vendedor = (Vendedor)getIntent().getExtras().getSerializable("Vendedor");
                        DBHelper h = new DBHelper(getApplicationContext());
                        SQLiteDatabase db = h.getReadableDatabase();
                        SQLiteDatabase dbw = h.getWritableDatabase();


                        co = h.getCorrelativoObra(db,dbw);



                        Log.e("Localizacion Obra nueva",loc.toString());
                        Obra o = new Obra();
                        Long tsLong = System.currentTimeMillis()/1000;
                        String rutV = AuxClass.getV().getPk_RutVendedor() + "";
                        String rut = rutV.substring(0,4);
                        Long correlativo = Long.parseLong(rut + "" + tsLong);
                        o.setPk_Obra(correlativo);
                        o.setFk_RutCliente(cl.getPk_RutCliente());
                        o.setNombreObra(nombre);
                        o.setDireccionObra(direccion);
                        o.setLatitudObra(loc.getLatitude());
                        o.setLongitudObra(loc.getLongitude());
                        o.setRadioObra(200);
                        o.setContacto(contacto);
                        try{
                            o.setTelefono(Integer.parseInt(telefono));
                        }catch (NumberFormatException ex){

                        }
                        o.setEmail(email);
                        o.setComentario(comentario);
                        int res = h.insertarObra(o, dbw);
                        ObraVendedor ov = new ObraVendedor();
                        ov.setFk_Obra(o.getPk_Obra());
                        ov.setFk_RutVendedor(vendedor.getPk_RutVendedor());
                        int res2 = h.insertarObraVendedor(ov,dbw);
                        if(res == 1 && res2 == 1){
                            Intent i2 = new Intent(NuevaObra.this,NuevaObra.class);
                            i2.putExtra("permiso",1);
                            setResult(Activity.RESULT_OK, i2);
                            Intent i = new Intent(this,Entregas.class);
                            i.putExtra("Vendedor",(Serializable)vendedor);
                            i.putExtra("Cliente",(Serializable)cl);
                            startActivity(i);
                            // Inflating the layout for the toast
                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.ok_toast,
                                    (ViewGroup) findViewById(R.id.toast_custom_ok));

                            // Typecasting and finding the view in the inflated layout
                            TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                            // Setting the text to be displayed in the Toast
                            text.setText("Obra agregada.");

                            // Creating the Toast
                            Toast toast = new Toast(getApplicationContext());

                            // Setting the position of the Toast to centre
                            toast.setGravity(Gravity.CENTER, 0, 0);

                            // Setting the duration of the Toast
                            toast.setDuration(Toast.LENGTH_SHORT);

                            // Setting the Inflated Layout to the Toast
                            toast.setView(layout);

                            // Showing the Toast
                            toast.show();
                            finish();
                        }else{
                            Log.e("Error add obra","Error");
                        }
                    }else{
                        //Toast.makeText(getApplicationContext(),"Obteniendo localización, intente nuevamente",Toast.LENGTH_SHORT).show();
                        // Inflating the layout for the toast
                        LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.error_toast,
                                (ViewGroup) findViewById(R.id.toast_custom_error));

                        // Typecasting and finding the view in the inflated layout
                        TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                        // Setting the text to be displayed in the Toast
                        text.setText("Obteniendo localización,\nintente nuevamente.");

                        // Creating the Toast
                        Toast toast = new Toast(getApplicationContext());

                        // Setting the position of the Toast to centre
                        toast.setGravity(Gravity.CENTER, 0, 0);

                        // Setting the duration of the Toast
                        toast.setDuration(Toast.LENGTH_SHORT);

                        // Setting the Inflated Layout to the Toast
                        toast.setView(layout);

                        // Showing the Toast
                        toast.show();
                    }


                }
            }else{
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(NuevaObra.this);
                alertDialogBuilder.setTitle("GPS Desactivado");
                alertDialogBuilder.setMessage("Tiene que activar el GPS para seguir").setNeutralButton("Habilitar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));

                    }
                });
                alertDialogBuilder.show();
                Log.e("ErrorALERT","dasd");
            }

        }

        return super.onOptionsItemSelected(item);
    }

    public class GetCorrelativo extends AsyncTask<Void,Void,Void>{
        @Override
        protected Void doInBackground(Void... params) {
            VendedorDAO vdao = new VendedorDAO(0);

            if(vdao.updateCorrelativoObra() == 1){
                co = vdao.getCorrelativoObra();
                Log.e("Correlativo","Correlativo obtenido");
            }
            return null;
        }

    }

    public static boolean verificaInternet(Context ctx) {
        boolean bConectado = false;
        ConnectivityManager connec = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        // No sólo wifi, también GPRS
        NetworkInfo[] redes = connec.getAllNetworkInfo();
        // este bucle debería no ser tan ñapa
        for (int i = 0; i < 2; i++) {
            // ¿Tenemos conexión? ponemos a true
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                bConectado = true;
            }
        }
        return bConectado;
    }
}
