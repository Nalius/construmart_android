package cl.zonagps.zonaventagps.clases;

/**
 * Created by HP on 04-01-2016.
 */
public final class AuxClass {
    public static Vendedor v;

    private AuxClass() {
    }

    public static Vendedor getV() {
        return v;
    }

    public static void setV(Vendedor v) {
        AuxClass.v = v;
    }
}
