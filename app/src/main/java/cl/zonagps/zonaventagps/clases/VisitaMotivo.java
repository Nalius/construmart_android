package cl.zonagps.zonaventagps.clases;

import java.io.Serializable;

/**
 * Created by HP on 28-07-2015.
 */
public class VisitaMotivo implements Serializable{
    private int fk_Visita;
    private int fk_Motivo;
    private int confirmacion;
    private String observacion;
    private String foto;

    public VisitaMotivo() {

    }

    public VisitaMotivo(int fk_Visita, int fk_Motivo, int confirmacion, String observacion, String foto) {
        this.fk_Visita = fk_Visita;
        this.fk_Motivo = fk_Motivo;
        this.confirmacion = confirmacion;
        this.observacion = observacion;
        this.foto = foto;
    }

    public int getFk_Visita() {
        return fk_Visita;
    }

    public void setFk_Visita(int fk_Visita) {
        this.fk_Visita = fk_Visita;
    }

    public int getFk_Motivo() {
        return fk_Motivo;
    }

    public void setFk_Motivo(int fk_Motivo) {
        this.fk_Motivo = fk_Motivo;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }

    public int getConfirmacion() {
        return confirmacion;
    }

    public void setConfirmacion(int confirmacion) {
        this.confirmacion = confirmacion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }
}
