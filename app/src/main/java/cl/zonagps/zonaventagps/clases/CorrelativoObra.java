package cl.zonagps.zonaventagps.clases;

/**
 * Created by HP on 28-07-2015.
 */
public class CorrelativoObra {
    private long nroCorrelativoObra;

    public CorrelativoObra(long nroCorrelativoObra) {
        this.nroCorrelativoObra = nroCorrelativoObra;
    }

    public CorrelativoObra() {
    }

    public long getNroCorrelativoObra() {
        return nroCorrelativoObra;
    }

    public void setNroCorrelativoObra(long nroCorrelativoObra) {
        this.nroCorrelativoObra = nroCorrelativoObra;
    }
}
