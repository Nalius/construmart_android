package cl.zonagps.zonaventagps.clases;

/**
 * Created by HP on 28-07-2015.
 */
public class Prospecto {

    private int pk_Prospecto;
    private int pk_RutVendedor;
    private String rutCliente;
    private String nombreCliente;
    private String encargado;
    private String direccion;
    private String telefono;
    private String email;
    private String comentario;
    private int fk_Estado;
    private String fechaIngreso;

    public Prospecto() {
    }

    public Prospecto(int pk_Prospecto, int pk_RutVendedor, String rutCliente, String nombreCliente, String encargado, String direccion, String telefono, String email, String comentario, int fk_Estado, String fechaIngreso) {
        this.pk_Prospecto = pk_Prospecto;
        this.pk_RutVendedor = pk_RutVendedor;
        this.rutCliente = rutCliente;
        this.nombreCliente = nombreCliente;
        this.encargado = encargado;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.comentario = comentario;
        this.fk_Estado = fk_Estado;
        this.fechaIngreso = fechaIngreso;
    }

    public int getPk_Prospecto() {
        return pk_Prospecto;
    }

    public void setPk_Prospecto(int pk_Prospecto) {
        this.pk_Prospecto = pk_Prospecto;
    }

    public int getPk_RutVendedor() {
        return pk_RutVendedor;
    }

    public void setPk_RutVendedor(int pk_RutVendedor) {
        this.pk_RutVendedor = pk_RutVendedor;
    }

    public String getRutCliente() {
        return rutCliente;
    }

    public void setRutCliente(String rutCliente) {
        this.rutCliente = rutCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getEncargado() {
        return encargado;
    }

    public void setEncargado(String encargado) {
        this.encargado = encargado;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getFk_Estado() {
        return fk_Estado;
    }

    public void setFk_Estado(int fk_Estado) {
        this.fk_Estado = fk_Estado;
    }

    public String getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(String fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
}
