package cl.zonagps.zonaventagps.clases;

/**
 * Created by HP on 31-07-2015.
 */
public class ObraVendedor {

    private int fk_RutVendedor;
    private long fk_Obra;

    public ObraVendedor() {
    }

    public ObraVendedor(int fk_RutVendedor, long fk_Obra) {
        this.fk_RutVendedor = fk_RutVendedor;
        this.fk_Obra = fk_Obra;
    }

    public int getFk_RutVendedor() {
        return fk_RutVendedor;
    }

    public void setFk_RutVendedor(int fk_RutVendedor) {
        this.fk_RutVendedor = fk_RutVendedor;
    }

    public long getFk_Obra() {
        return fk_Obra;
    }

    public void setFk_Obra(long fk_Obra) {
        this.fk_Obra = fk_Obra;
    }
}
