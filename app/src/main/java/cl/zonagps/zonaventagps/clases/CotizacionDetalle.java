package cl.zonagps.zonaventagps.clases;

/**
 * Created by HP on 28-07-2015.
 */
public class CotizacionDetalle {
    private int pk_CotizacionDetalle;
    private int fk_Cotizacion;
    private int codigoArticulo;
    private String  descripcionArticulo;
    private float cantidad;
    private String unidadMedida;
    private int monto;
    private String moneda;

    public CotizacionDetalle() {
    }

    public CotizacionDetalle(int pk_CotizacionDetalle, int fk_Cotizacion, int codigoArticulo, String descripcionArticulo, float cantidad, String unidadMedida, int monto, String moneda) {
        this.pk_CotizacionDetalle = pk_CotizacionDetalle;
        this.fk_Cotizacion = fk_Cotizacion;
        this.codigoArticulo = codigoArticulo;
        this.descripcionArticulo = descripcionArticulo;
        this.cantidad = cantidad;
        this.unidadMedida = unidadMedida;
        this.monto = monto;
        this.moneda = moneda;
    }

    public int getPk_CotizacionDetalle() {
        return pk_CotizacionDetalle;
    }

    public void setPk_CotizacionDetalle(int pk_CotizacionDetalle) {
        this.pk_CotizacionDetalle = pk_CotizacionDetalle;
    }

    public int getFk_Cotizacion() {
        return fk_Cotizacion;
    }

    public void setFk_Cotizacion(int fk_Cotizacion) {
        this.fk_Cotizacion = fk_Cotizacion;
    }

    public int getCodigoArticulo() {
        return codigoArticulo;
    }

    public void setCodigoArticulo(int codigoArticulo) {
        this.codigoArticulo = codigoArticulo;
    }

    public String getDescripcionArticulo() {
        return descripcionArticulo;
    }

    public void setDescripcionArticulo(String descripcionArticulo) {
        this.descripcionArticulo = descripcionArticulo;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public String getUnidadMedida() {
        return unidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        this.unidadMedida = unidadMedida;
    }

    public int getMonto() {
        return monto;
    }

    public void setMonto(int monto) {
        this.monto = monto;
    }

    public String getMoneda() {
        return moneda;
    }

    public void setMoneda(String moneda) {
        this.moneda = moneda;
    }
}
