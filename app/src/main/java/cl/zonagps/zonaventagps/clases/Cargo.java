package cl.zonagps.zonaventagps.clases;

/**
 * Created by HP on 28-07-2015.
 */
public class Cargo {
    private int pk_Cargo;
    private String nombreCargo;

    public Cargo() {

    }

    public Cargo(int pk_Cargo, String nombreCargo) {
        this.pk_Cargo = pk_Cargo;
        this.nombreCargo = nombreCargo;
    }

    public int getPk_Cargo() {
        return pk_Cargo;
    }

    public void setPk_Cargo(int pk_Cargo) {
        this.pk_Cargo = pk_Cargo;
    }

    public String getNombreCargo() {
        return nombreCargo;
    }

    public void setNombreCargo(String nombreCargo) {
        this.nombreCargo = nombreCargo;
    }
}
