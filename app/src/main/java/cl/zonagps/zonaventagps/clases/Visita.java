package cl.zonagps.zonaventagps.clases;

import java.io.Serializable;

/**
 * Created by HP on 28-07-2015.
 */
public class Visita implements Serializable{
    private int pk_Visita;
    private int fk_RutCliente;
    private long fk_Obra;
    private int fk_RutVendedor;
    private String fechaVisita;
    private double latitud;
    private double longitud;
    private String foto1;
    private String foto2;
    private String observacion;
    private int subida;
    private int idVisita;

    public Visita() {
    }

    public Visita(int pk_Visita, int fk_RutCliente, long fk_Obra, int fk_RutVendedor, String fechaVisita, double latitud, double longitud, String foto1, String foto2, String observacion, int subida) {
        this.pk_Visita = pk_Visita;
        this.fk_RutCliente = fk_RutCliente;
        this.fk_Obra = fk_Obra;
        this.fk_RutVendedor = fk_RutVendedor;
        this.fechaVisita = fechaVisita;
        this.latitud = latitud;
        this.longitud = longitud;
        this.foto1 = foto1;
        this.foto2 = foto2;
        this.observacion = observacion;
        this.subida = subida;
    }

    public int getIdVisita() {
        return idVisita;
    }

    public void setIdVisita(int idVisita) {
        this.idVisita = idVisita;
    }

    public int getSubida() {
        return subida;
    }

    public void setSubida(int subida) {
        this.subida = subida;
    }

    public int getPk_Visita() {
        return pk_Visita;
    }

    public void setPk_Visita(int pk_Visita) {
        this.pk_Visita = pk_Visita;
    }

    public long getFk_Obra() {
        return fk_Obra;
    }

    public void setFk_Obra(long fk_Obra) {
        this.fk_Obra = fk_Obra;
    }

    public int getFk_RutCliente() {
        return fk_RutCliente;
    }

    public void setFk_RutCliente(int fk_RutCliente) {
        this.fk_RutCliente = fk_RutCliente;
    }

    public int getFk_RutVendedor() {
        return fk_RutVendedor;
    }

    public void setFk_RutVendedor(int fk_RutVendedor) {
        this.fk_RutVendedor = fk_RutVendedor;
    }

    public String getFechaVisita() {
        return fechaVisita;
    }

    public void setFechaVisita(String fechaVisita) {
        this.fechaVisita = fechaVisita;
    }

    public double getLatitud() {
        return latitud;
    }

    public void setLatitud(double latitud) {
        this.latitud = latitud;
    }

    public double getLongitud() {
        return longitud;
    }

    public void setLongitud(double longitud) {
        this.longitud = longitud;
    }

    public String getFoto1() {
        return foto1;
    }

    public void setFoto1(String foto1) {
        this.foto1 = foto1;
    }

    public String getFoto2() {
        return foto2;
    }

    public void setFoto2(String foto2) {
        this.foto2 = foto2;
    }

    public String getObservacion() {
        return observacion;
    }

    public void setObservacion(String observacion) {
        this.observacion = observacion;
    }
}
