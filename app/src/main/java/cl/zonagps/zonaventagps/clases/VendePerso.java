package cl.zonagps.zonaventagps.clases;

/**
 * Created by HP on 28-07-2015.
 */
public class VendePerso {
    private int fk_RutVendedor;
    private int fk_Personal;

    public VendePerso() {
    }

    public VendePerso(int fk_RutVendedor, int fk_Personal) {
        this.fk_RutVendedor = fk_RutVendedor;
        this.fk_Personal = fk_Personal;
    }

    public int getFk_RutVendedor() {
        return fk_RutVendedor;
    }

    public void setFk_RutVendedor(int fk_RutVendedor) {
        this.fk_RutVendedor = fk_RutVendedor;
    }

    public int getFk_Personal() {
        return fk_Personal;
    }

    public void setFk_Personal(int fk_Personal) {
        this.fk_Personal = fk_Personal;
    }
}
