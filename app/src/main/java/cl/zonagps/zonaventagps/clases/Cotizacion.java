package cl.zonagps.zonaventagps.clases;

import java.util.Date;

/**
 * Created by HP on 28-07-2015.
 */
public class Cotizacion {
    private int pk_Cotizacion;
    private int fk_RutCliente;
    private Date fechaCotizacion;
    private int fk_Estado;

    public Cotizacion() {
    }

    public Cotizacion(int pk_Cotizacion, int fk_RutCliente, Date fechaCotizacion, int fk_Estado) {
        this.pk_Cotizacion = pk_Cotizacion;
        this.fk_RutCliente = fk_RutCliente;
        this.fechaCotizacion = fechaCotizacion;
        this.fk_Estado = fk_Estado;
    }

    public int getPk_Cotizacion() {
        return pk_Cotizacion;
    }

    public void setPk_Cotizacion(int pk_Cotizacion) {
        this.pk_Cotizacion = pk_Cotizacion;
    }

    public int getFk_RutCliente() {
        return fk_RutCliente;
    }

    public void setFk_RutCliente(int fk_RutCliente) {
        this.fk_RutCliente = fk_RutCliente;
    }

    public Date getFechaCotizacion() {
        return fechaCotizacion;
    }

    public void setFechaCotizacion(Date fechaCotizacion) {
        this.fechaCotizacion = fechaCotizacion;
    }

    public int getFk_Estado() {
        return fk_Estado;
    }

    public void setFk_Estado(int fk_Estado) {
        this.fk_Estado = fk_Estado;
    }
}
