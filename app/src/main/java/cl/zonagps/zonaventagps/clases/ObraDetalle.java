package cl.zonagps.zonaventagps.clases;

import java.util.Date;

/**
 * Created by HP on 28-07-2015.
 */
public class ObraDetalle {
    private long fk_Obra;
    private Date fechaIngreso;
    private int fk_Estado;



    public ObraDetalle() {
    }

    public ObraDetalle(long fk_Obra, Date fechaIngreso, int fk_Estado) {
        this.fk_Obra = fk_Obra;
        this.fechaIngreso = fechaIngreso;
        this.fk_Estado = fk_Estado;
    }

    public int getFk_Estado() {
        return fk_Estado;
    }

    public void setFk_Estado(int fk_Estado) {
        this.fk_Estado = fk_Estado;
    }

    public long getFk_Obra() {
        return fk_Obra;
    }

    public void setFk_Obra(long fk_Obra) {
        this.fk_Obra = fk_Obra;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }
}
