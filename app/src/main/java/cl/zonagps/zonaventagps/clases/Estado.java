package cl.zonagps.zonaventagps.clases;

/**
 * Created by HP on 28-07-2015.
 */
public class Estado {
    private int pk_Estado;
    private String nombreEstado;

    public Estado() {
    }

    public Estado(int pk_Estado, String nombreEstado) {
        this.pk_Estado = pk_Estado;
        this.nombreEstado = nombreEstado;
    }

    public int getPk_Estado() {
        return pk_Estado;
    }

    public void setPk_Estado(int pk_Estado) {
        this.pk_Estado = pk_Estado;
    }

    public String getNombreEstado() {
        return nombreEstado;
    }

    public void setNombreEstado(String nombreEstado) {
        this.nombreEstado = nombreEstado;
    }
}
