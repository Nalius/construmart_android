package cl.zonagps.zonaventagps.clases;

import java.util.Date;

/**
 * Created by HP on 28-07-2015.
 */
public class Credito {
    private int pk_Credito;
    private int fk_RutCliente;
    private int MLD;
    private Date fechaVLC;
    private int MDM;
    private Date fechaUV;
    private int uV;
    private int fk_Cotizacion;
    private Date fechaCotizacion;

    public Credito() {
    }

    public Credito(int pk_Credito, int MLD, int fk_RutCliente, Date fechaVLC, int MDM, Date fechaUV, int uV, int fk_Cotizacion, Date fechaCotizacion) {
        this.pk_Credito = pk_Credito;
        this.MLD = MLD;
        this.fk_RutCliente = fk_RutCliente;
        this.fechaVLC = fechaVLC;
        this.MDM = MDM;
        this.fechaUV = fechaUV;
        this.uV = uV;
        this.fk_Cotizacion = fk_Cotizacion;
        this.fechaCotizacion = fechaCotizacion;
    }

    public int getPk_Credito() {
        return pk_Credito;
    }

    public void setPk_Credito(int pk_Credito) {
        this.pk_Credito = pk_Credito;
    }

    public int getFk_RutCliente() {
        return fk_RutCliente;
    }

    public void setFk_RutCliente(int fk_RutCliente) {
        this.fk_RutCliente = fk_RutCliente;
    }

    public int getMLD() {
        return MLD;
    }

    public void setMLD(int MLD) {
        this.MLD = MLD;
    }

    public Date getFechaVLC() {
        return fechaVLC;
    }

    public void setFechaVLC(Date fechaVLC) {
        this.fechaVLC = fechaVLC;
    }

    public int getMDM() {
        return MDM;
    }

    public void setMDM(int MDM) {
        this.MDM = MDM;
    }

    public Date getFechaUV() {
        return fechaUV;
    }

    public void setFechaUV(Date fechaUV) {
        this.fechaUV = fechaUV;
    }

    public int getuV() {
        return uV;
    }

    public void setuV(int uV) {
        this.uV = uV;
    }

    public int getFk_Cotizacion() {
        return fk_Cotizacion;
    }

    public void setFk_Cotizacion(int fk_Cotizacion) {
        this.fk_Cotizacion = fk_Cotizacion;
    }

    public Date getFechaCotizacion() {
        return fechaCotizacion;
    }

    public void setFechaCotizacion(Date fechaCotizacion) {
        this.fechaCotizacion = fechaCotizacion;
    }
}
