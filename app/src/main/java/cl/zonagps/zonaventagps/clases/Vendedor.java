package cl.zonagps.zonaventagps.clases;

import java.io.Serializable;

/**
 * Created by HP on 28-07-2015.
 */
public class Vendedor implements Serializable {
    private int pk_RutVendedor;
    private String nombreVendedor;
    private String nombreUsuario;
    private String passUsuario;
    private String imei;
    private int fk_Estado;

    public Vendedor() {
    }

    public Vendedor(int pk_RutVendedor, String nombreVendedor, String nombreUsuario, String passUsuario, String imei, int fk_Estado) {
        this.pk_RutVendedor = pk_RutVendedor;
        this.nombreVendedor = nombreVendedor;
        this.nombreUsuario = nombreUsuario;
        this.passUsuario = passUsuario;
        this.imei = imei;
        this.fk_Estado = fk_Estado;
    }

    public int getPk_RutVendedor() {
        return pk_RutVendedor;
    }

    public void setPk_RutVendedor(int pk_RutVendedor) {
        this.pk_RutVendedor = pk_RutVendedor;
    }

    public String getNombreVendedor() {
        return nombreVendedor;
    }

    public void setNombreVendedor(String nombreVendedor) {
        this.nombreVendedor = nombreVendedor;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPassUsuario() {
        return passUsuario;
    }

    public void setPassUsuario(String passUsuario) {
        this.passUsuario = passUsuario;
    }

    public String getImei() {
        return imei;
    }

    public void setImei(String imei) {
        this.imei = imei;
    }

    public int getFk_Estado() {
        return fk_Estado;
    }

    public void setFk_Estado(int fk_Estado) {
        this.fk_Estado = fk_Estado;
    }
}
