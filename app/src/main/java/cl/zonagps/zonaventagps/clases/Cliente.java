package cl.zonagps.zonaventagps.clases;

import java.io.Serializable;

/**
 * Created by HP on 28-07-2015.
 */
public class Cliente implements Serializable {
    private int pk_RutCliente;
    private String nombreCliente;
    private String encargado;
    private String direccion;
    private String telefono;
    private String email;
    private String comentario;
    private int fk_Estado;

    public Cliente() {
    }

    public Cliente(int pk_RutCliente, String nombreCliente, String encargado, String direccion, String telefono, String email, String comentario, int fk_Estado) {
        this.pk_RutCliente = pk_RutCliente;
        this.nombreCliente = nombreCliente;
        this.encargado = encargado;
        this.direccion = direccion;
        this.telefono = telefono;
        this.email = email;
        this.comentario = comentario;
        this.fk_Estado = fk_Estado;
    }

    public int getPk_RutCliente() {
        return pk_RutCliente;
    }

    public void setPk_RutCliente(int pk_RutCliente) {
        this.pk_RutCliente = pk_RutCliente;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getEncargado() {
        return encargado;
    }

    public void setEncargado(String encargado) {
        this.encargado = encargado;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

    public int getFk_Estado() {
        return fk_Estado;
    }

    public void setFk_Estado(int fk_Estado) {
        this.fk_Estado = fk_Estado;
    }
}
