package cl.zonagps.zonaventagps.clases;

/**
 * Created by HP on 28-07-2015.
 */
public class Personal {
    private int pk_Personal;
    private String nombrePersonal;
    private int fk_Cargo;
    private int fk_Personal;

    public Personal() {
    }

    public Personal(int pk_Personal, int fk_Cargo, String nombrePersonal, int fk_Personal) {
        this.pk_Personal = pk_Personal;
        this.fk_Cargo = fk_Cargo;
        this.nombrePersonal = nombrePersonal;
        this.fk_Personal = fk_Personal;
    }

    public int getPk_Personal() {
        return pk_Personal;
    }

    public void setPk_Personal(int pk_Personal) {
        this.pk_Personal = pk_Personal;
    }

    public String getNombrePersonal() {
        return nombrePersonal;
    }

    public void setNombrePersonal(String nombrePersonal) {
        this.nombrePersonal = nombrePersonal;
    }

    public int getFk_Cargo() {
        return fk_Cargo;
    }

    public void setFk_Cargo(int fk_Cargo) {
        this.fk_Cargo = fk_Cargo;
    }

    public int getFk_Personal() {
        return fk_Personal;
    }

    public void setFk_Personal(int fk_Personal) {
        this.fk_Personal = fk_Personal;
    }
}
