package cl.zonagps.zonaventagps.clases;

/**
 * Created by HP on 28-07-2015.
 */
public class UsuarioPersonal {

    private int fk_Usuario;
    private int fk_Personal;
    private int fk_RutVendedor;
    private int fk_Cargo;

    public UsuarioPersonal() {
    }

    public UsuarioPersonal(int fk_Usuario, int fk_Personal, int fk_RutVendedor, int fk_Cargo) {
        this.fk_Usuario = fk_Usuario;
        this.fk_Personal = fk_Personal;
        this.fk_RutVendedor = fk_RutVendedor;
        this.fk_Cargo = fk_Cargo;
    }

    public int getFk_Usuario() {
        return fk_Usuario;
    }

    public void setFk_Usuario(int fk_Usuario) {
        this.fk_Usuario = fk_Usuario;
    }

    public int getFk_Personal() {
        return fk_Personal;
    }

    public void setFk_Personal(int fk_Personal) {
        this.fk_Personal = fk_Personal;
    }

    public int getFk_RutVendedor() {
        return fk_RutVendedor;
    }

    public void setFk_RutVendedor(int fk_RutVendedor) {
        this.fk_RutVendedor = fk_RutVendedor;
    }

    public int getFk_Cargo() {
        return fk_Cargo;
    }

    public void setFk_Cargo(int fk_Cargo) {
        this.fk_Cargo = fk_Cargo;
    }
}
