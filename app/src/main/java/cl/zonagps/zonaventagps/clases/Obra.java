package cl.zonagps.zonaventagps.clases;

/**
 * Created by HP on 28-07-2015.
 */
public class Obra {
    private long pk_Obra;
    private int fk_RutCliente;
    private String nombreObra;
    private String direccionObra;
    private double latitudObra;
    private double longitudObra;
    private int radioObra;
    private String contacto;
    private int telefono;
    private String email;
    private String comentario;

    public Obra() {
    }

    public Obra(long pk_Obra, int fk_RutCliente, String nombreObra, String direccionObra, double latitudObra, double longitudObra, int radioObra, String contacto, int telefono, String email, String comentario) {
        this.pk_Obra = pk_Obra;
        this.fk_RutCliente = fk_RutCliente;
        this.nombreObra = nombreObra;
        this.direccionObra = direccionObra;
        this.latitudObra = latitudObra;
        this.longitudObra = longitudObra;
        this.radioObra = radioObra;
        this.contacto = contacto;
        this.telefono = telefono;
        this.email = email;
        this.comentario = comentario;
    }

    public long getPk_Obra() {
        return pk_Obra;
    }

    public void setPk_Obra(long pk_Obra) {
        this.pk_Obra = pk_Obra;
    }

    public int getFk_RutCliente() {
        return fk_RutCliente;
    }

    public void setFk_RutCliente(int fk_RutCliente) {
        this.fk_RutCliente = fk_RutCliente;
    }

    public String getNombreObra() {
        return nombreObra;
    }

    public void setNombreObra(String nombreObra) {
        this.nombreObra = nombreObra;
    }

    public String getDireccionObra() {
        return direccionObra;
    }

    public void setDireccionObra(String direccionObra) {
        this.direccionObra = direccionObra;
    }

    public double getLatitudObra() {
        return latitudObra;
    }

    public void setLatitudObra(double latitudObra) {
        this.latitudObra = latitudObra;
    }

    public double getLongitudObra() {
        return longitudObra;
    }

    public void setLongitudObra(double longitudObra) {
        this.longitudObra = longitudObra;
    }

    public int getRadioObra() {
        return radioObra;
    }

    public void setRadioObra(int radioObra) {
        this.radioObra = radioObra;
    }

    public String getContacto() {
        return contacto;
    }

    public void setContacto(String contacto) {
        this.contacto = contacto;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }
}
