package cl.zonagps.zonaventagps.clases;

import java.io.Serializable;

/**
 * Created by HP on 28-07-2015.
 */
public class Motivo implements Serializable{
    private int pk_Motivo;
    private String descripcionMotivo;
    private int fk_Estado;

    public Motivo() {
    }

    public Motivo(int pk_Motivo, String descripcionMotivo, int fk_Estado) {
        this.pk_Motivo = pk_Motivo;
        this.descripcionMotivo = descripcionMotivo;
        this.fk_Estado = fk_Estado;
    }

    public int getPk_Motivo() {
        return pk_Motivo;
    }

    public void setPk_Motivo(int pk_Motivo) {
        this.pk_Motivo = pk_Motivo;
    }

    public String getDescripcionMotivo() {
        return descripcionMotivo;
    }

    public void setDescripcionMotivo(String descripcionMotivo) {
        this.descripcionMotivo = descripcionMotivo;
    }

    public int getFk_Estado() {
        return fk_Estado;
    }

    public void setFk_Estado(int fk_Estado) {
        this.fk_Estado = fk_Estado;
    }
}
