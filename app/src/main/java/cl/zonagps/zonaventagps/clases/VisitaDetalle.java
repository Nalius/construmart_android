package cl.zonagps.zonaventagps.clases;

import java.util.Date;

/**
 * Created by HP on 28-07-2015.
 */
public class VisitaDetalle {
    private int fk_Visita;
    private Date fechaIngreso;
    private int fk_Estado;

    public VisitaDetalle() {
    }

    public VisitaDetalle(int fk_Visita, int fk_Estado, Date fechaIngreso) {
        this.fk_Visita = fk_Visita;
        this.fk_Estado = fk_Estado;
        this.fechaIngreso = fechaIngreso;
    }

    public int getFk_Visita() {
        return fk_Visita;
    }

    public void setFk_Visita(int fk_Visita) {
        this.fk_Visita = fk_Visita;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public int getFk_Estado() {
        return fk_Estado;
    }

    public void setFk_Estado(int fk_Estado) {
        this.fk_Estado = fk_Estado;
    }
}
