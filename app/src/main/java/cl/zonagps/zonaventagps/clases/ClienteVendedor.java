package cl.zonagps.zonaventagps.clases;

/**
 * Created by HP on 28-07-2015.
 */
public class ClienteVendedor {
    private int fk_RutCliente;
    private int fk_RutVendedor;

    public ClienteVendedor() {
    }

    public ClienteVendedor(int fk_RutCliente, int fk_RutVendedor) {
        this.fk_RutCliente = fk_RutCliente;
        this.fk_RutVendedor = fk_RutVendedor;
    }

    public int getFk_RutCliente() {
        return fk_RutCliente;
    }

    public void setFk_RutCliente(int fk_RutCliente) {
        this.fk_RutCliente = fk_RutCliente;
    }

    public int getFk_RutVendedor() {
        return fk_RutVendedor;
    }

    public void setFk_RutVendedor(int fk_RutVendedor) {
        this.fk_RutVendedor = fk_RutVendedor;
    }
}
