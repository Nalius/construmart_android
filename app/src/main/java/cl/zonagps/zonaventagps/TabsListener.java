package cl.zonagps.zonaventagps;

import android.app.Activity;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;


/**
 * Created by LEONEL CARRASCO
 */
public class TabsListener <T extends Fragment> implements ActionBar.TabListener {

    private Fragment fragment;
    private final String tag;

    public TabsListener(Activity activity, String tag, Class<T> cls) {
        this.tag = tag;
        fragment = Fragment.instantiate(activity, cls.getName());
    }

    public void onTabSelected(Tab tab, FragmentTransaction ft) {
        ft.replace(android.R.id.content, fragment, tag);
    }

    public void onTabUnselected(Tab tab, FragmentTransaction ft) {
        ft.remove(fragment);
    }

    public void onTabReselected(Tab tab, FragmentTransaction ft) {}

}
