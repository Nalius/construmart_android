package cl.zonagps.zonaventagps;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.io.Serializable;
import java.util.ArrayList;

import cl.zonagps.zonaventagps.clases.Visita;


public class VisitasDia extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visitas_dia);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#006e98")));

        DBHelper h = new DBHelper(getApplicationContext());
        SQLiteDatabase db = h.getReadableDatabase();

        final ArrayList<Visita> visitas= h.verMisVisitasDelDia(db);


        if(visitas==null || visitas.size() == 0){

            ((TextView)findViewById(R.id.lblvisitadiaResultado)).setVisibility(View.VISIBLE);

        }else{
            ((TextView)findViewById(R.id.lblvisitadiaResultado)).setVisibility(View.INVISIBLE);
            ListView lvVisitas = (ListView)findViewById(R.id.lvVisitasdelDia);
            ListVisitasDia adapter = new ListVisitasDia(this,visitas);
            lvVisitas.setAdapter(adapter);


            lvVisitas.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent i = new Intent(getApplicationContext(), cl.zonagps.zonaventagps.Visita.class);
                    Visita visita = visitas.get(position);
                    i.putExtra("Visita",(Serializable)visita);
                    startActivity(i);
                }
            });
        }





    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_visitas_dia, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            Intent i = new Intent();
            i.setData(Uri.parse("http://www.zonagps.cl/construmart/manual.pdf"));
            startActivity(i);
            return true;
        }
        if(id == R.id.action_salir){
            if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN){
                finishAffinity();
            }
        }
        if(id == R.id.action_about){
            Intent i = new Intent(getApplicationContext(),Acerca.class);
            startActivity(i);
        }
        if(id == R.id.btnVolver){
            finish();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
