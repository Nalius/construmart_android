package cl.zonagps.zonaventagps.DAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by LEONEL CARRASCO
 */
public class Conectar {
    public static Connection GetConnection()
    {
        Connection conexion=null;

        try
        {
            Class.forName("net.sourceforge.jtds.jdbc.Driver");
            String url = "jdbc:jtds:sqlserver://200.63.97.14:1433/CONSTRUMART";
            conexion= DriverManager.getConnection(url,"AppSrv","ctt01**");
        }
        catch(ClassNotFoundException ex)
        {
            conexion=null;
        }
        catch(SQLException ex)
        {
            conexion=null;
        }
        catch(Exception ex)
        {
            conexion=null;
        }
        finally
        {
            return conexion;
        }
    }
}
