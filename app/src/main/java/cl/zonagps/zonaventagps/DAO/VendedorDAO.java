package cl.zonagps.zonaventagps.DAO;

import android.app.Application;
import android.content.res.Resources;
import android.util.Log;
import android.widget.Toast;


import java.io.IOException;
import java.net.URLDecoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import com.android.volley.*;
import com.android.volley.toolbox.*;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.*;

import cl.zonagps.zonaventagps.R;
import cl.zonagps.zonaventagps.clases.Cliente;
import cl.zonagps.zonaventagps.clases.CorrelativoObra;
import cl.zonagps.zonaventagps.clases.Motivo;
import cl.zonagps.zonaventagps.clases.Obra;
import cl.zonagps.zonaventagps.clases.Vendedor;
import cl.zonagps.zonaventagps.clases.Visita;

/**
 * Created by LEONEL CARRASCO
 * Edited by BRAYYAN YANES
 */
public class VendedorDAO {
    Connection conn;
    private int rut;

    public VendedorDAO(int rut) {
        this.rut= rut;
    }

    public Vendedor verVendedor(String user){
        Vendedor v = null;
        String url = "http://boxapp.cl/construmart_services/verVendedor.php";
        String params = url + "?usuario=" + user;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpPost = new HttpGet(params);
        HttpResponse response= null;
        try {
            String json = "";
            response = httpClient.execute(httpPost);
            json = EntityUtils.toString(response.getEntity());
            JSONArray jArray = new JSONArray(json);
            if(jArray.length() > 0){
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jPit = jArray.getJSONObject(i);
                    v = new Vendedor();
                    v.setPk_RutVendedor(jPit.getInt("IdRutVendedor"));
                    v.setNombreVendedor(jPit.getString("NombreVendedor"));
                    v.setNombreUsuario(jPit.getString("NombreUsuario"));
                    v.setPassUsuario(jPit.getString("Password"));
                    v.setImei(jPit.getString("Imei"));
                    Log.e("A", String.valueOf(jPit.getInt("IdRutVendedor")));
                    Log.e("B", String.valueOf(jPit.getString("NombreVendedor")));
                    Log.e("C", String.valueOf(jPit.getString("NombreUsuario")));
                    Log.e("D", String.valueOf(jPit.getString("Password")));
                    Log.e("E", String.valueOf(jPit.getString("Imei")));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return v;
    }

    public ArrayList<Cliente> getClientesVendedor(){
        ArrayList<Cliente> clientes = new ArrayList<Cliente>();

        String url = "http://boxapp.cl/construmart_services/getClientesVendedor.php";
        String params = url + "?user=" + rut;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpPost = new HttpGet(params);
        HttpResponse response= null;
        try {
            String json = "";
            response = httpClient.execute(httpPost);
            json = EntityUtils.toString(response.getEntity());
            JSONArray jArray = new JSONArray(json);
            if(jArray.length() > 0){
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jPit = jArray.getJSONObject(i);
                    Cliente c = new Cliente();
                    c.setPk_RutCliente(jPit.getInt("IdRutCliente"));
                    c.setNombreCliente(jPit.getString("NombreCliente"));
                    clientes.add(c);
                    Log.e("A", String.valueOf(jPit.getInt("IdRutCliente")));
                    Log.e("B", String.valueOf(jPit.getString("NombreCliente")));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return clientes;
    }

    public ArrayList<Obra> getObrasporCliente(int rutCliente){

        ArrayList<Obra> obras = new ArrayList<>();

        String url = "http://boxapp.cl/construmart_services/getObraCliente.php";
        String params = url + "?cliente=" + rutCliente +"&vendedor=" + rut;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpPost = new HttpGet(params);
        HttpResponse response= null;
        try {
            String json = "";
            response = httpClient.execute(httpPost);
            json = EntityUtils.toString(response.getEntity());
            JSONArray jArray = new JSONArray(json);
            if(jArray.length() > 0){
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jPit = jArray.getJSONObject(i);
                    Obra o = new Obra();
                    o.setPk_Obra(jPit.getLong("IdObra"));
                    o.setNombreObra(jPit.getString("NombreObra"));
                    o.setLatitudObra(jPit.getDouble("Latitud"));
                    o.setLongitudObra(jPit.getDouble("Longitud"));
                    o.setRadioObra(jPit.getInt("Radio"));
                    o.setFk_RutCliente(jPit.getInt("IdCliente"));
                    Log.e("A",String.valueOf(jPit.getLong("IdObra")));
                    Log.e("A",String.valueOf(jPit.getString("NombreObra")));
                    Log.e("A",String.valueOf(jPit.getDouble("Latitud")));
                    Log.e("A",String.valueOf(jPit.getDouble("Longitud")));
                    Log.e("A",String.valueOf(jPit.getInt("Radio")));
                    Log.e("A",String.valueOf(jPit.getInt("IdCliente")));
                    obras.add(o);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }


        return obras;

    }

    public ArrayList<Motivo> getMotivos(){

        ArrayList<Motivo> motivos = new ArrayList<>();

        String url = "http://boxapp.cl/construmart_services/getMotivos.php";
        String params = url;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpPost = new HttpGet(params);
        HttpResponse response= null;
        try {
            String json = "";
            response = httpClient.execute(httpPost);
            json = EntityUtils.toString(response.getEntity());
            JSONArray jArray = new JSONArray(json);
            if(jArray.length() > 0){
                for (int i = 0; i < jArray.length(); i++) {
                    Motivo m = new Motivo();
                    JSONObject jPit = jArray.getJSONObject(i);
                    m.setPk_Motivo(jPit.getInt("IdMotivo"));
                    m.setDescripcionMotivo(jPit.getString("NombreMotivo"));
                    m.setFk_Estado(1);
                    motivos.add(m);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return motivos;
    }

    public int updateCorrelativoObra(){
        int res = 0;
        String sql = "UPDATE CorrelativoObra SET NroCorrelativoObra = NroCorrelativoObra + 1";
        conn = Conectar.GetConnection();
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            res = 1;
        }catch (SQLException ex){
            res = -1;
            Log.e("SQLException",ex.getMessage());
        }
        return res;
    }

    public CorrelativoObra getCorrelativoObra(){
        CorrelativoObra co = null;
        String url = "http://boxapp.cl/construmart_services/getCorrelativoObra.php";
        String params = url;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpPost = new HttpGet(params);
        HttpResponse response= null;
        try {
            String json = "";
            response = httpClient.execute(httpPost);
            json = EntityUtils.toString(response.getEntity());
            JSONArray jArray = new JSONArray(json);
            if(jArray.length() > 0){
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jPit = jArray.getJSONObject(i);
                    co = new CorrelativoObra();
                    co.setNroCorrelativoObra(jPit.getLong("Correlativo"));
                    Log.e("CORRELATIVO",String.valueOf(jPit.getLong("Correlativo")));
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return co;
    }

    public String getFechaServidor(){
        String fecha = "";

        String url = "http://boxapp.cl/construmart_services/getFechaServidor.php";
        String params = url;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpPost = new HttpGet(params);
        HttpResponse response= null;
        try {
            String json = "";
            response = httpClient.execute(httpPost);
            json = EntityUtils.toString(response.getEntity());
            JSONArray jArray = new JSONArray(json);
            if(jArray.length() > 0){
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jPit = jArray.getJSONObject(i);
                    fecha = jPit.getString("Fecha");
                    Log.e("FECHA",fecha);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return fecha;
    }

    public ArrayList<Visita> getVisitasdelVendedor(){
        ArrayList<Visita> visitas = new ArrayList<>();


        String url = "http://boxapp.cl/construmart_services/getVisitaVendedor.php";
        String  params = url + "?vendedor=" + rut;
        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpPost = new HttpGet(params);
        HttpResponse response= null;
        try {
            String json = "";
            response = httpClient.execute(httpPost);
            json = EntityUtils.toString(response.getEntity());
            JSONArray jArray = new JSONArray(json);
            if(jArray.length() > 0){
                for (int i = 0; i < jArray.length(); i++) {
                    JSONObject jPit = jArray.getJSONObject(i);
                    Visita v = new Visita();
                    v.setPk_Visita(jPit.getInt("IdVisita"));
                    v.setFk_RutCliente(jPit.getInt("IdCliente"));
                    v.setFk_RutVendedor(jPit.getInt("IdVendedor"));
                    v.setFk_Obra(jPit.getLong("IdObra"));
                    v.setFechaVisita(jPit.getString("FechaVisita"));
                    v.setObservacion(jPit.getString("Observacion"));
                    visitas.add(v);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return visitas;
    }

}
