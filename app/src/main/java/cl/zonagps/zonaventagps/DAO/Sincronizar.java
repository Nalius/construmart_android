package cl.zonagps.zonaventagps.DAO;

import android.util.Log;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import cl.zonagps.zonaventagps.clases.CorrelativoObra;
import cl.zonagps.zonaventagps.clases.Obra;
import cl.zonagps.zonaventagps.clases.ObraVendedor;
import cl.zonagps.zonaventagps.clases.Prospecto;
import cl.zonagps.zonaventagps.clases.Visita;
import cl.zonagps.zonaventagps.clases.VisitaMotivo;

/**
 * Created by LEONEL CARRASCO
 */
public class Sincronizar {
    Connection conn;

    public Sincronizar() {
    }


    public int agregarProspectoSQLServer(Prospecto p){
        int res = 0;
        conn = Conectar.GetConnection();
        String sql = "INSERT INTO Prospecto(PK_RutVendedor, RutCliente, NombreCliente, Encargado, Direccion, Telefono, Email, Comentario, FK_Estado, FechaIngreso) VALUES"+
                "("+p.getPk_RutVendedor()+","+p.getRutCliente()+",'"+p.getNombreCliente()+"','"+p.getEncargado()+"','"+p.getDireccion()+"','"+p.getTelefono()+
                "','"+p.getEmail()+"','"+p.getComentario()+"',0,'"+p.getFechaIngreso()+"')";
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            res = 1;
            ps.close();
            conn.close();
        }catch (SQLException e){
            Log.e("Sincronizacion","Error al agregar Prospecto");
        }
        return res;
    }

    public int updateCorrelativoObra(){
        int res = 0;
        String sql = "UPDATE CorrelativoObra SET NroCorrelativoObra = NroCorrelativoObra + 2";
        conn = Conectar.GetConnection();
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            res = 1;
        }catch (SQLException ex){
            res = -1;
            Log.e("SQLException",ex.getMessage());
        }
        return res;
    }

    public CorrelativoObra getCorrelativoObra(){
        CorrelativoObra co = null;
        String sql = "SELECT * FROM CorrelativoObra";
        conn = Conectar.GetConnection();
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                co = new CorrelativoObra();
                co.setNroCorrelativoObra(rs.getLong(1));
            }
            rs.close();
            ps.close();
            conn.close();
        } catch (SQLException e) {
            Log.e("SQLException", e.getMessage());
            e.printStackTrace();
        }
        return co;
    }

    public int agregarObraSQLServer(Obra o, long correlativo){
        int res = 0;
        String sql = "INSERT INTO Obra VALUES("+o.getPk_Obra()+","+o.getFk_RutCliente()+",'"+o.getNombreObra()+"','"+o.getDireccionObra()+
                "',"+o.getLatitudObra()+","+o.getLongitudObra()+","+o.getRadioObra()+",'"+o.getContacto()+"',"+o.getTelefono()+",'"+o.getEmail()+
                "','"+o.getComentario()+"')";
        conn = Conectar.GetConnection();
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            res = 1;
            ps.close();
            conn.close();
        }catch (SQLException e){
            Log.e("SQLException",e.getMessage());
            res = -1;
        }
        return res;
    }

    public int agregarObraVendedorSQLServer(ObraVendedor ov, long obra){
        int res = 0;
        String sql = "INSERT INTO ObraVendedor VALUES("+ov.getFk_RutVendedor()+","+obra+")";
        conn = Conectar.GetConnection();
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            res = 1;
            ps.close();
            conn.close();
        }catch (SQLException e){
            res = -1;
            Log.e("SQLException",e.getMessage());
        }
        return 0;
    }

    public int agregarVisitaSQLServer(Visita v){
        int res = 0;
        String sql = "INSERT INTO Visita(FK_RutCliente, FK_Obra, FK_RutVendedor, FechaVisita, Latitud, Longitud, Observacion) VALUES("
                +v.getFk_RutCliente()+","+v.getFk_Obra()+","+v.getFk_RutVendedor()+",'"+v.getFechaVisita()+"',"+v.getLatitud()+","+v.getLongitud()+",'"+v.getObservacion()+"')";
        conn = Conectar.GetConnection();
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            res = 1;
            ps.close();
            conn.close();
        }catch (SQLException e){
            Log.e("SQLException", e.getMessage());
            res = -1;
        }
        return res;
    }

    public int getIDVisitaAgregadaSQLServer(){
        int v = 0;
        String sql = "SELECT TOP 1 PK_Visita FROM Visita ORDER BY PK_Visita DESC";
        conn = Conectar.GetConnection();
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            if(rs.next()){
                v = rs.getInt(1);
            }
            rs.close();
            ps.close();
            conn.close();
        }catch (SQLException e){
            Log.e("SQLException", e.getMessage());
            v = -1;
        }
        return v;
    }

    public int agregarVisitaMotivoSQLServer(VisitaMotivo vm){
        int res = 0;

        String sql = "INSERT INTO VisitaMotivo VALUES("+vm.getFk_Visita()+","+vm.getFk_Motivo()+","+vm.getConfirmacion()+",'"+vm.getObservacion()+"','')";
        conn = Conectar.GetConnection();
        try{
            PreparedStatement ps = conn.prepareStatement(sql);
            ps.executeUpdate();
            res = 1;
            ps.close();
            conn.close();
        }catch (SQLException e){
            Log.e("SQLException",e.getMessage());
            res = -1;
        }
        return res;
    }
}
