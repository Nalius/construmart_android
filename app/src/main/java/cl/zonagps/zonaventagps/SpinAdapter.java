package cl.zonagps.zonaventagps;

import android.app.Activity;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import cl.zonagps.zonaventagps.clases.Obra;

/**
 * Created by LEONEL CARRASCO
 */
public class SpinAdapter extends ArrayAdapter<Obra>{

    private ArrayList<Obra> obras;
    private Activity act;

    public SpinAdapter(Activity act, ArrayList<Obra> obras) {
        super(act, R.layout.spinnerobras, obras);
        this.obras = obras;
        this.act=act;
    }

    @Override
    public int getCount() {
        return obras.size();
    }

    @Override
    public Obra getItem(int position) {
        return obras.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        TextView label = new TextView(act);
        label.setTextColor(Color.BLACK);
        label.setPadding(15, 15, 15, 15);
        label.setTextSize(16);
        label.setText(obras.get(position).getNombreObra());

        return label;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        TextView label = new TextView(act);
        label.setTextColor(Color.BLACK);
        label.setPadding(15,15,15,15);
        label.setTextSize(16);
        // Then you can get the current item using the values array (Users array) and the current position
        // You can NOW reference each method you has created in your bean object (User class)
        label.setText(obras.get(position).getNombreObra());

        // And finally return your dynamic (or custom) view for each spinner item
        return label;
    }
}
