package cl.zonagps.zonaventagps;

import android.app.Activity;
import android.database.sqlite.SQLiteDatabase;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import cl.zonagps.zonaventagps.clases.*;
import cl.zonagps.zonaventagps.clases.Visita;

/**
 * Created by LEONEL CARRASCO
 */
public class ListClientesAdapter extends BaseAdapter {

    private final Activity actividad;
    private final ArrayList<Cliente> lista;

    public ListClientesAdapter(Activity actividad, ArrayList<Cliente> clientes) {
        super();
        this.actividad = actividad;
        this.lista = clientes;
    }

    @Override
    public int getCount() {
        return lista.size();
    }

    @Override
    public Cliente getItem(int position) {
        return lista.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = actividad.getLayoutInflater();
        View view = inflater.inflate(R.layout.item_cliente, null,
                true);

        Cliente c = lista.get(position);



        ImageView i = (ImageView)view.findViewById(R.id.imgItemCliente);
        if(visitaMes(c.getPk_RutCliente())){
            i.setImageResource(R.drawable.noprofilecheck);
        }else{
            i.setImageResource(R.drawable.noprofilenocheck);
        }


        TextView t = (TextView)view.findViewById(R.id.lblItemNombreCliente);
        t.setText("Nombre: " + lista.get(position).getNombreCliente());
        t = (TextView) view.findViewById(R.id.lblItemRutCliente);
        t.setText("RUT: " +lista.get(position).getPk_RutCliente()+"");

        return view;
    }

    public boolean visitaMes(int rut){
        boolean hay = false;
        DBHelper h = new DBHelper(actividad);
        SQLiteDatabase db = h.getReadableDatabase();

        ArrayList<Visita> vs = h.verMisVisitasCliente(db, rut);

        if(vs != null && vs.size() > 0) {
            for (Visita v : vs) {

                Date fechaActual = new Date();
                SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
                String fechaSistema = formateador.format(fechaActual);

                try {
                    Date fechaDate1 = formateador.parse(v.getFechaVisita());
                    Date fechaDate2 = formateador.parse(fechaSistema);

                    int monthHoy = fechaDate2.getMonth();
                    int monthVisita = fechaDate1.getMonth();
                    if (monthHoy == monthVisita)
                        hay = true;
                } catch (ParseException ex) {
                    hay = false;
                }
            }
        }
        return hay;
    }
}
