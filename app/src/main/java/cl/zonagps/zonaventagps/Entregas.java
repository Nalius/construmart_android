package cl.zonagps.zonaventagps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import cl.zonagps.zonaventagps.clases.*;
import cl.zonagps.zonaventagps.clases.Visita;


public class Entregas extends ActionBarActivity{

    Cliente cl;
    Vendedor vendedor;
    ArrayList<VisitaMotivo> motivos;
    ArrayList<VisitaMotivo> motivosdef;
    private static final int REQUEST_PERMISO = 0;
    private static final int REQUEST_CHECK = 0;
    private Obra obra;
    int posicion;
    String observacion;

    private LocationManager locManager;
    private LocationListener locListener;
    private Location locVisita;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entregas);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#006e98")));

        motivosdef = new ArrayList<VisitaMotivo>();
        VisitaMotivo m = new VisitaMotivo();
        m.setFk_Motivo(1);
        m.setObservacion("");
        m.setConfirmacion(0);
        motivosdef.add(m);
        m = new VisitaMotivo();
        m.setFk_Motivo(2);
        m.setObservacion("");
        m.setConfirmacion(0);
        motivosdef.add(m);
        m = new VisitaMotivo();
        m.setFk_Motivo(3);
        m.setObservacion("");
        m.setConfirmacion(0);
        motivosdef.add(m);
        m = new VisitaMotivo();
        m.setFk_Motivo(4);
        m.setObservacion("");
        m.setConfirmacion(0);
        motivosdef.add(m);
        m = new VisitaMotivo();
        m.setFk_Motivo(5);
        m.setObservacion("");
        m.setConfirmacion(0);
        motivosdef.add(m);

        obra = new Obra();
        obra.setPk_Obra(0);


        posicion = getIntent().getExtras().getInt("PosicionObra", 0);
        observacion = getIntent().getExtras().getString("Observacion","");

        if(!observacion.equals("") && observacion!= null){
            ((EditText)findViewById(R.id.txtEntregasObservacion)).setText(observacion);
        }

        //Obtenemos una referencia al LocationManager
        locManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

        //Obtenemos la última posición conocida
        Location loc = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(loc == null){
            loc = locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }
        locVisita = locManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
        if(locVisita == null){
            locVisita = locManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
        }

        if(loc!=null) {
            ((TextView)findViewById(R.id.lblLatitud)).setText("Latitud: "+ String.valueOf(loc.getLatitude()));
            ((TextView)findViewById(R.id.lblLongitud)).setText("Longitud: " + String.valueOf(loc.getLongitude()));
        }else{
            ((TextView)findViewById(R.id.lblLatitud)).setText("Latitud: 0.0");
            ((TextView)findViewById(R.id.lblLongitud)).setText("Longitud: 0.0");
        }

        locListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                ((TextView)findViewById(R.id.lblLatitud)).setText("Latitud: "+ String.valueOf(location.getLatitude()));
                ((TextView)findViewById(R.id.lblLongitud)).setText("Longitud: " + String.valueOf(location.getLongitude()));
                locVisita = location;
            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {
                Log.e("GPS", "GPS OFF");

            }
        };

        locManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,3000,0,locListener);
        locManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER,3000,0,locListener);





        motivos = (ArrayList<VisitaMotivo>)getIntent().getExtras().getSerializable("Motivos");

        /*if(motivos!=null){
            Log.e("Motivo","Hay motivos");
            Log.e("Motivo", motivos.get(0).getPk_Motivo() + " "+motivos.get(0).getDescripcionMotivo());
            TextView lblmotivos = (TextView)findViewById(R.id.lblMotivos);
            String conMotivos = "";
            for(cl.boxapp.construmart.clases.Motivo mtv : motivos){
                switch (mtv.getPk_Motivo()){
                    case 1: conMotivos += "Se cierra negocio: ";
                        break;
                    case 2: conMotivos += "Solicita Cotización: ";
                        break;
                    case 3: conMotivos+= "Cancela deuda: ";
                        break;
                    case 4: conMotivos += "Compromiso de pago: ";
                        break;
                    case 5: conMotivos += "Agenda nueva reunión: ";
                        break;
                    default:break;
                }
                conMotivos += mtv.getDescripcionMotivo()+"\r\n";
            }
            lblmotivos.setText(conMotivos);
            lblmotivos.setVisibility(View.VISIBLE);
        }else{
            Log.e("Motivo","No hay mtivos");
        }*/



        cl = (Cliente)getIntent().getExtras().getSerializable("Cliente");
        vendedor = (Vendedor)getIntent().getExtras().getSerializable("Vendedor");
        if(cl == null || vendedor == null){
            finish();
        }
        DBHelper helper = new DBHelper(getApplicationContext());
        SQLiteDatabase db = helper.getReadableDatabase();

        ((TextView) findViewById(R.id.txtEntregasNombreCliente)).setText(cl.getNombreCliente());
        ((TextView)findViewById(R.id.txtEntregasRutCliente)).setText(cl.getPk_RutCliente() + "");

        final ArrayList<Obra> obras = helper.verObrasCliente(cl.getPk_RutCliente(),db);
        if(obras == null || obras.size() == 0){
            ((MenuItem)findViewById(R.id.btnGuardarDatos)).setEnabled(false);
            ((Button)findViewById(R.id.btnEntregasCheckList)).setEnabled(false);
            //Toast.makeText(getApplicationContext(),"El cliente no tiene obras, agregue una",Toast.LENGTH_LONG).show();
            // Inflating the layout for the toast
            LayoutInflater inflater = getLayoutInflater();
            View layout = inflater.inflate(R.layout.error_toast,
                    (ViewGroup) findViewById(R.id.toast_custom_error));

            // Typecasting and finding the view in the inflated layout
            TextView text = (TextView) layout.findViewById(R.id.tvtoast);

            // Setting the text to be displayed in the Toast
            text.setText("El cliente no tiene obras,\nagregue una.");

            // Creating the Toast
            Toast toast = new Toast(getApplicationContext());

            // Setting the position of the Toast to centre
            toast.setGravity(Gravity.CENTER, 0, 0);

            // Setting the duration of the Toast
            toast.setDuration(Toast.LENGTH_SHORT);

            // Setting the Inflated Layout to the Toast
            toast.setView(layout);

            // Showing the Toast
            toast.show();
        }else {
            Spinner sp = (Spinner)findViewById(R.id.spObrasCliente);
            SpinAdapter spa = new SpinAdapter(this,obras);
            sp.setAdapter(spa);
            if(posicion > 0){
                sp.setSelection(posicion);
            }

            sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    obra = obras.get(position);
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
        }


        Button entregaNuevaObra = (Button)findViewById(R.id.btnEntregasNuevaObra);
        entregaNuevaObra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(),NuevaObra.class);
                i.putExtra("Cliente",(Serializable)cl);
                i.putExtra("Vendedor",(Serializable)vendedor);

                startActivityForResult(i, REQUEST_PERMISO);
            }
        });

        Button entregaCheckList = (Button)findViewById(R.id.btnEntregasCheckList);
        entregaCheckList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Spinner sp = (Spinner)findViewById(R.id.spObrasCliente);
                int position = sp.getSelectedItemPosition();
                String valorObservacion = ((EditText)findViewById(R.id.txtEntregasObservacion)).getText().toString();
                Intent i = new Intent(getApplicationContext(),Motivo.class);
                i.putExtra("PosicionObra",position);
                i.putExtra("Cliente",(Serializable)cl);
                i.putExtra("Vendedor",(Serializable)vendedor);
                i.putExtra("Observacion",valorObservacion);
                i.putExtra("Motivos",(Serializable)motivos);
                startActivityForResult(i, REQUEST_CHECK);
            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if ( requestCode == REQUEST_PERMISO ){
            if ( resultCode == Activity.RESULT_OK ){
                finish();
            }
        }
        if( requestCode == REQUEST_CHECK){
            if( resultCode == Activity.RESULT_OK){
                finish();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_entregas, menu);
        return true;
    }

    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Finalizar...");
        alertDialogBuilder.setMessage("Desea volver sin guardar?").setPositiveButton("Si", new DialogInterface.OnClickListener()  {
            public void onClick(DialogInterface dialog, int id)
            {
                finish();
            }}).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alertDialogBuilder.show();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            Intent i = new Intent();
            i.setData(Uri.parse("http://www.zonagps.cl/construmart/manual.pdf"));
            startActivity(i);
            return true;
        }
        if(id == R.id.action_salir){

            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
            alertDialogBuilder.setTitle("Finalizar");
            alertDialogBuilder.setMessage("Desea finalizar el programa sin guardar?").setPositiveButton("Si", new DialogInterface.OnClickListener()  {
                public void onClick(DialogInterface dialog, int id)
                {
                    if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
                        finishAffinity();
                    }
                }}).setNegativeButton("No", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    dialog.cancel();
                }
            });
            alertDialogBuilder.show();

        }
        if(id == R.id.action_about){
            Intent i = new Intent(getApplicationContext(),Acerca.class);
            startActivity(i);
        }


        if(id == R.id.btnGuardarDatosObra){

            LocationManager location = (LocationManager)getSystemService(LOCATION_SERVICE);
            if(location.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                if(obra.getPk_Obra() != 0){
                   // locVisita = location.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if(locVisita!=null){
                        Location locObra  = new Location("");
                        locObra.setLatitude(obra.getLatitudObra());
                        locObra.setLongitude(obra.getLongitudObra());

                        float distancia = locVisita.distanceTo(locObra);
                        Log.e("Obra",obra.getPk_Obra() + "");
                        Log.e("Location Visita", locVisita.toString());
                        Log.e("Location Obra",obra.getLatitudObra() + " " +obra.getLongitudObra() + " " + obra.getRadioObra());
                        Log.e("DIstancia", distancia + "");

                        if(distancia < obra.getRadioObra() || (obra.getLongitudObra() == 0 && obra.getLatitudObra()==0)){
                            cl.zonagps.zonaventagps.clases.Visita v = new Visita();
                            v.setFk_RutCliente(cl.getPk_RutCliente());
                            v.setFk_Obra(obra.getPk_Obra());
                            v.setFk_RutVendedor(vendedor.getPk_RutVendedor());
                            v.setFechaVisita(getDate());
                            v.setLatitud(locVisita.getLatitude());
                            v.setLongitud(locVisita.getLongitude());
                            v.setObservacion(((EditText) findViewById(R.id.txtEntregasObservacion)).getText().toString());

                            DBHelper h = new DBHelper(getApplicationContext());
                            SQLiteDatabase db = h.getWritableDatabase();
                            SQLiteDatabase dbw = h.getReadableDatabase();
                            int r = h.insertarVisita(v,db,dbw);
                            int ultimaVisita = h.getUltimoIDVisita(dbw);
                            Log.e("Ultima Visita", String.valueOf(ultimaVisita));
                            if(r==1){
                                if(motivos!=null){
                                    Log.e("VisitaMotivos","DEL ACTIVITY");
                                    for(VisitaMotivo motivo : motivos){

                                            VisitaMotivo vm = motivo;
                                            vm.setFk_Visita(ultimaVisita);
                                            int res = h.insertarVisitaMotivo(vm,db);
                                            if(res == 1){
                                                Log.e("Motivo a obra","Motivo agregado a obra");
                                            }else{
                                                Log.e("Motivo a obra","Error al agregar motivo");
                                            }

                                    }
                                }else{
                                    Log.e("VisitaMotivos","DEFAULT");
                                    for(VisitaMotivo motivo : motivosdef){

                                            VisitaMotivo vm = motivo;
                                            vm.setFk_Visita(ultimaVisita);
                                            int res = h.insertarVisitaMotivo(vm,db);
                                            if(res == 1){
                                                Log.e("Motivo a obra","Motivo agregado a obra");
                                            }else{
                                                Log.e("Motivo a obra","Error al agregar motivo");
                                            }

                                    }
                                }
                                //Toast.makeText(getApplicationContext(),"Visita guardada",Toast.LENGTH_SHORT).show();
                                // Inflating the layout for the toast
                                LayoutInflater inflater = getLayoutInflater();
                                View layout = inflater.inflate(R.layout.ok_toast,
                                        (ViewGroup) findViewById(R.id.toast_custom_ok));

                                // Typecasting and finding the view in the inflated layout
                                TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                                // Setting the text to be displayed in the Toast
                                text.setText("Visita guardada.");

                                // Creating the Toast
                                Toast toast = new Toast(getApplicationContext());

                                // Setting the position of the Toast to centre
                                toast.setGravity(Gravity.CENTER, 0, 0);

                                // Setting the duration of the Toast
                                toast.setDuration(Toast.LENGTH_SHORT);

                                // Setting the Inflated Layout to the Toast
                                toast.setView(layout);

                                // Showing the Toast
                                toast.show();
                                finish();
                                Intent i = new Intent(Entregas.this,Panel.class);
                                startActivity(i);
                            }else{
                                //Toast.makeText(getApplicationContext(),"Error al guardar la visita",Toast.LENGTH_SHORT).show();
                                // Inflating the layout for the toast
                                LayoutInflater inflater = getLayoutInflater();
                                View layout = inflater.inflate(R.layout.error_toast,
                                        (ViewGroup) findViewById(R.id.toast_custom_error));

                                // Typecasting and finding the view in the inflated layout
                                TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                                // Setting the text to be displayed in the Toast
                                text.setText("Error al guardar la visita.");

                                // Creating the Toast
                                Toast toast = new Toast(getApplicationContext());

                                // Setting the position of the Toast to centre
                                toast.setGravity(Gravity.CENTER, 0, 0);

                                // Setting the duration of the Toast
                                toast.setDuration(Toast.LENGTH_SHORT);

                                // Setting the Inflated Layout to the Toast
                                toast.setView(layout);

                                // Showing the Toast
                                toast.show();
                            }
                        }else{
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Entregas.this);
                            alertDialogBuilder.setTitle("Visita fuera de rango");
                            alertDialogBuilder.setMessage("Desea guardar de todas maneras?").setPositiveButton("Si", new DialogInterface.OnClickListener()  {
                                public void onClick(DialogInterface dialog, int id)
                                {
                                    cl.zonagps.zonaventagps.clases.Visita v = new Visita();
                                    v.setFk_RutCliente(cl.getPk_RutCliente());
                                    v.setFk_Obra(obra.getPk_Obra());
                                    v.setFk_RutVendedor(vendedor.getPk_RutVendedor());
                                    v.setFechaVisita(getDate());
                                    v.setLatitud(locVisita.getLatitude());
                                    v.setLongitud(locVisita.getLongitude());
                                    v.setObservacion(((EditText) findViewById(R.id.txtEntregasObservacion)).getText().toString());

                                    DBHelper h = new DBHelper(getApplicationContext());
                                    SQLiteDatabase db = h.getWritableDatabase();
                                    SQLiteDatabase dbw = h.getReadableDatabase();
                                    int r = h.insertarVisita(v,db,dbw);
                                    int ultimaVisita = h.getUltimoIDVisita(dbw);
                                    Log.e("Ultima Visita", String.valueOf(ultimaVisita));
                                    if(r==1){
                                        if(motivos!=null){
                                            for(VisitaMotivo motivo : motivos){

                                                    VisitaMotivo vm = motivo;
                                                    vm.setFk_Visita(ultimaVisita);
                                                    int res = h.insertarVisitaMotivo(vm,db);
                                                    if(res == 1){
                                                        Log.e("Motivo a obra","Motivo agregado a obra");
                                                    }else{
                                                        Log.e("Motivo a obra","Error al agregar motivo");
                                                    }

                                            }
                                        }else{
                                            Log.e("VisitaMotivos","DEFAULT");
                                            for(VisitaMotivo motivo : motivosdef){

                                                VisitaMotivo vm = motivo;
                                                vm.setFk_Visita(ultimaVisita);
                                                int res = h.insertarVisitaMotivo(vm,db);
                                                if(res == 1){
                                                    Log.e("Motivo a obra","Motivo agregado a obra");
                                                }else{
                                                    Log.e("Motivo a obra","Error al agregar motivo");
                                                }

                                            }
                                        }
                                        //Toast.makeText(getApplicationContext(),"Visita guardada",Toast.LENGTH_SHORT).show();
                                        // Inflating the layout for the toast
                                        LayoutInflater inflater = getLayoutInflater();
                                        View layout = inflater.inflate(R.layout.ok_toast,
                                                (ViewGroup) findViewById(R.id.toast_custom_ok));

                                        // Typecasting and finding the view in the inflated layout
                                        TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                                        // Setting the text to be displayed in the Toast
                                        text.setText("Visita guardada.");

                                        // Creating the Toast
                                        Toast toast = new Toast(getApplicationContext());

                                        // Setting the position of the Toast to centre
                                        toast.setGravity(Gravity.CENTER, 0, 0);

                                        // Setting the duration of the Toast
                                        toast.setDuration(Toast.LENGTH_SHORT);

                                        // Setting the Inflated Layout to the Toast
                                        toast.setView(layout);

                                        // Showing the Toast
                                        toast.show();
                                        finish();
                                    }else{
                                        //Toast.makeText(getApplicationContext(),"Error al guardar la visita",Toast.LENGTH_SHORT).show();
                                        // Inflating the layout for the toast
                                        LayoutInflater inflater = getLayoutInflater();
                                        View layout = inflater.inflate(R.layout.error_toast,
                                                (ViewGroup) findViewById(R.id.toast_custom_error));

                                        // Typecasting and finding the view in the inflated layout
                                        TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                                        // Setting the text to be displayed in the Toast
                                        text.setText("Error al guardar la visita.");

                                        // Creating the Toast
                                        Toast toast = new Toast(getApplicationContext());

                                        // Setting the position of the Toast to centre
                                        toast.setGravity(Gravity.CENTER, 0, 0);

                                        // Setting the duration of the Toast
                                        toast.setDuration(Toast.LENGTH_SHORT);

                                        // Setting the Inflated Layout to the Toast
                                        toast.setView(layout);

                                        // Showing the Toast
                                        toast.show();
                                    }

                                }}).setNegativeButton("No", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });
                            alertDialogBuilder.show();
                        }
                    }else{
                        //Toast.makeText(getApplicationContext(),"GPS obteniendo localización, intente de nuevo",Toast.LENGTH_LONG).show();
                        // Inflating the layout for the toast
                        LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.error_toast,
                                (ViewGroup) findViewById(R.id.toast_custom_error));

                        // Typecasting and finding the view in the inflated layout
                        TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                        // Setting the text to be displayed in the Toast
                        text.setText("GPS obteniendo localización,\nintente nuevamente.");

                        // Creating the Toast
                        Toast toast = new Toast(getApplicationContext());

                        // Setting the position of the Toast to centre
                        toast.setGravity(Gravity.CENTER, 0, 0);

                        // Setting the duration of the Toast
                        toast.setDuration(Toast.LENGTH_SHORT);

                        // Setting the Inflated Layout to the Toast
                        toast.setView(layout);

                        // Showing the Toast
                        toast.show();
                    }
                }else{
                    //Toast.makeText(getApplicationContext(),"Debe seleccionar una obra",Toast.LENGTH_SHORT).show();
                    // Inflating the layout for the toast
                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.error_toast,
                            (ViewGroup) findViewById(R.id.toast_custom_error));

                    // Typecasting and finding the view in the inflated layout
                    TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                    // Setting the text to be displayed in the Toast
                    text.setText("Debe seleccionar una obra.");

                    // Creating the Toast
                    Toast toast = new Toast(getApplicationContext());

                    // Setting the position of the Toast to centre
                    toast.setGravity(Gravity.CENTER, 0, 0);

                    // Setting the duration of the Toast
                    toast.setDuration(Toast.LENGTH_SHORT);

                    // Setting the Inflated Layout to the Toast
                    toast.setView(layout);

                    // Showing the Toast
                    toast.show();
                }
            }else{
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Entregas.this);
                alertDialogBuilder.setTitle("GPS Desactivado");
                alertDialogBuilder.setMessage("Tiene que activar el GPS para seguir").setNeutralButton("Habilitar", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                });
                alertDialogBuilder.show();
                Log.e("ErrorALERT", "dasd");
            }

        }

        return super.onOptionsItemSelected(item);
    }

    public String getDate(){
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String date = df.format(Calendar.getInstance().getTime());
        return  date;
    }


}
