package cl.zonagps.zonaventagps;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import java.io.Serializable;
import java.util.ArrayList;

import cl.zonagps.zonaventagps.clases.*;


public class Motivo extends ActionBarActivity {

    Vendedor vendedor;
    Cliente cl;
    int posicion = 0;
    String observacion;
    ArrayList<VisitaMotivo> motivos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_motivo);

        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#006e98")));


        vendedor = (Vendedor)getIntent().getExtras().getSerializable("Vendedor");
        cl= (Cliente)getIntent().getExtras().getSerializable("Cliente");
        posicion = getIntent().getExtras().getInt("PosicionObra");
        observacion = getIntent().getExtras().getString("Observacion");
        motivos = (ArrayList<VisitaMotivo>)getIntent().getExtras().getSerializable("Motivos");

        if(motivos!=null){

            for(VisitaMotivo m : motivos){
                switch (m.getFk_Motivo()){
                    case 1:
                        if(m.getConfirmacion() == 1)
                            ((ToggleButton)findViewById(R.id.tglBtnCerrarNegocio)).setChecked(true);
                        ((EditText)findViewById(R.id.txtMotivoCierraNegocio)).setText(m.getObservacion());
                        break;
                    case 2:
                        if(m.getConfirmacion() == 1)
                            ((ToggleButton)findViewById(R.id.tglBtnSolicitaCotizacion)).setChecked(true);
                        ((EditText)findViewById(R.id.txtMotivoSolicitaCotizacion)).setText(m.getObservacion());
                        break;
                    case 3:
                        if(m.getConfirmacion() == 1)
                            ((ToggleButton)findViewById(R.id.tglBtnCancelaDeuda)).setChecked(true);
                        ((EditText)findViewById(R.id.txtMotivoCancelaDeuda)).setText(m.getObservacion());
                        break;
                    case 4:
                        if(m.getConfirmacion() == 1)
                            ((ToggleButton)findViewById(R.id.tglBtnCompromisodePago)).setChecked(true);
                        ((EditText)findViewById(R.id.txtMotivoCompromisodePago)).setText(m.getObservacion());
                        break;
                    case 5:
                        if(m.getConfirmacion() == 1)
                            ((ToggleButton)findViewById(R.id.tglBtnAgendaNuevaReunion)).setChecked(true);
                        ((EditText)findViewById(R.id.txtMotivoAgendaNuevaReunion)).setText(m.getObservacion());
                        break;
                }
            }


        }else{
            Log.e("Motivos","No hay motivos");
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_motivo, menu);
        return true;
    }

    @Override
    public void onBackPressed()
    {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
        alertDialogBuilder.setTitle("Finalizar");
        alertDialogBuilder.setMessage("Desea volver sin guardar?").setPositiveButton("Si", new DialogInterface.OnClickListener()  {
            public void onClick(DialogInterface dialog, int id)
            {
                finish();
            }}).setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        alertDialogBuilder.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            Intent i = new Intent();
            i.setData(Uri.parse("http://www.zonagps.cl/construmart/manual.pdf"));
            startActivity(i);
            return true;
        }
        if(id == R.id.action_about){
            Intent i = new Intent(getApplicationContext(),Acerca.class);
            startActivity(i);
        }

        if(id == R.id.action_salir){

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this);
                alertDialogBuilder.setTitle("Finalizar");
                alertDialogBuilder.setMessage("Desea finalizar el programa sin guardar?").setPositiveButton("Si", new DialogInterface.OnClickListener()  {
                    public void onClick(DialogInterface dialog, int id)
                    {
                        if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN) {
                            finishAffinity();
                        }
                    }}).setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
                alertDialogBuilder.show();

        }



        if(id == R.id.btnGuardarDatos){

            boolean existsinfo = false;
            ArrayList<VisitaMotivo> motivos = new ArrayList<VisitaMotivo>();
            boolean seCierraNegocio = ((ToggleButton) findViewById(R.id.tglBtnCerrarNegocio)).isChecked();
            boolean solicitaCotizacion = ((ToggleButton) findViewById(R.id.tglBtnSolicitaCotizacion)).isChecked();
            boolean cancelaDeuda = ((ToggleButton) findViewById(R.id.tglBtnCancelaDeuda)).isChecked();
            boolean compromisoPago = ((ToggleButton) findViewById(R.id.tglBtnCompromisodePago)).isChecked();
            boolean agendaNuevaReunion = ((ToggleButton) findViewById(R.id.tglBtnAgendaNuevaReunion)).isChecked();

            existsinfo = true;

            EditText txtSeCierraNegocio = (EditText)findViewById(R.id.txtMotivoCierraNegocio);
            VisitaMotivo m = new VisitaMotivo();
            m.setFk_Motivo(1);
            m.setObservacion(txtSeCierraNegocio.getText().toString());
            if(seCierraNegocio)
                m.setConfirmacion(1);
            else
                m.setConfirmacion(0);
            motivos.add(m);


            EditText txtSolicitaCotizacion = (EditText)findViewById(R.id.txtMotivoSolicitaCotizacion);
            m = new VisitaMotivo();
            m.setFk_Motivo(2);
            m.setObservacion(txtSolicitaCotizacion.getText().toString());
            if(solicitaCotizacion)
                m.setConfirmacion(1);
            else
                m.setConfirmacion(0);
            motivos.add(m);

            EditText txtCancelaDeuda = (EditText)findViewById(R.id.txtMotivoCancelaDeuda);
            m = new VisitaMotivo();
            m.setFk_Motivo(3);
            m.setObservacion(txtCancelaDeuda.getText().toString());
            if(cancelaDeuda)
                m.setConfirmacion(1);
            else
                m.setConfirmacion(0);
            motivos.add(m);

            EditText txtCompromisoPago = (EditText)findViewById(R.id.txtMotivoCompromisodePago);
            m = new VisitaMotivo();
            m.setFk_Motivo(4);
            m.setObservacion(txtCompromisoPago.getText().toString());
            if(compromisoPago)
                m.setConfirmacion(1);
            else
                m.setConfirmacion(0);
            motivos.add(m);


            EditText txtAgendaReunion = (EditText)findViewById(R.id.txtMotivoAgendaNuevaReunion);
            m = new VisitaMotivo();
            m.setFk_Motivo(5);
            m.setObservacion(txtAgendaReunion.getText().toString());
            if(agendaNuevaReunion)
                m.setConfirmacion(1);
            else
                m.setConfirmacion(0);
            motivos.add(m);



            if(existsinfo){
                Intent i2 = new Intent(Motivo.this,Motivo.class);
                i2.putExtra("permiso",1);
                setResult(Activity.RESULT_OK, i2);
                Intent i = new Intent(getApplicationContext(),Entregas.class);
                i.putExtra("Motivos",(Serializable)motivos);
                i.putExtra("Vendedor",vendedor);
                i.putExtra("Cliente",cl);
                i.putExtra("PosicionObra",posicion);
                i.putExtra("Observacion",observacion);
                startActivity(i);
                // Inflating the layout for the toast
                LayoutInflater inflater = getLayoutInflater();
                View layout = inflater.inflate(R.layout.ok_toast,
                        (ViewGroup) findViewById(R.id.toast_custom_ok));

                // Typecasting and finding the view in the inflated layout
                TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                // Setting the text to be displayed in the Toast
                text.setText("Motivos guardados.");

                // Creating the Toast
                Toast toast = new Toast(getApplicationContext());

                // Setting the position of the Toast to centre
                toast.setGravity(Gravity.CENTER, 0, 0);

                // Setting the duration of the Toast
                toast.setDuration(Toast.LENGTH_SHORT);

                // Setting the Inflated Layout to the Toast
                toast.setView(layout);

                // Showing the Toast
                toast.show();
                finish();
            }else{
                Toast.makeText(getApplicationContext(),"No hay datos para guardar",Toast.LENGTH_LONG).show();
            }

        }

        return super.onOptionsItemSelected(item);
    }
}
