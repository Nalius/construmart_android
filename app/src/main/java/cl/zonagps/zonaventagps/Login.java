package cl.zonagps.zonaventagps;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import cl.zonagps.zonaventagps.DAO.Conectar;
import cl.zonagps.zonaventagps.DAO.VendedorDAO;
import cl.zonagps.zonaventagps.clases.*;
import cl.zonagps.zonaventagps.clases.Visita;


public class Login extends ActionBarActivity {
    private Vendedor vendedor;
    private Vendedor vsqlite;
    private String user;
    private String pass;
    private String imei;
    private String FechaHoy;
    private boolean opcion;
    private int micod = 0;
    int app = 0;
    boolean errorClientes = true;
    boolean cortaInternet = false;


    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);


        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        getSupportActionBar().setBackgroundDrawable(new ColorDrawable(Color.parseColor("#006e98")));

        ImageView logo = (ImageView)findViewById(R.id.logoact);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                app++;
                if((app%10) == 0){
                    Intent i = new Intent(getApplicationContext(),Credits.class);
                    startActivity(i);
                }
            }
        });

        try
        {
            BufferedReader fin =
                    new BufferedReader(
                            new InputStreamReader(
                                    openFileInput("password.txt")));

            String texto = fin.readLine();

            String[] cred = texto.split("~");

            ((EditText)findViewById(R.id.txtUser)).setText(cred[0]);
            ((EditText)findViewById(R.id.txtPass)).setText(cred[1]);
            ((CheckBox)findViewById(R.id.cbxRecordarContrasena)).setChecked(true);
            fin.close();
        }
        catch (FileNotFoundException e){
            Log.e("Fichero no encontrado","Fichero no encontrado");
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al leer fichero desde memoria interna");
        }


        //CHECK MOSTRAR CONTRASENA
        CheckBox cbxMostarContrasena = (CheckBox)findViewById(R.id.cbxVerContrasena);
        cbxMostarContrasena.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    ((TextView)findViewById(R.id.txtPass)).setInputType(InputType.TYPE_CLASS_TEXT);

                }else{
                    ((TextView)findViewById(R.id.txtPass)).setInputType(129);

                }
            }
        });

        //Recordar Contraseña
        CheckBox cbxGuardarContrasena = (CheckBox)findViewById(R.id.cbxRecordarContrasena);
        cbxGuardarContrasena.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    EditText pass = (EditText)findViewById(R.id.txtPass);
                    EditText user = (EditText)findViewById(R.id.txtUser);
                    if(!pass.getText().toString().isEmpty() || !user.getText().toString().isEmpty()){

                        try
                        {
                            OutputStreamWriter fout=
                                    new OutputStreamWriter(
                                            openFileOutput("password.txt", Context.MODE_PRIVATE));

                            fout.write(user.getText().toString() + "~" +pass.getText().toString());
                            fout.close();
                            //Toast.makeText(getApplicationContext(),"Contraseña guardada",Toast.LENGTH_LONG).show();
                            // Inflating the layout for the toast
                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.ok_toast,
                                    (ViewGroup) findViewById(R.id.toast_custom_ok));

                            // Typecasting and finding the view in the inflated layout
                            TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                            // Setting the text to be displayed in the Toast
                            text.setText("Datos guardados.");

                            // Creating the Toast
                            Toast toast = new Toast(getApplicationContext());

                            // Setting the position of the Toast to centre
                            toast.setGravity(Gravity.CENTER, 0, 0);

                            // Setting the duration of the Toast
                            toast.setDuration(Toast.LENGTH_SHORT);

                            // Setting the Inflated Layout to the Toast
                            toast.setView(layout);

                            // Showing the Toast
                            toast.show();
                        }
                        catch (Exception ex)
                        {
                            Log.e("Ficheros", "Error al escribir fichero a memoria interna");
                        }
                    }
                    else {
                        buttonView.setChecked(false);
                        //Toast.makeText(getApplicationContext(),"Debe ingresar una contraseña para guardar.",Toast.LENGTH_LONG).show();
                        // Inflating the layout for the toast
                        LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.error_toast,
                                (ViewGroup) findViewById(R.id.toast_custom_error));

                        // Typecasting and finding the view in the inflated layout
                        TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                        // Setting the text to be displayed in the Toast
                        text.setText("Los campos son requeridos\npara guardar.");

                        // Creating the Toast
                        Toast toast = new Toast(getApplicationContext());

                        // Setting the position of the Toast to centre
                        toast.setGravity(Gravity.CENTER, 0, 0);

                        // Setting the duration of the Toast
                        toast.setDuration(Toast.LENGTH_SHORT);

                        // Setting the Inflated Layout to the Toast
                        toast.setView(layout);

                        // Showing the Toast
                        toast.show();
                    }
                }else{

                    if(deleteFile("password.txt")){
                        ((EditText)findViewById(R.id.txtUser)).setText("");
                        ((EditText)findViewById(R.id.txtPass)).setText("");
                        Log.e("Se borro archivo","Se logro borrar archivo");
                    }else {
                        Log.e("borrar archivo", "No se pudo borrar archivo");
                    }
                }
            }
        });

        Button login = (Button)findViewById(R.id.btnLogin);
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                LocationManager location = (LocationManager)getSystemService(LOCATION_SERVICE);
                if(location.isProviderEnabled(LocationManager.GPS_PROVIDER)){




                    user = ((EditText)findViewById(R.id.txtUser)).getText().toString();
                    pass = ((EditText)findViewById(R.id.txtPass)).getText().toString();
                    imei = ((TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE)).getDeviceId();
                    if(!user.isEmpty() && !pass.isEmpty()){

                        DBHelper helper = new DBHelper(getApplicationContext());
                        SQLiteDatabase db = helper.getReadableDatabase();
                        SQLiteDatabase dbw = helper.getWritableDatabase();
                        vsqlite = helper.getVendedor(db, user);
                        if(vsqlite!=null){
                            if(pass.equals(vsqlite.getPassUsuario())){
                                //imei.equals(vsqlite.getImei())
                                if(true){
                                    //hayInternet(getApplicationContext()) && isOnline() && !fechaDescarga.equals(fechaTelefono)
                                            /* if(borrarFecha()){
                                                    if(guardarFecha())
                                                        Log.e("Fecha actualizada","Fecha actuaizada");
                                                    else
                                                        Log.e("Fecha Actualizada", "ERROR");
                                                }else{
                                                    guardarFecha();
                                                }*/
                                    String fechaTelefono = getFecha();
                                    String fechaDescarga = verFecha();
                                    if(hayInternet(getApplicationContext()) && isOnline() && !fechaDescarga.equals(fechaTelefono)) {
                                        opcion = false;
                                        new syncApp().execute();
                                               /* if(borrarFecha()){
                                                    if(guardarFecha())
                                                        Log.e("Fecha actualizada","Fecha actuaizada");
                                                    else
                                                        Log.e("Fecha Actualizada", "ERROR");
                                                }else{
                                                    guardarFecha();
                                                }*/
                                        if(borrarFecha()){
                                            if(guardarFecha())
                                                Log.e("Fecha actualizada","Fecha actuaizada");
                                            else
                                                Log.e("Fecha Actualizada", "ERROR");
                                        }else{
                                            guardarFecha();
                                        }
                                    }else{



                                        Intent i = new Intent(getApplicationContext(),Panel.class);
                                        i.putExtra("Vendedor", (Serializable) vsqlite);
                                        AuxClass.setV(vsqlite);
                                        startActivity(i);
                                    }

                                }else{
                                    //Toast.makeText(getApplicationContext(),"El Imei no corresponde con el usuario",Toast.LENGTH_SHORT).show();
                                    // Inflating the layout for the toast
                                    LayoutInflater inflater = getLayoutInflater();
                                    View layout = inflater.inflate(R.layout.error_toast,
                                            (ViewGroup) findViewById(R.id.toast_custom_error));

                                    // Typecasting and finding the view in the inflated layout
                                    TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                                    // Setting the text to be displayed in the Toast
                                    text.setText("El imei no corresponde\ncon el usuario.");

                                    // Creating the Toast
                                    Toast toast = new Toast(getApplicationContext());

                                    // Setting the position of the Toast to centre
                                    toast.setGravity(Gravity.CENTER, 0, 0);

                                    // Setting the duration of the Toast
                                    toast.setDuration(Toast.LENGTH_SHORT);

                                    // Setting the Inflated Layout to the Toast
                                    toast.setView(layout);

                                    // Showing the Toast
                                    toast.show();
                                }
                            }else{
                                //Toast.makeText(getApplicationContext(),"La contraseña ingresada no coincide",Toast.LENGTH_SHORT).show();
                                // Inflating the layout for the toast
                                LayoutInflater inflater = getLayoutInflater();
                                View layout = inflater.inflate(R.layout.error_toast,
                                        (ViewGroup) findViewById(R.id.toast_custom_error));

                                // Typecasting and finding the view in the inflated layout
                                TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                                // Setting the text to be displayed in the Toast
                                text.setText("La contraseña ingresada\nno coincide.");

                                // Creating the Toast
                                Toast toast = new Toast(getApplicationContext());

                                // Setting the position of the Toast to centre
                                toast.setGravity(Gravity.CENTER, 0, 0);

                                // Setting the duration of the Toast
                                toast.setDuration(Toast.LENGTH_SHORT);

                                // Setting the Inflated Layout to the Toast
                                toast.setView(layout);

                                // Showing the Toast
                                toast.show();
                            }

                        }else {
                            if(hayInternet(getApplicationContext()) && isOnline()){
                                new LoginTask().execute();
                            }else{

                                // Inflating the layout for the toast
                                LayoutInflater inflater = getLayoutInflater();
                                View layout = inflater.inflate(R.layout.error_toast,
                                        (ViewGroup) findViewById(R.id.toast_custom_error));

                                // Typecasting and finding the view in the inflated layout
                                TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                                // Setting the text to be displayed in the Toast
                                text.setText("No tienes internet\npor favor conéctate.");

                                // Creating the Toast
                                Toast toast = new Toast(getApplicationContext());

                                // Setting the position of the Toast to centre
                                toast.setGravity(Gravity.CENTER, 0, 0);

                                // Setting the duration of the Toast
                                toast.setDuration(Toast.LENGTH_SHORT);

                                // Setting the Inflated Layout to the Toast
                                toast.setView(layout);

                                // Showing the Toast
                                toast.show();


                            }
                        }
                    }else{
                        //Toast.makeText(getApplicationContext(),"Todos los campos son obligatorios.",Toast.LENGTH_LONG).show();
                        // Inflating the layout for the toast
                        LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.error_toast,
                                (ViewGroup) findViewById(R.id.toast_custom_error));

                        // Typecasting and finding the view in the inflated layout
                        TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                        // Setting the text to be displayed in the Toast
                        text.setText("Todos los campos son\nobligatorios.");

                        // Creating the Toast
                        Toast toast = new Toast(getApplicationContext());

                        // Setting the position of the Toast to centre
                        toast.setGravity(Gravity.CENTER, 0, 0);

                        // Setting the duration of the Toast
                        toast.setDuration(Toast.LENGTH_SHORT);

                        // Setting the Inflated Layout to the Toast
                        toast.setView(layout);

                        // Showing the Toast
                        toast.show();
                    }

                }else {
                    //Toast.makeText(getApplicationContext(),"El GPS no está activado, actívelo por favor.", Toast.LENGTH_LONG).show();
                    // Inflating the layout for the toast
                    LayoutInflater inflater = getLayoutInflater();
                    View layout = inflater.inflate(R.layout.error_toast,
                            (ViewGroup) findViewById(R.id.toast_custom_error));

                    // Typecasting and finding the view in the inflated layout
                    TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                    // Setting the text to be displayed in the Toast
                    text.setText("El GPS no está activado,\nactívelo por favor");

                    // Creating the Toast
                    Toast toast = new Toast(getApplicationContext());

                    // Setting the position of the Toast to centre
                    toast.setGravity(Gravity.CENTER, 0, 0);

                    // Setting the duration of the Toast
                    toast.setDuration(Toast.LENGTH_SHORT);

                    // Setting the Inflated Layout to the Toast
                    toast.setView(layout);

                    // Showing the Toast
                    toast.show();
                    startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                }
            }
        });


        TextView t = (TextView)findViewById(R.id.lblURL);
        t.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent();
                i.setData(Uri.parse("http://www.zonagps.cl"));
                startActivity(i);
            }
        });
    }

    public boolean borrarFecha(){
        boolean exito = false;
        if(deleteFile("fecha.txt")){
            Log.e("Se borro fecha","Se logro borrar fecha");
            exito = true;
        }else {
            Log.e("borrar fecha", "No se pudo borrar fecha");
        }
        return exito;
    }

    public boolean guardarFecha(){
        boolean exito = false;
        try
        {
            OutputStreamWriter fout=
                    new OutputStreamWriter(
                            openFileOutput("fecha.txt", Context.MODE_PRIVATE));

            fout.write(getFecha());
            fout.close();
            exito = true;
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al escribir fichero a memoria interna");
        }
        return exito;
    }

    public String verFecha(){
        String fecha = "";
        try
        {
            BufferedReader fin =
                    new BufferedReader(
                            new InputStreamReader(
                                    openFileInput("fecha.txt")));

            fecha = fin.readLine();
            fin.close();
        }
        catch (FileNotFoundException e){
            Log.e("Fichero no encontrado","Fichero no encontrado");
        }
        catch (Exception ex)
        {
            Log.e("Ficheros", "Error al leer fichero desde memoria interna");
        }
        return fecha;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_help) {
            Intent i = new Intent();
            i.setData(Uri.parse("http://www.zonagps.cl/construmart/manual.pdf"));
            startActivity(i);
            return true;
        }
        if(id == R.id.action_about){
            Intent i = new Intent(getApplicationContext(),Acerca.class);
            startActivity(i);
        }

        if(id == R.id.action_salir){
            if(Build.VERSION.SDK_INT > Build.VERSION_CODES.JELLY_BEAN){
                finishAffinity();
            }


        }

        return super.onOptionsItemSelected(item);
    }

    public static boolean verificaInternet(Context ctx) {

        boolean bConectado = false;
        ConnectivityManager connec = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        // No sólo wifi, también GPRS
        NetworkInfo[] redes = connec.getAllNetworkInfo();
        // este bucle debería no ser tan ñapa
        for (int i = 0; i < 2; i++) {
            // ¿Tenemos conexión? ponemos a true
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                bConectado = true;
            }
        }
        return bConectado;
    }

    public boolean verConexion(){
        boolean conectado = false;
        if(Conectar.GetConnection() != null)
            conectado = true;
        return conectado;
    }

    public static boolean hayInternet(Context ctx) {
        /*if (verificaInternet(ctx)) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL("http://www.google.com").openConnection());
                urlc.setRequestProperty("User-Agent", "Test");
                urlc.setRequestProperty("Connection", "close");
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (IOException e) {
                Log.e("INTERNET", "Error checking internet connection", e);
                e.printStackTrace();
            }
        } else {
            Log.e("INTERNET", "RED NO DISPONIBLE");
        }
        return false;*/
        boolean bConectado = false;
        ConnectivityManager connec = (ConnectivityManager) ctx
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        // No sólo wifi, también GPRS
        NetworkInfo[] redes = connec.getAllNetworkInfo();
        // este bucle debería no ser tan ñapa
        for (int i = 0; i < 2; i++) {
            // ¿Tenemos conexión? ponemos a true
            if (redes[i].getState() == NetworkInfo.State.CONNECTED) {
                bConectado = true;
            }
        }
        return bConectado;
    }

    private String getFecha() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "dd/MM/yyyy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    public Boolean isOnline() {
        try {
            Process p1 = java.lang.Runtime.getRuntime().exec("ping -c 1 www.google.com");
            int returnVal = p1.waitFor();
            boolean reachable = (returnVal==0);
            return reachable;
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return false;
    }

    ProgressDialog pd = null;
    public class LoginTask extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {

                    pd = ProgressDialog.show(Login.this, "Ingresando por primera vez...", "Sincronizando datos, espere por favor");
        }

        @Override
        protected void onPostExecute(Void aVoid) {

                    if (vendedor != null) {

                        if (pass.equals(vendedor.getPassUsuario())) {
                            //imei.equals(vendedor.getImei())
                            if (true) {
                                DBHelper helper = new DBHelper(Login.this);
                                SQLiteDatabase db = helper.getWritableDatabase();
                                int res = helper.agregarVendedorSQLite(vendedor, db);
                                if (res == -1 || res == -2) {
                                    Toast.makeText(Login.this, "Error al guardar usuario en SQLite", Toast.LENGTH_SHORT).show();
                                } else {
                                    opcion = true;
                                    new syncApp().execute();
                                }
                            } else {
                                //Toast.makeText(Login.this, "El Imei del telefono no corresponde con el usuario", Toast.LENGTH_SHORT).show();
                                // Inflating the layout for the toast
                                LayoutInflater inflater = getLayoutInflater();
                                View layout = inflater.inflate(R.layout.error_toast,
                                        (ViewGroup) findViewById(R.id.toast_custom_error));

                                // Typecasting and finding the view in the inflated layout
                                TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                                // Setting the text to be displayed in the Toast
                                text.setText("El imei del teléfono no\ncorresponde con el usuario.");

                                // Creating the Toast
                                Toast toast = new Toast(getApplicationContext());

                                // Setting the position of the Toast to centre
                                toast.setGravity(Gravity.CENTER, 0, 0);

                                // Setting the duration of the Toast
                                toast.setDuration(Toast.LENGTH_SHORT);

                                // Setting the Inflated Layout to the Toast
                                toast.setView(layout);

                                // Showing the Toast
                                toast.show();
                                pd.dismiss();
                            }
                        } else {
                            //Toast.makeText(Login.this, "La contraseña no es correcta", Toast.LENGTH_SHORT).show();
                            // Inflating the layout for the toast
                            LayoutInflater inflater = getLayoutInflater();
                            View layout = inflater.inflate(R.layout.error_toast,
                                    (ViewGroup) findViewById(R.id.toast_custom_error));

                            // Typecasting and finding the view in the inflated layout
                            TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                            // Setting the text to be displayed in the Toast
                            text.setText("La contraseña no es correcta.");

                            // Creating the Toast
                            Toast toast = new Toast(getApplicationContext());

                            // Setting the position of the Toast to centre
                            toast.setGravity(Gravity.CENTER, 0, 0);

                            // Setting the duration of the Toast
                            toast.setDuration(Toast.LENGTH_SHORT);

                            // Setting the Inflated Layout to the Toast
                            toast.setView(layout);

                            // Showing the Toast
                            toast.show();
                            pd.dismiss();
                        }


                    } else {
                        //Toast.makeText(getApplicationContext(), "El usuario no existe.", Toast.LENGTH_LONG).show();
                        // Inflating the layout for the toast
                        LayoutInflater inflater = getLayoutInflater();
                        View layout = inflater.inflate(R.layout.error_toast,
                                (ViewGroup) findViewById(R.id.toast_custom_error));

                        // Typecasting and finding the view in the inflated layout
                        TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                        // Setting the text to be displayed in the Toast
                        text.setText("El vendedor no existe.");//aqui

                        // Creating the Toast
                        Toast toast = new Toast(getApplicationContext());

                        // Setting the position of the Toast to centre
                        toast.setGravity(Gravity.CENTER, 0, 0);

                        // Setting the duration of the Toast
                        toast.setDuration(Toast.LENGTH_SHORT);

                        // Setting the Inflated Layout to the Toast
                        toast.setView(layout);

                        // Showing the Toast
                        toast.show();
                        pd.dismiss();

                    }

        }

        @Override
        protected Void doInBackground(Void... params) {

                    vendedor = new VendedorDAO(0).verVendedor(user);

            return null;
        }
    }

    public class syncApp extends AsyncTask<Void,Void,Void>{
        @Override
        protected void onPreExecute() {

                if (!opcion) {

                    pd = ProgressDialog.show(Login.this, "Sincronizando...", "Sincronizando datos con la aplicación, espere por favor.");
                }

        }

        @Override
        protected Void doInBackground(Void... params) {

            ArrayList<Cliente> bClientes = null;
            ArrayList<Obra> bObras = null;
                    VendedorDAO vDAO ;

                    if(vendedor!=null){
                        vDAO = new VendedorDAO(vendedor.getPk_RutVendedor());
                    }else{
                        vDAO = new VendedorDAO(vsqlite.getPk_RutVendedor());
                    }

                    //Obtener fecha Servidor formato 01-01-2015
                    FechaHoy = vDAO.getFechaServidor();
            try {
                //Agregar Clientes a SQLite
                DBHelper helper = new DBHelper(Login.this);
                SQLiteDatabase dbw = helper.getWritableDatabase();
                try {
                    //dbw.execSQL("DELETE FROM Cliente");
                    //dbw.execSQL("DELETE FROM Obra");
                    //dbw.execSQL("DELETE FROM ObraNew");
                    dbw.execSQL("DELETE FROM Motivo");
                    //dbw.execSQL("DELETE FROM Visita");
                    //dbw.execSQL("DELETE FROM VisitaNew");
                    //dbw.execSQL("DELETE FROM VisitaMotivo");
                    //dbw.execSQL("DELETE FROM VisitaMotivoNew");
                    //dbw.execSQL("DELETE FROM ProspectoNew");
                    //dbw.execSQL("DELETE FROM ObraVendedorNew");
                } catch (SQLiteException ex) {

                }

                //NUEVO
                DBHelper h = new DBHelper(Login.this);
                SQLiteDatabase dbr = h.getReadableDatabase();
                bObras = h.verObras(dbr);
                ArrayList<Cliente> clientes_sqlite = h.verClientes(dbr);
                bClientes = clientes_sqlite;
                ArrayList<Cliente> clientes_bd = vDAO.getClientesVendedor();
                int sizeClientesSQLite = 0;
                int sizeClientesNube = 0;
                if (clientes_sqlite == null) {
                    Log.e("Clientes", "Clientes sqlite null");
                } else {
                    Log.e("Clientes", "Cliente sqlite tiene");
                    sizeClientesSQLite = clientes_sqlite.size();
                }

                if (clientes_bd == null) {
                    Log.e("Clientes", "Clientes BD NULL");
                } else {
                    Log.e("Clientes", "Clientes BD TIENE");
                    sizeClientesNube = clientes_bd.size();
                }


                if(sizeClientesNube == 0){
                    try {
                        dbw.execSQL("DELETE FROM Cliente");

                    } catch (SQLiteException e) {

                    }
                }

                if (clientes_sqlite == null || sizeClientesNube != sizeClientesSQLite || sizeClientesNube == sizeClientesSQLite) {

                    try {
                        //dbw.execSQL("DELETE FROM Cliente");
                        //POR SI NO
                        //dbw.execSQL("DELETE FROM Obra");
                    } catch (SQLiteException e) {

                    }

                    for (Cliente cl : vDAO.getClientesVendedor()) {
                        helper.eliminarClienteSQLite(cl.getPk_RutCliente(), dbw);
                        helper.agregarClientesaSQLite(cl, dbw);
                        Log.e("AGREGADOCliente", "Cliente agregado: " + cl.getNombreCliente());
                        //Añadimos obras de cada cliente

                        //NUEVO
                        int sizeObrasCliente = 0;
                       ArrayList<Obra> obsc = vDAO.getObrasporCliente(cl.getPk_RutCliente());
                        if(obsc != null){
                            sizeObrasCliente = obsc.size();
                        }
                        Log.e("CANTIDAD OBRAS", "Cliente " + cl.getNombreCliente() + " tiene " + sizeObrasCliente);

                        if (sizeObrasCliente > 0) {
                            for (Obra o : obsc) {
                                //NUEVO V
                                Log.e("RADIO", o.getRadioObra()+ "");
                                int aux = helper.eliminarObraSQLite(o.getPk_Obra(), dbw);
                                int r = helper.agregarObrasSQLite(o, dbw);
                                Log.e("ObraCliente", "Obra agregada: " + o.getNombreObra() + " de " + o.getFk_RutCliente());
                                if (r == 1)
                                    Log.e("Obra agregada", "Obra agregada: " + o.getNombreObra());
                                else
                                    Log.e("Obra no agregada", "Obra no agregada: " + o.getNombreObra());
                            }
                        } else {
                            if (helper.eliminarClienteSQLite(cl.getPk_RutCliente(), dbw) == 1) {
                                Log.e("Cliente eliminado", "Cliente eliminado por no tener obras");
                            }
                        }
                    }

                } else {
                    errorClientes = false;
                    //NUEVO
                    try{
                        dbw.execSQL("DELETE FROM Cliente");
                    }catch (SQLiteException ex){

                    }
                    for(Cliente c : bClientes){
                        h.agregarClientesaSQLite(c,dbw);
                    }
                    try{
                        dbw.execSQL("DELETE FROM Obra");
                    }catch (SQLiteException ex){

                    }
                    for(Obra o : bObras){
                        h.agregarObrasSQLite(o,dbw);
                    }
                }


                    /*
                    for(Cliente cl : vDAO.getClientesVendedor()){
                        helper.agregarClientesaSQLite(cl, dbw);
                        Log.e("AGREGADOCliente","Cliente agregado: "+cl.getNombreCliente());
                        //Añadimos obras de cada cliente
                        int sizeObrasCliente = vDAO.getObrasporCliente(cl.getPk_RutCliente()).size();
                        Log.e("CANTIDAD OBRAS", "Cliente "+cl.getNombreCliente()+" tiene "+sizeObrasCliente);
                        if(sizeObrasCliente > 0){
                            for (Obra o : vDAO.getObrasporCliente(cl.getPk_RutCliente())){
                                int r = helper.agregarObrasSQLite(o,dbw);
                                Log.e("ObraCliente","Obra agregada: "+o.getNombreObra() + " de " +o.getFk_RutCliente());
                                if(r==1)
                                    Log.e("Obra agregada","Obra agregada: "+o.getNombreObra());
                                else
                                    Log.e("Obra no agregada","Obra no agregada: "+o.getNombreObra());
                            }
                        }else{
                            if(helper.eliminarClienteSQLite(cl.getPk_RutCliente(),dbw) == 1){
                                Log.e("Cliente eliminado","Cliente eliminado por no tener obras");
                            }
                        }
                    }

            */

                //Agregamos los motivos a SQLite
                for (cl.zonagps.zonaventagps.clases.Motivo m : vDAO.getMotivos()) {
                    helper.agregarMotivosaSQLite(m, dbw);
                }

                //Obtener el ultimo numero Correlativo de Obra
                helper.eliminarCorrelativoObra(dbw);
                helper.insertarCorrelativoObra(vDAO.getCorrelativoObra(), dbw);


                //Agregamos las Visitas a SQLite
                //NUEVO
                ArrayList<Visita> visitas_sqlite = h.verVisitas(dbr);
                int sizeVisitasSQLite = 0;
                if(visitas_sqlite != null){
                    sizeVisitasSQLite = visitas_sqlite.size();
                }
                ArrayList<Visita> visitas_nube = vDAO.getVisitasdelVendedor();
                int sizeVisitasNube = 0;
                if(visitas_nube != null){
                    sizeVisitasNube = visitas_nube.size();
                }
                if (sizeVisitasNube != sizeVisitasSQLite) {
                    dbw.execSQL("DELETE FROM Visita");
                    for (Visita vis : visitas_nube) {
                        int res = helper.agregarVisitasSQLite(vis, dbw);
                        if (res == 1)
                            Log.e("Visita agregada", "Visita " + vis.getPk_Visita() + " " + vis.getFechaVisita());
                    }
                }
                    /*for(Visita vis : vDAO.getVisitasdelVendedor()){
                        int res = helper.agregarVisitasSQLite(vis,dbw);
                        if(res == 1)
                            Log.e("Visita agregada","Visita " + vis.getPk_Visita() + " " + vis.getFechaVisita());
                    }*/
            }catch(Exception npe){
                cortaInternet = true;

                DBHelper h = new DBHelper(Login.this);
                SQLiteDatabase dbr = h.getReadableDatabase();
                SQLiteDatabase dbw = h.getWritableDatabase();

                Log.e("ERRORnpe", npe.getMessage());
                try{
                    dbw.execSQL("DELETE FROM Cliente");
                }catch (SQLiteException ex){

                }
                for(Cliente c : bClientes){
                    h.agregarClientesaSQLite(c,dbw);
                }
                try{
                    dbw.execSQL("DELETE FROM Obra");
                }catch (SQLiteException ex){

                }
                for(Obra o : bObras){
                    h.agregarObrasSQLite(o,dbw);
                }
            }




            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
                if(pd.isShowing())
                    pd.dismiss();
            if(cortaInternet){
                Toast.makeText(Login.this,"Hubo una interrupción con la conexión a internet, se trabajara con la última sincronización.",Toast.LENGTH_LONG).show();
            }
            if(!errorClientes){
                Toast.makeText(Login.this,"No se pudo cargar la cartera completa, se cargará la última cartera de clientes",Toast.LENGTH_LONG).show();
            }
                if(opcion) {
                    Intent i = new Intent(Login.this, Panel.class);
                    AuxClass.setV(vendedor);
                    i.putExtra("Vendedor", (Serializable) vendedor);
                    startActivity(i);
                }else{
                    Intent i = new Intent(getApplicationContext(),Panel.class);
                    AuxClass.setV(vsqlite);
                    i.putExtra("Vendedor", (Serializable) vsqlite);
                    startActivity(i);
                }


        }
    }



}
