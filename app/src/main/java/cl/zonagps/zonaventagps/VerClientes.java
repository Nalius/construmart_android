package cl.zonagps.zonaventagps;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;

import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;

import cl.zonagps.zonaventagps.clases.AuxClass;
import cl.zonagps.zonaventagps.clases.Cliente;
import cl.zonagps.zonaventagps.clases.Vendedor;


public class VerClientes extends Fragment {

    private String[] nombres;
    private String[] rut;
    private View viewAct;
    private ArrayList<Cliente> clientes = new ArrayList<Cliente>();
    private ArrayAdapter<Cliente> adapter;
    ListClientesAdapter lca;

    private SQLiteDatabase dbaux;
    private ListView ltCliente;
    private DBHelper haux;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_ver_clientes, container, false);
        //Vendedor vendedor = (Vendedor)getActivity().getIntent().getExtras().getSerializable("Vendedor");
        Vendedor vendedor = AuxClass.getV();
        Log.e("V", vendedor.toString());
        eventosFragmento(v, vendedor);

        this.viewAct = v;
        return v;
    }




    private void eventosFragmento(View vista, final Vendedor vendedor) {

        DBHelper helper = new DBHelper(getActivity());
        SQLiteDatabase db = helper.getReadableDatabase();
        dbaux = db;
        haux = helper;
        clientes = null;


        //Traemos los clientes de SQLite
        clientes = helper.verClientes(db);

        //Si no es null y mayor a 0, lo enlazamos al ListView
        if(clientes != null && clientes.size() > 0){
            ltCliente = (ListView)vista.findViewById(R.id.lvNewListaClientes);
            lca = new ListClientesAdapter(getActivity(),clientes);
            lca.notifyDataSetChanged();
            ltCliente.setAdapter(lca);

            ltCliente.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Cliente cl = clientes.get(position);
                    Intent i = new Intent(getActivity(), Entregas.class);
                    i.putExtra("Cliente", (Serializable) cl);
                    i.putExtra("Vendedor", (Serializable) vendedor);
                    startActivity(i);

                }
            });

        }

        ImageButton verCliente = (ImageButton)vista.findViewById(R.id.btnVerCliente);
        verCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TextView buscar = (TextView) viewAct.findViewById(R.id.txtBuscar);

                Spinner sp = (Spinner) viewAct.findViewById(R.id.spinFiltro);
                String filtro = sp.getSelectedItem().toString();
                String valor = buscar.getText().toString();

                DBHelper h = new DBHelper(getActivity());
                SQLiteDatabase db = h.getReadableDatabase();
                clientes = null;

                boolean opcion;
                if (filtro.equals("RUT"))
                    opcion = true;
                else
                    opcion = false;
                if (valor.isEmpty() || valor.equals("")) {
                    clientes = h.verClientes(db);
                } else {
                    clientes = h.buscarClientes(opcion, db, valor);
                }
                if (clientes != null && clientes.size() > 0) {
                    ListView ltClientes = (ListView) viewAct.findViewById(R.id.lvNewListaClientes);
                    lca = new ListClientesAdapter(getActivity(), clientes);
                    lca.notifyDataSetChanged();
                    ltClientes.setAdapter(lca);


                    ltClientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            Cliente cl = clientes.get(position);
                            Intent i = new Intent(getActivity(),Entregas.class);
                            i.putExtra("Cliente",(Serializable)cl);
                            i.putExtra("Vendedor",(Serializable)vendedor);
                            startActivity(i);
                        }
                    });
                } else {
                    //Toast.makeText(getActivity(),"No hay resultados en la búsqueda",Toast.LENGTH_SHORT).show();
                    // Inflating the layout for the toast
                    LayoutInflater inflater = getActivity().getLayoutInflater();
                    View layout = inflater.inflate(R.layout.error_toast,
                            (ViewGroup) viewAct.findViewById(R.id.toast_custom_error));

                    // Typecasting and finding the view in the inflated layout
                    TextView text = (TextView) layout.findViewById(R.id.tvtoast);

                    // Setting the text to be displayed in the Toast
                    text.setText("No hay resultados en\n la búsqueda.");

                    // Creating the Toast
                    Toast toast = new Toast(getActivity());

                    // Setting the position of the Toast to centre
                    toast.setGravity(Gravity.CENTER, 0, 0);

                    // Setting the duration of the Toast
                    toast.setDuration(Toast.LENGTH_SHORT);

                    // Setting the Inflated Layout to the Toast
                    toast.setView(layout);

                    // Showing the Toast
                    toast.show();
                }

            }
        });





        Spinner sp = (Spinner)vista.findViewById(R.id.spinFiltro);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position == 1) {
                    TextView t = (TextView) viewAct.findViewById(R.id.txtBuscar);
                    t.setHint("Ingrese RUT...");
                    t.setText("");
                    t.setInputType(InputType.TYPE_CLASS_NUMBER);

                } else {
                    TextView t = (TextView) viewAct.findViewById(R.id.txtBuscar);
                    t.setHint("Ingrese Nombre...");
                    t.setText("");
                    t.setInputType(InputType.TYPE_CLASS_TEXT);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }




}
